<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('video_drafts', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->timestamps();
            $table->uuid('user_id');
            $table->string('title');
            $table->text('description')->nullable();
            $table->mediumText('file_path');
            $table->mediumText('streaming_path');
            $table->string('thumbnail');
            $table->string('byteplus_video_id')->nullable();
            $table->mediumText('meta');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('video_drafts');
    }
};
