<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('videos', function (Blueprint $table) {
            $table->string('byteplus_video_id')->nullable();
            $table->mediumText('file_path')->change();
            $table->mediumText('streaming_path')->change();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('videos', function (Blueprint $table) {
            $table->dropColumn('byteplus_video_id');
            $table->string('file_path')->change();
            $table->string('streaming_path')->change();
        });
    }
};
