<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('hashtags', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->timestamps();
            $table->text('name');
        });

        Schema::create('video_hashtags', function (Blueprint $table) {
            $table->timestamps();
            $table->uuid('video_id');
            $table->uuid('hashtag_id');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('video_hashtags');
        Schema::dropIfExists('hashtags');
    }
};
