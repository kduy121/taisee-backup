<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('video_drafts', function (Blueprint $table) {
            $table->string('title')->nullable()->change();
            $table->integer('video_privacy')->default(1)->after('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('video_drafts', function (Blueprint $table) {
            $table->string('title')->nullable(false)->change();
        });
    }
};
