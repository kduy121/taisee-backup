<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Video>
 */
class VideoDraftFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'user_id' => $this->faker->uuid(),
            'video_privacy' => 2,
            'title' => Str::random(10),
            'description' => Str::random(10),
            'file_path' => Str::random(10),
            'streaming_path' => Str::random(10),
            'thumbnail' => Str::random(10),
            'byteplus_video_id' => Str::random(10),
            'meta' => Str::random(10),
        ];
    }
}
