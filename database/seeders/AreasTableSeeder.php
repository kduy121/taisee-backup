<?php

namespace Database\Seeders;

use App\Models\Area;
use Illuminate\Database\Seeder;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Facades\Excel;

class AreasTableSeeder extends Seeder implements ToModel, WithHeadingRow
{
    public function run()
    {
        $filePath = storage_path('excel/[Taisee]_Data.xlsx');
        $sheetName = 'Location - Prefectures';

        $areas = Excel::toArray(new AreasTableSeeder, $filePath);

        foreach ($areas[2] as $row) {
            $area = $this->model($row);
            $area->save();
        }
    }

    public function model(array $row)
    {
        return new Area([
            'address' => [
                'province' => [
                    'en' => $row['prefectures_en'],
                    'ja' => $row['jp'],
                ],
            ],
        ]);
    }
}
