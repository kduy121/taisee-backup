<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\Video;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $videos = [
            'https://v.pinimg.com/videos/720p/77/4f/21/774f219598dde62c33389469f5c1b5d1.mp4',
            'https://v.pinimg.com/videos/720p/75/40/9a/75409a62e9fb61a10b706d8f0c94de9a.mp4',
            'https://v.pinimg.com/videos/720p/0d/29/18/0d2918323789eabdd7a12cdd658eda04.mp4',
            'https://v.pinimg.com/videos/720p/dd/24/bb/dd24bb9cd68e9e25d1def88cad0a9ea7.mp4',
            'https://v.pinimg.com/videos/720p/d5/15/78/d51578c69d36c93c6e20144e9f887c73.mp4',
            'https://v.pinimg.com/videos/720p/c2/6d/2b/c26d2bacb4a9f6402d2aa0721193e06e.mp4',
            'https://v.pinimg.com/videos/720p/62/81/60/628160e025f9d61b826ecc921b9132cd.mp4',
            'https://v.pinimg.com/videos/720p/5f/aa/3d/5faa3d057eb31dd05876f622ea2e7502.mp4',
            'https://v.pinimg.com/videos/720p/65/b0/54/65b05496c385c89f79635738adc3b15d.mp4',
            'https://v.pinimg.com/videos/720p/86/a1/c6/86a1c63fc58b2e1ef18878b7428912dc.mp4',
        ];

        $thumbnails = [
            'https://p9-sign-sg.tiktokcdn.com/obj/tos-alisg-p-0037/49aa0e895d8647dca7e9f9fdaa6b9a71_1691247080?x-expires=1691506800&x-signature=21C2ZHE7En4oTtkfTb88RllDsSw%3D',
            'https://i.pinimg.com/originals/ea/eb/70/eaeb7052a15ff3631d00461cf1deed60.jpg',
            'https://i.pinimg.com/originals/70/e7/12/70e71233d852f17cde5e5ba03ee3ca95.jpg',
            'https://i.pinimg.com/originals/a1/56/dc/a156dca7d6d5980c89eb03cbe6e76e85.jpg',
            'https://i.pinimg.com/originals/45/cd/a1/45cda128a98424f242da23c59bad3914.jpg',
            'https://p9-sign-sg.tiktokcdn.com/obj/tos-alisg-p-0037/49aa0e895d8647dca7e9f9fdaa6b9a71_1691247080?x-expires=1691506800&x-signature=21C2ZHE7En4oTtkfTb88RllDsSw%3D',
            'https://p9-sign-sg.tiktokcdn.com/obj/tos-alisg-p-0037/49aa0e895d8647dca7e9f9fdaa6b9a71_1691247080?x-expires=1691506800&x-signature=21C2ZHE7En4oTtkfTb88RllDsSw%3D',
            'https://p9-sign-sg.tiktokcdn.com/obj/tos-alisg-p-0037/49aa0e895d8647dca7e9f9fdaa6b9a71_1691247080?x-expires=1691506800&x-signature=21C2ZHE7En4oTtkfTb88RllDsSw%3D',
            'https://p9-sign-sg.tiktokcdn.com/obj/tos-alisg-p-0037/49aa0e895d8647dca7e9f9fdaa6b9a71_1691247080?x-expires=1691506800&x-signature=21C2ZHE7En4oTtkfTb88RllDsSw%3D',
            'https://p9-sign-sg.tiktokcdn.com/obj/tos-alisg-p-0037/49aa0e895d8647dca7e9f9fdaa6b9a71_1691247080?x-expires=1691506800&x-signature=21C2ZHE7En4oTtkfTb88RllDsSw%3D',
        ];

        for ($i = 0; $i < 10; $i++) {
            $user = new User([
                'id' => 'id_'.$i,
                'name' => 'user_test_'.$i,
                'user_name' => 'user_test'.$i,
                'email' => 'email_'.$i.'@mail.com',
            ]);
            $user->save();

            for ($j = 0; $j < 10; $j++) {
                $video = new Video([
                    'user_id' => $user->id,
                    'title' => "video test $j",
                    'description' => "description $j",
                    'file_path' => '',
                    'streaming_path' => $videos[$j],
                    'thumbnail' => $thumbnails[$j],
                ]);

                $video->save();
            }
        }
    }
}
