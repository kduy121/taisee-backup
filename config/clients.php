<?php

// Facebook
$facebookUrl = 'https://graph.facebook.com/v17.0/me?';
$appId = '715713379706658';
$appSecret = 'a766212abd37e0118b366884daa28f68';

// Google
$googleUrl = 'https://www.googleapis.com/';

// Yahoo
$yahooUrl = 'https://api.login.yahoo.com/openid/v1/userinfo';

// Twitter
$twitterUrl = 'https://api.twitter.com/';

// Line
$lineUrl = 'https://api.line.me/';

return [
    'socialite' => [
        'facebook' => [
            'base_url' => $facebookUrl,
            'app_id' => $appId,
            'app_secret' => $appSecret,
            'default_graph_version' => 'v17.0',
        ],
        'google' => [
            'base_url' => $googleUrl,
        ],
        'yahoo' => [
            'base_url' => $yahooUrl,
        ],
        'twitter' => [
            'base_url' => $twitterUrl,
        ],
        'line' => [
            'base_url' => $lineUrl,
        ],
    ],
];
