<?php

return [
    // Define error code
    'general' => [
        'example_err_model_user' => 2001,
    ],
    'token' => [
        'expire_token_time' => 10000000000, // min
    ],
    'refresh_token' => [
        'expire_token_time' => 86400, // min
        'signingKey' => 'm2jxkyP66zLr3qJeXJo5yTwM01S5WJHPYPwPRm9m9DP0y6uofYMOxnCZxc4dC42D',
    ],
    'per_page_item' => 25,
];
