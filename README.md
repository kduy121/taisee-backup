# Taisee Backend

# Table of Contents

- [Requirements](#requirements)
  - [Dev Setup Options](#dev-setup-options)
- [Installation](#installation)
  
- [Testing](#testing)
  - [Husky setup](#husky-setup)

## Running commands
- composer i
- php artisan jwt:secret
- php artisan key:generate
- php artisan migrate
- php artisan db:seed

# Requirements

- PHP Extensions: Mod Rewrite (overwrite .htaccess)
- PHP >= v8.2
- MySQL >= 8
- Nginx
- PHP Standards Recommendations (PSR)
  - Required PSR1, PSR4
- [Spatie](https://spatie.be/)
  - [Laravel Permission](https://spatie.be/docs/laravel-permission/v5/installation-laravel) ^5.10
- Husky
- JWT
- UUID: ramsey/uuid


# Installation

- Copy `.env.example` to `.env` then change config for databases... inside `.env` file
- Setup environments and databases, create MYSQL databases inside .env file
- Set `CACHE_DRIVER` to empty
- RUN 
```
php artisan jwt:secret
```

## Husky setup

```bash
npx husky install
```

Create `.huskyrc` in your profile directory with following content:

```sh

./vendor/bin/pint

# Laravel Swagger https://github.com/mtrajano/laravel-swagger
``` composer require mtrajano/laravel-swagger
``` php artisan vendor:publish --provider "Mtrajano\LaravelSwagger\SwaggerServiceProvider"
``` php artisan laravel-swagger:generate --filter="/api" --output="storage/api-docs/api-docs.json"

# Vue template 
``` https://demos.themeselection.com/sneat-vuetify-vuejs-admin-template/documentation/guide/installation.html