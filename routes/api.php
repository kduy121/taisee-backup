<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['namespace' => 'App\Api\Controllers', 'middleware' => ['throttle:600,1']], function () {
    // Auth
    Route::middleware(['api'])->prefix('auth')->group(function () {
        Route::post('login', 'AuthController@login')->name('api.auth.login');
        Route::post('login-admin', 'AuthController@loginAdmin')->name('api.auth.login-admin');
        Route::post('logout', 'AuthController@logout')->name('api.auth.logout');
        Route::post('refresh', 'AuthController@refresh')->name('api.auth.refresh');
        Route::get('me', 'AuthController@me')->name('api.auth.me')->middleware('auth.token');
        Route::post('register', 'AuthController@register')->name('api.auth.register');
        Route::post('guest-login', 'AuthController@guestLogin')->name('api.auth.guest-login');
        // Route::post('delete', 'AuthController@delete')->name('api.guest.delete');
        // Route::post('guest-login', 'AuthController@guestLogin')->name('api.guest.login')->middleware('throttle:2,1');

        // Auth Socialite
        // Route::get('google', 'GoogleController@redirectToGoogle')->name('auth.google');
        // Route::get('google/callback', 'GoogleController@handleGoogleCallback');
    });

    Route::middleware(['api'])->group(function () {
        Route::post('update-user', 'UserController@update')->name('api.user.update-user')->middleware('auth.token');
        Route::get('check-email', 'UserController@checkEmail')->name('api.user.check-mail');
        Route::get('get-user-info', 'UserController@index')->name('api.user.get-info');
        Route::post('delete-user', 'UserController@deleteUserForTester')->name('api.user.delete-user');
        Route::post('update-password', 'UserController@updatePasswordByEmail')->name('api.user.update-password');
        Route::post('like-action', 'UserController@likeAction')->name('api.user.like-action')->middleware('auth.token');
        Route::post('follow-user', 'UserController@followUser')->name('api.user.follow-user')->middleware('auth.token');
        Route::post('comment/create', 'CommentController@create')->name('api.user.comment.create')->middleware('auth.token');
        Route::delete('comment/delete', 'CommentController@delete')->name('api.user.comment.delete')->middleware('auth.token');
        Route::post('comment/update', 'CommentController@update')->name('api.user.comment.update')->middleware('auth.token');
        Route::get('comment/list', 'CommentController@getComment')->name('api.user.comment.list');
        Route::get('user/search', 'UserController@searchUser')->name('api.user.search');
        Route::get('profile', 'UserController@getProfile')->name('api.user.profile');
        Route::get('user/get-follower', 'UserController@getFollower')->name('api.user.get.follower')->middleware('auth.token');
        Route::get('user/get-following', 'UserController@getFollowing')->name('api.user.get.following')->middleware('auth.token');
        Route::get('user/trending', 'UserController@getUserTrending')->name('api.user.get.getUserTrending');
        Route::get('user/top', 'UserController@getTopUser')->name('api.user.get.getTopUser')->middleware('auth.token');
    });

    // Route::middleware(['api'])->prefix('socialite')->group(function () {
    Route::middleware(['api'])->group(function () {
        Route::post('socialite', 'SocialiteController@verifySocialite')->name('socialte.facebook');
    });

    Route::middleware(['api'])->group(function () {
        Route::get('ping', 'AuthController@ping');
        // Route::middleware(['api', 'throttle:2,1'])->group(function () {
        Route::post('interest/store', 'InterestController@store')->name('api.interest.store')->middleware('auth.token');
        Route::get('common', 'CommonController@index')->name('api.common.list');
        Route::get('area', 'AreaController@index');
    });

    Route::middleware(['api'])->group(function () {
        Route::get('get-videos', 'VideosController@getVideos')->name('user.videos');
        Route::get('video/user', 'VideosController@getVideosByUserId')->name('user.videos.get-by-user');
        Route::get('video', 'VideosController@getDetail')->name('user.videos.get-detail');
    });

    Route::middleware(['api'])->prefix('video')->group(function () {
        Route::get('/collection', 'CollectionController@getCollection')->name('user.collection.get')->middleware('auth.token');
        Route::post('/collection', 'CollectionController@create')->name('user.collection.create')->middleware('auth.token');
        Route::delete('/collection', 'CollectionController@delete')->name('user.collection.delete')->middleware('auth.token');
        Route::put('/collection', 'CollectionController@renameCollection')->name('user.collection-video.rename-collection')->middleware('auth.token');
        Route::post('/collection/change', 'CollectionVideoController@changeCollection')->name('user.collection-video.change-collection')->middleware('auth.token');
        Route::post('/bookmark', 'CollectionVideoController@bookmarkVideo')->name('user.collection-video.bookmark-video')->middleware('auth.token');
        Route::get('/bookmark', 'CollectionVideoController@getVideo')->name('user.collection-video.get-video')->middleware('auth.token');
        Route::delete('/bookmark', 'CollectionVideoController@unbookmarkVideo')->name('user.collection-video.unbookmark-video')->middleware('auth.token');
        Route::get('/following', 'VideosController@getVideoFollowing')->name('user.collection-video.getVideoFollowing')->middleware('auth.token');
        Route::get('/friend', 'VideosController@getFriendVideos')->name('user.collection-video.getFriendVideos')->middleware('auth.token');
        Route::post('/change', 'VideosController@changeStatus')->name('user.collection-video.changeStatus')->middleware('auth.token');
        Route::get('/drafts', 'VideosController@getVideoDrafts')->name('user.collection-video.getVideoDrafts')->middleware('auth.token');
        Route::post('/draft/publish', 'VideosController@publish')->name('user.collection-video.draft-publish')->middleware('auth.token');
        Route::get('/draft', 'VideosController@getDetailVideoDraft')->name('user.collection-video.getDetailVideoDraft')->middleware('auth.token');
        Route::put('/draft', 'VideosController@updateVideoDraft')->name('user.collection-video.updateVideoDraft')->middleware('auth.token');
        Route::delete('/draft', 'VideosController@deleteVideoDraft')->name('user.collection-video.deleteVideoDraft')->middleware('auth.token');
        Route::get('/private', 'VideosController@getVideoPrivate')->name('user.video.getVideoPrivate')->middleware('auth.token');
        Route::get('/search-by-hashtag', 'VideosController@searchByHashtag')->name('user.video.searchByHashtag');
        Route::put('/{video}', 'VideosController@updateStatusVideo')->name('user.video.updateStatusVideo')->middleware('auth.token');
        Route::get('', 'VideosController@getVideosBE')->name('user.video.getVideosBE')->middleware('auth.token');
        Route::delete('/{video}', 'VideosController@deleteVideo')->name('user.video.deleteVideo')->middleware('auth.token');
        Route::get('/search', 'VideosController@searchVideo')->name('user.video.searchVideo')->middleware('auth.token');
        Route::get('/top', 'VideosController@getTopVideos')->name('user.video.getTopVideos')->middleware('auth.token');
    });

    Route::middleware(['api', 'auth.token'])->prefix('room')->group(function () {
        Route::post('/', 'RoomController@createRoom')->name('user.chat.create-room');
        Route::get('/', 'RoomController@getRooms')->name('user.chat.get-room');
        Route::delete('/', 'RoomController@deleteRoom')->name('user.chat.delete-room');
        Route::post('/leave', 'RoomController@leaveRoom')->name('user.chat.leave-room');
        Route::post('/add-mem', 'RoomController@addMember')->name('user.chat.add-member');
        Route::post('/message', 'MessageController@sendMessage')->name('user.chat.send-message');
        Route::get('/message', 'MessageController@getMessage')->name('user.chat.get-message');
        Route::post('/pin', 'RoomController@pinRoom')->name('user.chat.pin-room');
        Route::post('/un-pin', 'RoomController@unPinRoom')->name('user.chat.un-pin-room');
        Route::post('/change', 'RoomController@changeRoomName')->name('user.chat.change-room');
        Route::get('/detail', 'RoomController@getRoomDetail')->name('user.chat.get-room-detail');
        Route::get('/member', 'RoomMemberController@getMember')->name('user.chat.get-member');
        Route::get('/top', 'MessageController@searchInbox')->name('user.chat.search-inbox');
        Route::get('/top-user', 'MessageController@topUser')->name('user.chat.top-user');
        Route::get('/top-room', 'MessageController@topRoom')->name('user.chat.top-room');
        Route::get('/search-friend', 'MessageController@searchFriend')->name('user.chat.search-friend');
        Route::delete('/message', 'MessageController@delete')->name('user.chat.delete');
        Route::get('/count/message', 'MessageController@countUnreadMessage')->name('user.chat.count-unread-message');
    });

    Route::middleware(['api'])->prefix('bp')->group(function () {
        Route::get('upload/config', 'ByteplusController@getUploadConfig')->name('bp.config.upload')->middleware('auth.token');
        Route::match(['get', 'post'], 'upload/handle', 'ByteplusController@handleUpload')->name('bp.handler.upload');
    });

    Route::middleware(['api'])->prefix('image')->group(function () {
        Route::post('upload', 'ImageController@handleUpload')->name('image.upload')->middleware('auth.token');
    });

    Route::middleware(['api'])->prefix('livestream')->group(function () {
        Route::get('/', 'LivestreamController@index');
        Route::post('/', 'LivestreamController@create')->middleware('auth.token');
        Route::get('/get-by-id', 'LivestreamController@getById');
        Route::put('', 'LivestreamController@updateLivestream')->middleware('auth.token');
        Route::delete('/', 'LivestreamController@delete')->middleware('auth.token');
        Route::get('/comment', 'CommentLivestreamController@index');
        Route::post('/comment', 'CommentLivestreamController@create')->middleware('auth.token');
        Route::put('/comment', 'CommentLivestreamController@updateComment')->middleware('auth.token');
        Route::delete('/comment', 'CommentLivestreamController@delete')->middleware('auth.token');
    });

    Route::middleware(['api'])->prefix('hashtag')->group(function () {
        Route::get('/', 'HashtagController@getHashtag')->name('user.hashtag.getHashtag');
        Route::get('/{id}', 'HashtagController@getById')->name('user.hashtag.getById');
        Route::post('/', 'HashtagController@createHashtag')->name('user.hashtag.createHashtag');
        Route::put('/{hashtag}', 'HashtagController@updateHashtag')->name('user.hashtag.updateHashtag');
        Route::delete('/{hashtag}', 'HashtagController@delete')->name('user.hashtag.delete');
    });

    Route::middleware(['api'])->prefix('user')->group(function () {
        Route::get('/roles', 'RoleController@index')->name('user.roles.getRoles');
        Route::get('/roles/{id}', 'RoleController@show')->name('user.roles.getRole');
        Route::post('/roles', 'RoleController@store')->name('user.roles.createRole');
        Route::put('/roles/{id}', 'RoleController@update')->name('user.roles.updateRole');
        Route::delete('/roles/{id}', 'RoleController@destroy')->name('user.roles.deleteRole');

        Route::get('/permissions', 'PermissionController@index')->name('user.roles.getPermissions');
        Route::get('/permissions/{id}', 'PermissionController@show')->name('user.roles.getPermission');
        Route::post('/permissions', 'PermissionController@store')->name('user.roles.createPermission');
        Route::put('/permissions/{id}', 'PermissionController@update')->name('user.roles.updatePermission');
        Route::delete('/permissions/{id}', 'PermissionController@destroy')->name('user.roles.deletePermission');

        Route::get('/search-be', 'UserController@searchUserBE')->name('user.searchBE')->middleware('auth.token');
        Route::post('/update-user/{user}', 'UserController@updateUser')->name('user.updateUser')->middleware('auth.token');
        Route::delete('/{user}', 'UserController@delete')->name('user.deleteUser')->middleware('auth.token');
        Route::get('/admin', 'UserController@getAdmin')->name('user.getAdmin')->middleware('auth.token');
        Route::get('/admin/{id}', 'UserController@getAdminById')->name('user.getAdminById')->middleware('auth.token');
        Route::put('/admin/{admin}', 'UserController@updateAdmin')->name('user.updateAdmin')->middleware('web', 'auth.token');
    });
});
