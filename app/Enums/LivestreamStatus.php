<?php

namespace App\Enums;

enum LivestreamStatus: int
{
    case Public = 1;
    case Practice = 2;
}
