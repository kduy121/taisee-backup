<?php

namespace App\Enums;

enum VideoStatus: int
{
    case Public = 1;
    case Private = 2;
    case Draft = 3;
}
