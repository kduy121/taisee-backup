<?php

namespace App\Enums;

enum GenderType
{
    case male;
    case female;
    case other;

    public static function from($value): GenderType
    {
        switch ($value) {
            case 'male':
                return self::male;
            case 'female':
                return self::female;
            case 'other':
                return self::other;
            default:
                throw new \InvalidArgumentException("Invalid SocialiteType value: $value");
        }
    }
}
