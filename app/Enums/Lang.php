<?php

namespace App\Enums;

enum Lang: string
{
    case EN = 'en';
    case JA = 'ja';
}
