<?php

namespace App\Enums;

enum SocialiteType
{
    case LINE;
    case TWITTER;
    case WECHAT;
    case SNAPCHAT;
    case WHATSAPP;
    case AMAZON;
    case SPOTIFY;
    case LINKEDIN;
    case APPLE;
    case GUEST;
    case FACEBOOK;
    case GOOGLE;
    case YAHOO;
    case EMAIL;

    public static function from($value): SocialiteType
    {
        switch ($value) {
            case 'LINE':
                return self::LINE;
            case 'TWITTER':
                return self::TWITTER;
            case 'WECHAT':
                return self::WECHAT;
            case 'SNAPCHAT':
                return self::SNAPCHAT;
            case 'WHATSAPP':
                return self::WHATSAPP;
            case 'AMAZON':
                return self::AMAZON;
            case 'SPOTIFY':
                return self::SPOTIFY;
            case 'LINKEDIN':
                return self::LINKEDIN;
            case 'APPLE':
                return self::APPLE;
            case 'GUEST':
                return self::GUEST;
            case 'FACEBOOK':
                return self::FACEBOOK;
            case 'GOOGLE':
                return self::GOOGLE;
            case 'YAHOO':
                return self::YAHOO;
            case 'EMAIL':
                return self::EMAIL;
            default:
                throw new \InvalidArgumentException("Invalid SocialiteType value: $value");
        }
    }
}
