<?php

namespace App\Enums;

enum StateAccount
{
    case new;
    case member;

    public static function from($value): SocialiteType
    {
        switch ($value) {
            case 'new':
                return self::new;
            case 'member':
                return self::member;
            default:
                throw new \InvalidArgumentException("Invalid SocialiteType value: $value");
        }
    }
}
