<?php

namespace App\Enums;

enum ChatType
{
    case PRIVATE;
    case GROUP;

    public static function from($value): ChatType
    {
        switch ($value) {
            case 'private':
                return self::PRIVATE;
            case 'group':
                return self::GROUP;
            default:
                throw new \InvalidArgumentException("Invalid Chat Type value: $value");
        }
    }
}
