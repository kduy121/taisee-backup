<?php

namespace App\Console\Commands;

use App\Enums\ChatType;
use App\Models\Follower;
use App\Repositories\RoomRepository;
use App\Services\MessageService;
use App\Services\UserService;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Mockery\Exception;

class CreateFriends extends Command
{
    public function __construct(private UserService $userService, private RoomRepository $roomRepo,
        private MessageService $messageService)
    {
        parent::__construct();
    }

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:create-friends {user-id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create friends for user-id';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $userId = $this->argument('user-id');
        $user = $this->userService->getById($userId);
        if (empty($user)) {
            throw new Exception('User not found');
        }

        echo "Start creating 50 friends for user $userId \n";

        $followUsers = $this->userService->searchUser('', $limit = 50);
        foreach ($followUsers as $key => $followUser) {
            $existingFollower = Follower::where([
                ['follower_id', $userId],
                ['following_id', $followUser->id],
            ])->first();

            if (empty($existingFollower)) {
                Follower::create([
                    'follower_id' => $userId,
                    'following_id' => $followUser->id,
                    'is_friend' => true,
                ]);
            }

            $existingFollower = Follower::where([
                ['follower_id', $followUser->id],
                ['following_id', $userId],
            ])->first();

            if (empty($existingFollower)) {
                Follower::create([
                    'follower_id' => $followUser->id,
                    'following_id' => $userId,
                    'is_friend' => true,
                ]);
            }

            // Create Room
            $data['user_id'] = $userId;
            $data['name'] = $userId.'_'.$key;
            $data['member'] = [$followUser->id];
            $data['type'] = count($data['member']) > 1 ? ChatType::from('group')->name : ChatType::from('private')->name;
            $data['total_member'] = count($data['member']) + 1;
            $data['avatar_ids'] = array_slice($data['member'], 0, 2);
            $data['thumb_ids'] = array_slice($data['member'], 0, 3);

            // Check room
            $room = $this->roomRepo->checkRoomPrivate($userId, $data['member'][0]);

            if (! $room) {
                $room = $this->roomRepo->create($data);
                $this->roomRepo->syncData($room, $data['member']);

                // Send Message
                $data['room_id'] = $room->id;
                $data['content'] = "Here is the message $key";
                $data['timestamp'] = Carbon::now()->timestamp;

                $this->messageService->sendMessage($data);
            }
        }

        echo "Finish creating 50 friends for user $userId \n";
        echo "Finish creating 50 room private for user $userId \n";
    }
}
