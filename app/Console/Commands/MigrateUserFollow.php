<?php

namespace App\Console\Commands;

use App\Repositories\FollowerRepository;
use App\Services\UserService;
use Illuminate\Console\Command;

class MigrateUserFollow extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:migrate-follow';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migrate User Follow FollowBack Friend Data';

    public function __construct(private UserService $userService, private FollowerRepository $followerRepository)
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        echo "Start Migration User Follow FollowBack Friend Data \n";

        $follows = $this->followerRepository->getAll();
        foreach ($follows as $follow) {
            // re-follow
            $this->userService->followUser($follow->follower_id, $follow->following_id);
            $this->userService->followUser($follow->follower_id, $follow->following_id);
            echo "\t Finish Migration User {$follow->follower_id} \n";
        }

        echo "End Migration User Follow FollowBack Friend Data \n";
    }
}
