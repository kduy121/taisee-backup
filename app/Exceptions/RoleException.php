<?php

namespace App\Exceptions;

class RoleException extends AppException
{
    public const DEFAULT_ERR_CODE = 200000;

    public const NOT_FOUND = 200001;

    public static $statusTexts = [
        self::NOT_FOUND => 'Role not found.',
    ];

    protected function getDefaultCode()
    {
        return self::DEFAULT_ERR_CODE;
    }

    protected function getResponseCodeHash()
    {
        return self::$statusTexts;
    }
}
