<?php

namespace App\Exceptions;

class UserException extends AppException
{
    public const DEFAULT_ERR_CODE = 30000;

    public const USER_NOT_FOUND = 30001;

    public const PASSWORD_FAIL = 30002;

    public const UPDATE_FAIL = 30003;

    public static $statusTexts = [
        self::DEFAULT_ERR_CODE => 'Bad request.',
        self::USER_NOT_FOUND => 'User not found.',
        self::PASSWORD_FAIL => 'Password and Confirm Password do not match.',
        self::UPDATE_FAIL => 'Update user has fail.',
    ];

    protected function getDefaultCode()
    {
        return self::DEFAULT_ERR_CODE;
    }

    protected function getResponseCodeHash()
    {
        return self::$statusTexts;
    }
}
