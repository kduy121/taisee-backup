<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\Response;

abstract class AppException extends Exception
{
    abstract protected function getDefaultCode();

    abstract protected function getResponseCodeHash();

    public function __construct(protected $code = 400, protected $message = '', Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->message = $message;
    }

    protected function getMessageFromCode()
    {
        if (! empty($this->message)) {
            return $this->message;
        }

        $codeMsgHash = $this->getResponseCodeHash();

        return isset($codeMsgHash[$this->code]) ? $codeMsgHash[$this->code] : $codeMsgHash[$this->getDefaultCode()];
    }

    public function render($request)
    {
        return response()->json([
            'data' => [
                'status' => 'fail',
                'message' => $this->getMessageFromCode(),
                'code' => $this->code,
            ],
        ], Response::HTTP_BAD_REQUEST);
    }
}
