<?php

namespace App\Exceptions;

class ByteplusException extends AppException
{
    public const DEFAULT_ERR_CODE = 60000;

    public const MISSING_BYTEPLUS_CONFIG = 60001;

    public const CAN_NOT_GET_UPLOAD_TOKEN = 60002;

    public const INVALID_CALLBACK_CONTENT = 60003;

    public const CALLBACK_ARG_MISSING = 60004;

    public const INVALID_VIDEO_STATUS = 60005;

    public const USER_NOT_FOUND = 60006;

    public const CAN_NOT_CREATE_VIDEO = 60007;

    public const VIDEO_ID_IS_EMPTY = 60008;

    public static $statusTexts = [
        self::DEFAULT_ERR_CODE => 'Bad request.',
        self::MISSING_BYTEPLUS_CONFIG => 'Missing config for Byteplus.',
        self::CAN_NOT_GET_UPLOAD_TOKEN => 'Can not get token.',
        self::INVALID_CALLBACK_CONTENT => 'Invalid callback content.',
        self::CALLBACK_ARG_MISSING => 'Callback arg is missing.',
        self::INVALID_VIDEO_STATUS => 'Invalid video status.',
        self::USER_NOT_FOUND => 'User not found.',
        self::CAN_NOT_CREATE_VIDEO => 'Can not create video.',
        self::VIDEO_ID_IS_EMPTY => 'video id is empty.',
    ];

    protected function getDefaultCode()
    {
        return self::DEFAULT_ERR_CODE;
    }

    protected function getResponseCodeHash()
    {
        return self::$statusTexts;
    }
}
