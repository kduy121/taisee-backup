<?php

namespace App\Exceptions;

class HashtagException extends AppException
{
    public const DEFAULT_ERR_CODE = 400000;

    public const HASHTAG_NOT_FOUND = 400001;

    public const UPDATE_FAIL = 400002;

    public static $statusTexts = [
        self::HASHTAG_NOT_FOUND => 'Hashtag not found.',
        self::UPDATE_FAIL => 'Update has fail.',
    ];

    protected function getDefaultCode()
    {
        return self::HASHTAG_NOT_FOUND;
    }

    protected function getResponseCodeHash()
    {
        return self::$statusTexts;
    }
}
