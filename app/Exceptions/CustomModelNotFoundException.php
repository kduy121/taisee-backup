<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class CustomModelNotFoundException extends ModelNotFoundException
{
    public function __construct($message, $code = 404, Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->message = $message;
        $this->code = $code;
    }

    public function render($request)
    {
        return response()->json(['error' => $this->message], $this->code);
    }
}
