<?php

namespace App\Exceptions;

class CollectionException extends AppException
{
    public const DEFAULT_ERR_CODE = 20000;

    public const COLLECTION_CREATE_ERR = 20001;

    public const COLLECTION_NOT_FOUND = 20002;

    public const COLLECTION_FORBIDDEN = 20003;

    public const BOOKMARK_VIDEO_FAIL = 20004;

    public const CAN_NOT_CHANGE_THUMB = 20005;

    public static $statusTexts = [
        self::DEFAULT_ERR_CODE => 'Bad request.',
        self::COLLECTION_CREATE_ERR => 'Create has fail.',
        self::COLLECTION_NOT_FOUND => 'Collection not found.',
        self::COLLECTION_FORBIDDEN => 'Collection Forbidden.',
        self::BOOKMARK_VIDEO_FAIL => 'Bookmark video has fail.',
        self::CAN_NOT_CHANGE_THUMB => 'Can not change thumb.',
    ];

    protected function getDefaultCode()
    {
        return self::DEFAULT_ERR_CODE;
    }

    protected function getResponseCodeHash()
    {
        return self::$statusTexts;
    }
}
