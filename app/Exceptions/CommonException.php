<?php

namespace App\Exceptions;

class CommonException extends AppException
{
    public const DEFAULT_ERR_CODE = 11000;

    public const VALIDATE_ERR = 11000;

    public static $statusTexts = [
        self::VALIDATE_ERR => 'Something went wrong.',
    ];

    protected function getDefaultCode()
    {
        return self::DEFAULT_ERR_CODE;
    }

    protected function getResponseCodeHash()
    {
        return self::$statusTexts;
    }
}
