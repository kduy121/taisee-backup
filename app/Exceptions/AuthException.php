<?php

namespace App\Exceptions;

class AuthException extends AppException
{
    public const DEFAULT_ERR_CODE = 10000;

    public const REFRESH_TOKEN_NOT_FOUND = 10001;

    public const CREATE_TOKEN_FAIL = 10002;

    public const REFRESH_TOKEN_INVALID = 10003;

    public const CREATE_REFRESH_TOKEN_FAIL = 10004;

    public static $statusTexts = [
        self::DEFAULT_ERR_CODE => 'Bad request.',
        self::REFRESH_TOKEN_NOT_FOUND => 'Refresh token not found.',
        self::CREATE_TOKEN_FAIL => 'Create token has failed.',
        self::REFRESH_TOKEN_INVALID => 'Refresh token invalid.',
        self::CREATE_REFRESH_TOKEN_FAIL => 'Create refresh token has failed.',
    ];

    protected function getDefaultCode()
    {
        return self::DEFAULT_ERR_CODE;
    }

    protected function getResponseCodeHash()
    {
        return self::$statusTexts;
    }
}
