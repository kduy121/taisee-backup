<?php

namespace App\Exceptions;

class LivestreamException extends AppException
{
    public const DEFAULT_ERR_CODE = 80000;

    public const LIVESTREAM_FORBIDDEN = 80001;

    public const CREATE_FAIL = 80002;

    public const LIVESTREAM_NOT_FOUND = 80003;

    public static $statusTexts = [
        self::DEFAULT_ERR_CODE => 'Bad request.',
        self::LIVESTREAM_FORBIDDEN => 'Forbidden.',
        self::CREATE_FAIL => 'Create livestream has fail.',
        self::LIVESTREAM_NOT_FOUND => 'Livestream not found.',
    ];

    protected function getDefaultCode()
    {
        return self::DEFAULT_ERR_CODE;
    }

    protected function getResponseCodeHash()
    {
        return self::$statusTexts;
    }
}
