<?php

namespace App\Exceptions;

class ChatException extends AppException
{
    public const DEFAULT_ERR_CODE = 40000;

    public const CREATE_ROOM_FAIL = 40001;

    public const CAN_NOT_ADD_MEMBER = 40002;

    public const ROOM_NOT_FOUND = 40003;

    public const USER_NOT_MEMBER = 40004;

    public const ROOM_NOT_GROUP = 40005;

    public const MEMBER_NOT_IN_ROOM = 40006;

    public const ROOM_ALREADY_PINNED = 40007;

    public const ROOM_ALREADY_UNPINNED = 40008;

    public const FORBIDDEN = 40009;

    public const MEMBER_ALREADY = 40010;

    public const SEND_MESSAGE_FAIL = 40011;

    public const DELETE_MESSAGE_FAIL = 40012;

    public const CREATE_SEEN_MESSAGE_FAIL = 40013;

    public static $statusTexts = [
        self::DEFAULT_ERR_CODE => 'Bad request.',
        self::CREATE_ROOM_FAIL => 'Create room has fail.',
        self::CAN_NOT_ADD_MEMBER => 'Can not add member to room.',
        self::ROOM_NOT_FOUND => 'Room not found.',
        self::USER_NOT_MEMBER => 'User is not a member of the room.',
        self::ROOM_ALREADY_PINNED => 'Room is already  pinned.',
        self::ROOM_ALREADY_UNPINNED => 'Room is already  un-pinned.',
        self::FORBIDDEN => 'Forbidden.',
        self::MEMBER_ALREADY => 'Member already in this room.',
        self::SEND_MESSAGE_FAIL => 'Send message has fail.',
        self::DELETE_MESSAGE_FAIL => 'Delete message fail.',
        self::CREATE_SEEN_MESSAGE_FAIL => 'Create seen message has fail.',
        self::MEMBER_NOT_IN_ROOM => 'Only support to change room name if user must belong to this group.',
        self::ROOM_NOT_GROUP => 'Only support to change room name if it is public group and not private type.',

    ];

    protected function getDefaultCode()
    {
        return self::DEFAULT_ERR_CODE;
    }

    protected function getResponseCodeHash()
    {
        return self::$statusTexts;
    }
}
