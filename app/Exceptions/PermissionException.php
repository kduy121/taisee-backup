<?php

namespace App\Exceptions;

class PermissionException extends AppException
{
    public const DEFAULT_ERR_CODE = 300000;

    public const NOT_FOUND = 300001;

    public static $statusTexts = [
        self::NOT_FOUND => 'Permission not found.',
    ];

    protected function getDefaultCode()
    {
        return self::DEFAULT_ERR_CODE;
    }

    protected function getResponseCodeHash()
    {
        return self::$statusTexts;
    }
}
