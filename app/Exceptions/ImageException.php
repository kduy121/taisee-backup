<?php

namespace App\Exceptions;

class ImageException extends AppException
{
    public const DEFAULT_ERR_CODE = 70000;

    public const FILE_IS_NOT_BASE64 = 70001;

    public const MAXSIZE_UPLOAD = 70002;

    public static $statusTexts = [
        self::DEFAULT_ERR_CODE => 'Bad request.',
        self::FILE_IS_NOT_BASE64 => 'File content is not base64.',
        self::MAXSIZE_UPLOAD => 'Max size upload.',
    ];

    protected function getDefaultCode()
    {
        return self::DEFAULT_ERR_CODE;
    }

    protected function getResponseCodeHash()
    {
        return self::$statusTexts;
    }
}
