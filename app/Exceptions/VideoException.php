<?php

namespace App\Exceptions;

class VideoException extends AppException
{
    public const DEFAULT_ERR_CODE = 50000;

    public const VIDEO_CREATE_ERR = 50001;

    public const VIDEO_NOT_FOUND = 50002;

    public const VIDEO_FORBIDDEN = 50003;

    public const VIDEO_ALREADY_EXISTS = 50004;

    public const NOT_AUTHOR = 50005;

    public const FAILED_PUBLISH_VIDEO = 50006;

    public const VIDEO_DRAFT_NOT_FOUND = 50007;

    public const DELETE_VIDEO_DRAFT_FAIL = 50008;

    public const SYNC_HASHTAG_FAIL = 50009;

    public static $statusTexts = [
        self::DEFAULT_ERR_CODE => 'Bad request.',
        self::VIDEO_CREATE_ERR => 'Create has fail.',
        self::VIDEO_NOT_FOUND => 'Video not found.',
        self::VIDEO_FORBIDDEN => 'Video Forbidden.',
        self::VIDEO_ALREADY_EXISTS => 'Video is already exists.',
        self::NOT_AUTHOR => 'You are not the author of this video.',
        self::FAILED_PUBLISH_VIDEO => 'Failed to publish videos: ',
        self::VIDEO_DRAFT_NOT_FOUND => 'Video draft not found.',
        self::DELETE_VIDEO_DRAFT_FAIL => 'Video draft delete fail.',
        self::SYNC_HASHTAG_FAIL => 'Sync hashtag has fail.',
    ];

    protected function getDefaultCode()
    {
        return self::DEFAULT_ERR_CODE;
    }

    protected function getResponseCodeHash()
    {
        return self::$statusTexts;
    }
}
