<?php

namespace App\Exceptions;

class CommentLivestreamException extends AppException
{
    public const DEFAULT_ERR_CODE = 90000;

    public const COMMENT_FORBIDDEN = 90001;

    public const CREATE_FAIL = 90002;

    public const FORBIDDEN = 90003;

    public static $statusTexts = [
        self::DEFAULT_ERR_CODE => 'Bad request.',
        self::COMMENT_FORBIDDEN => 'Forbidden.',
        self::CREATE_FAIL => 'Create livestream has fail.',
        self::FORBIDDEN => 'Forbidden.',
    ];

    protected function getDefaultCode()
    {
        return self::DEFAULT_ERR_CODE;
    }

    protected function getResponseCodeHash()
    {
        return self::$statusTexts;
    }
}
