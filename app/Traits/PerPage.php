<?php

namespace App\Traits;

trait PerPage
{
    protected static function getPerPage(int $perPage = null)
    {
        $perPageItem = config('constants.per_page_item');

        if (! isset($perPage) || $perPage > $perPageItem) {
            $perPage = $perPageItem;
        }

        return $perPage;
    }
}
