<?php

namespace App\Transformers;

use App\Models\User;

class FollowerTransformer extends TransformerAbstract
{
    public static function transform(User $user)
    {
        return [
            'id' => $user->id,
            'name' => $user->name,
            'email' => $user->email,
            'socialite_id' => $user->socialite_id,
            'socialite_type' => $user->socialite_type,
            'date_of_birth' => $user->date_of_birth,
            'gender' => $user->gender,
            'user_name' => $user->user_name,
            'bio' => $user->bio,
            'avatar_url' => $user->avatar_url,
        ];
    }
}
