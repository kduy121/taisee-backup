<?php

namespace App\Transformers;

use App\Models\Role;

class RoleTransformer extends TransformerAbstract
{
    public static function transform(Role $model)
    {
        return [
            'id' => $model->id,
            'name' => $model->name,
            'guard_name' => $model->guard_name,
            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at,
            'role_has_permission' => $model->permissions,
        ];
    }
}
