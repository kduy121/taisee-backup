<?php

namespace App\Transformers;

use App\Models\Room;
use Illuminate\Support\Facades\Auth;

class RoomTransformer extends TransformerAbstract
{
    public static function transform(Room $room)
    {
        $user = Auth::user();
        $roomMember = $room->members->where('id', $user->id)->toArray();
        $userSeens = $room->userSeens;

        if (count($roomMember)) {
            $roomMember = array_shift($roomMember);
            $pinned = $roomMember['pivot']['pinned'];
        }

        return [
            'id' => $room->id,
            'created_at' => $room->created_at,
            'updated_at' => $room->updated_at,
            'name' => $room->name,
            'user_id' => $room->user_id,
            'type' => $room->type,
            'total_member' => $room->total_member,
            'avatar_ids' => $room->avatar_ids,
            'thumb_ids' => $room->thumb_ids,
            'init_members' => $room->init_members,
            'init_total_member' => $room->init_total_member,
            'pinned' => $pinned ?? 0,
            'room_member' => $room->members->map(function ($members) {
                $totalFollowing = $members->following->count();
                $totalFollowers = $members->followers->count();

                return [
                    'id' => $members->id,
                    'name' => $members->name,
                    'user_name' => $members->user_name,
                    'email' => $members->email,
                    'avatar_url' => $members->avatar_url,
                    'is_admin' => $members->pivot->is_admin,
                    'total_following' => $totalFollowing,
                    'total_follower' => $totalFollowers,
                ];
            }),
            'messages' => $room->messages,
            'user_seen' => $userSeens->map(function ($members) {
                return [
                    'user_id' => $members['user_id'],
                    'seen_at' => $members['seen_at'],
                ];
            }),
        ];
    }
}
