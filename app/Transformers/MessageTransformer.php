<?php

namespace App\Transformers;

use App\Models\Message;

class MessageTransformer extends TransformerAbstract
{
    public static function transform(Message $message)
    {
        $user = $message->user;
        $room = $message->room()->first();
        $userSeens = $room->userSeens;

        return [
            'id' => $message->id,
            'created_at' => $message->created_at,
            'updated_at' => $message->created_at,
            'user_id' => $message->user_id,
            'user_info' => [
                'user_id' => $message->user_id,
                'user_name' => $user->user_name,
                'name' => $user->name,
                'avatar_url' => $user->avatar_url,
            ],
            'room_id' => $message->room_id,
            'content' => $message->content,
            'timestamp' => $message->timestamp,
            'user_seen' => $userSeens->map(function ($members) {
                return [
                    'user_id' => $members['user_id'],
                    'seen_at' => $members['seen_at'],
                ];
            }),
        ];
    }
}
