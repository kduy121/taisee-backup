<?php

namespace App\Transformers;

use App\Models\User;
use App\Models\Video;
use Illuminate\Support\Facades\Auth;

class UserTransformer extends TransformerAbstract
{
    public static function transform(User $user)
    {
        $user = $user->load('videos.likes', 'followers', 'following', 'interests', 'areas', 'collections');
        $rootUser = Auth::guard('api')->user() ?? null;

        $totalLike = Video::join('likes', 'videos.id', '=', 'likes.video_id')
            ->where('videos.user_id', $user->id)
            ->count();

        $totalFollowing = $user->following->count();
        $totalFollowers = $user->followers->count();

        $follower = null;
        $following = null;

        if ($rootUser) {
            $follower = $user->followers->where('id', $rootUser->id)->first();
            $following = $user->following->where('id', $rootUser->id)->first();
        }

        return [
            'id' => $user->id,
            'name' => $user->name,
            'email' => $user->email,
            'socialite_id' => $user->socialite_id,
            'socialite_type' => $user->socialite_type,
            'date_of_birth' => $user->date_of_birth,
            'gender' => $user->gender,
            'user_name' => $user->user_name,
            'bio' => $user->bio,
            'avatar_url' => $user->avatar_url,
            'total_like' => $totalLike,
            'total_following' => $totalFollowing,
            'total_follower' => $totalFollowers,
            'interests' => $user->interests,
            'areas' => $user->areas,
            'videos' => $user->videos,
            'follower' => is_null($follower) ? null : $follower->id,
            'following' => is_null($following) ? null : $following->id,
            'collections' => $user->collections,
            'is_show_trending' => isset($user->isShowTrending) ? $user->isShowTrending : false,
            'has_friend_video' => isset($user->hasFriendVideo) ? $user->hasFriendVideo : false,
            'has_friend' => isset($user->hasFriend) ? $user->hasFriend : false,
            'count_draft' => isset($user->countDraft) ? $user->countDraft : 0,
            'count_msg' => isset($user->countMsg) ? $user->countMsg : 0,
            'is_live' => $user->is_live,
            'status' => $user->status,
            'likes_count' => $user->likes_count ?? null,
            'videos_count' => $user->videos->count() ?? null,
            'lang' => $user->lang,
        ];
    }
}
