<?php

namespace App\Transformers;

class CommentLivestreamTransformer extends TransformerAbstract
{
    public static function transform($comment)
    {
        if (! $comment) {
            return [];
        }

        return [
            'id' => $comment->id,
            'created_at' => $comment->created_at,
            'user_id' => $comment->user_id,
            'livestream_id' => $comment->livestream_id,
            'comment' => $comment->comment,
        ];
    }
}
