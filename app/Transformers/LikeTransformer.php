<?php

namespace App\Transformers;

class LikeTransformer extends TransformerAbstract
{
    public static function transform($model)
    {
        return [
            'like' => count($model) ? true : false,
        ];
    }
}
