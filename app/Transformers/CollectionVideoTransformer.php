<?php

namespace App\Transformers;

use App\Models\CollectionVideo;
use App\Models\Video;

class CollectionVideoTransformer extends TransformerAbstract
{
    public static function transform(CollectionVideo $model)
    {
        $video = $model->video;
        if (is_null($video)) {
            $video = new Video();
        }

        return [
            'id' => $video->id,
            'user_id' => $video->user_id,
            'title' => $video->title,
            'description' => $video->description,
            'thumbnail' => $video->thumbnail,
            'status' => $video->status,
            'streaming_path' => $video->streaming_path,
            'meta' => $video->meta,
            'author' => [
                'id' => $video->user_id,
                'name' => $$video->user->name ?? '',
                'user_name' => $video->user->user_name ?? '',
                'avatar_url' => $video->user->avatar_url ?? '',
            ],
        ];
    }
}
