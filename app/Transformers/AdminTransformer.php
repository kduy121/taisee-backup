<?php

namespace App\Transformers;

use App\Models\Admin;

class AdminTransformer extends TransformerAbstract
{
    public static function transform(Admin $model)
    {
        return [
            'id' => $model->id,
            'name' => $model->name,
            'email' => $model->email,
            'status' => $model->status,
            'avatar_url' => $model->avatar_url,
        ];
    }
}
