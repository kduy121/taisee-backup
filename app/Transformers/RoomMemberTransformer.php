<?php

namespace App\Transformers;

use App\Models\RoomMember;

class RoomMemberTransformer extends TransformerAbstract
{
    public static function transform(RoomMember $roomMember)
    {
        return [
            'room_id' => $roomMember->room_id,
            'user_id' => $roomMember->user_id,
            'is_admin' => $roomMember->is_admin,
            'created_at' => $roomMember->created_at,
            'user' => $roomMember->user,
        ];
    }
}
