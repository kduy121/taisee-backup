<?php

namespace App\Transformers;

use App\Enums\VideoStatus;
use App\Models\VideoDraft;

class VideoDraftTransformer extends TransformerAbstract
{
    public static function transform(VideoDraft $video)
    {
        $name = $video->user->name;
        $user_name = $video->user->user_name;
        $avatarUrl = $video->user->avatar_url;

        return [
            'author' => [
                'id' => $video->user_id,
                'name' => $name ?? '',
                'user_name' => $user_name ?? '',
                'follower' => null,
                'following' => null,
                'avatar_url' => $avatarUrl ?? '',
            ],
            'video' => [
                'id' => $video['id'],
                'like' => null,
                'is_bookmark' => false,
                'total_like' => null,
                'total_comment' => null,
                'total_bookmark' => null,
                'thumbnail' => $video->thumbnail,
                'description' => $video->description,
                'playAddr' => $video->streaming_path,
                'title' => $video->title,
                'meta' => $video->meta,
                'status' => VideoStatus::Draft->value,
                'video_privacy' => $video->video_privacy,
                'created_at' => $video->created_at,
                'updated_at' => $video->updated_at,
            ],
        ];
    }
}
