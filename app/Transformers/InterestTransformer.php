<?php

namespace App\Transformers;

use App\Models\Interest;

class InterestTransformer extends TransformerAbstract
{
    public static function transform(Interest $model)
    {
        return [
            'id' => $model->id,
            'name' => $model->name,
        ];
    }
}
