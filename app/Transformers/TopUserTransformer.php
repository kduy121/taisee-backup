<?php

namespace App\Transformers;

use App\Models\User;

class TopUserTransformer extends TransformerAbstract
{
    public function transform(User $user)
    {
        return [
            'id' => $user->id,
            'name' => $user->name,
            'email' => $user->email,
            'user_name' => $user->user_name,
            'avatar_url' => $user->avatar_url,
            'in_room' => $user->in_room,
        ];
    }
}
