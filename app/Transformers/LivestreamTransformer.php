<?php

namespace App\Transformers;

class LivestreamTransformer extends TransformerAbstract
{
    public static function transform($livestream)
    {
        return [
            'author' => [
                'id' => $livestream->user_id,
                'name' => $livestream->user->name ?? '',
                'user_name' => $livestream->user->user_name ?? '',
                'follower' => null,
                'following' => null,
                'avatar_url' => $livestream->user->avatarUrl ?? '',
            ],
            'video' => [
                'id' => $livestream->id,
                'like' => null,
                'is_bookmark' => null,
                'total_like' => null,
                'total_comment' => count($livestream->comments) ?? 0,
                'total_bookmark' => null,
                'thumbnail' => $livestream->cover,
                'description' => $livestream->introduction,
                'playAddr' => $livestream->streaming_path,
                'title' => $livestream->title,
                'meta' => null,
                'status' => $livestream->status,
                'created_at' => $livestream->created_at,
                'updated_at' => $livestream->updated_at,
            ],
        ];
    }
}
