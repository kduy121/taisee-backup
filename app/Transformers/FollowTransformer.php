<?php

namespace App\Transformers;

use App\Models\User;

class FollowTransformer extends TransformerAbstract
{
    public static function transform($followInfo)
    {
        $follow = isset($followInfo['follow']) ? $followInfo['follow'] : null;
        $followBack = isset($followInfo['follow_back']) ? $followInfo['follow_back'] : null;
        $authorVideo = User::with('following')->find($follow->following_id);

        return [
            'author' => [
                'id' => $authorVideo->id,
                'name' => $authorVideo->name ?? null,
                'user_name' => $authorVideo->user_name ?? null,
                'avatar_url' => $authorVideo->avatar_url ?? null,
                'follower' => ! empty($follow) ? $follow->follower_id : null,
                'following' => ! empty($followBack) ? $followBack->following_id : null,
            ],
        ];
    }
}
