<?php

namespace App\Transformers;

use App\Models\User;
use App\Models\Video;
use Illuminate\Support\Facades\Auth;

class VideoTransformer extends TransformerAbstract
{
    public static function transform(Video $video)
    {
        $user = Auth::guard('api')->user();
        $like = [];
        $follower = null;
        $following = null;
        $name = $video->user->name;
        $user_name = $video->user->user_name;
        $avatarUrl = $video->user->avatar_url;

        if ($user) {
            $like = $user->likes->where('video_id', $video->id);
            $authorVideo = User::with('followers', 'following')->find($video->user_id);
            $follower = $authorVideo->followers->where('id', $user->id)->first();
            $following = $authorVideo->following->where('id', $user->id)->first();

            if (count($video->collections) || count($video->collectionVideos)) {
                $isSave = true;
            }
        }

        // Total likes + comments
        $totalLike = $video->likes->count();
        $totalComment = $video->comments->count();

        return [
            'author' => [
                'id' => $video->user_id,
                'name' => $name ?? '',
                'user_name' => $user_name ?? '',
                'follower' => is_null($follower) ? null : $follower->id,
                'following' => is_null($following) ? null : $following->id,
                'avatar_url' => $avatarUrl ?? '',
                'is_live' => $video->user->is_live,
            ],
            'video' => [
                'id' => $video['id'],
                'like' => count($like) ? true : false,
                'is_bookmark' => isset($video->is_bookmark) ? $video->is_bookmark : false,
                'total_like' => $totalLike,
                'total_comment' => $totalComment,
                'total_bookmark' => isset($video->total_bookmark) ? $video->total_bookmark : 0,
                'thumbnail' => $video->thumbnail,
                'description' => $video->description,
                'playAddr' => $video->streaming_path,
                'title' => $video->title,
                'meta' => $video->meta,
                'status' => $video->status,
                'created_at' => $video->created_at,
                'updated_at' => $video->updated_at,
            ],
        ];
    }
}
