<?php

namespace App\Transformers;

use App\Models\Area;

class AreaTransformer extends TransformerAbstract
{
    public static function transform(Area $model)
    {
        $data = $model->address['province'];

        return $data;
    }
}
