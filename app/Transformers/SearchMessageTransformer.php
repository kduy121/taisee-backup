<?php

namespace App\Transformers;

class SearchMessageTransformer extends TransformerAbstract
{
    public static function transform(array $data)
    {
        return [
            'users' => $data['users'],
            'messages' => $data['messages'],
        ];
    }
}
