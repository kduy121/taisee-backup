<?php

namespace App\Transformers;

use App\Models\Collection;

class CollectionTransformer extends TransformerAbstract
{
    public static function transform(Collection $collection)
    {
        return [
            'id' => $collection->id,
            'name' => $collection->name,
            'user_id' => $collection->user_id,
            'public' => $collection->public,
            'thumbnail' => $collection->thumbnail,
            'video_count' => $collection->video_count,
            'created_at' => $collection->created_at,
            'updated_at' => $collection->created_at,
        ];
    }
}
