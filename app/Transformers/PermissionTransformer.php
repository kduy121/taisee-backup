<?php

namespace App\Transformers;

use App\Models\Permission;

class PermissionTransformer extends TransformerAbstract
{
    public static function transform(Permission $model)
    {
        return [
            'id' => $model->id,
            'name' => $model->name,
            'guard_name' => $model->guard_name,
            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at,
        ];
    }
}
