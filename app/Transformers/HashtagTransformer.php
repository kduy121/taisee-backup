<?php

namespace App\Transformers;

use App\Models\Hashtag;

class HashtagTransformer extends TransformerAbstract
{
    public static function transform(Hashtag $hashtag)
    {
        return [
            'id' => $hashtag->id,
            'created_at' => $hashtag->created_at,
            'name' => "#$hashtag->name",
            'total_video' => $hashtag->videos()->count(),
        ];
    }
}
