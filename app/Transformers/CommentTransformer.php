<?php

namespace App\Transformers;

use App\Models\Comment;

class CommentTransformer extends TransformerAbstract
{
    public static function transform(Comment $model)
    {
        return [
            'id' => $model->id,
            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at,
            'video_id' => $model->video_id,
            'user_id' => $model->user_id,
            'user_name' => $model->user->user_name,
            'name' => $model->user->name,
            'avatar_url' => $model->user->avatar_url,
            'comment' => $model->comment,
        ];
    }
}
