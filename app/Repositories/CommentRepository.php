<?php

namespace App\Repositories;

use App\Enums\VideoStatus;
use App\Exceptions\CustomModelNotFoundException;
use App\Models\Comment;

class CommentRepository extends BaseRepository
{
    protected $createRules = [
    ];

    public function getModel()
    {
        return Comment::class;
    }

    public function checkComment(string $commentId, string $videoId, string $userId): Comment
    {
        $comment = Comment::where([
            ['id', $commentId],
            ['video_id', $videoId],
            ['user_id', $userId],
        ])->first();

        if (! $comment) {
            throw new CustomModelNotFoundException('Comment id not found.');
        }

        if ($comment->video->status == VideoStatus::Private->value) {
            throw new CustomModelNotFoundException('Video not found.');
        }

        return $comment;
    }

    public function searchByVideoId($videoId, $perPage)
    {
        $perPage = is_null($perPage) ? self::PER_PAGE_ITEM : $perPage;
        $comment = Comment::with('user', 'video')->where('video_id', $videoId)
            ->orderBy('created_at', 'desc')->paginate($perPage);

        return $comment;
    }
}
