<?php

namespace App\Repositories;

use App\Models\Admin;
use App\Enums\SocialiteType;
use App\Enums\VideoStatus;
use App\Models\User;
use App\Traits\PerPage;

class UserRepository extends BaseRepository
{
    use PerPage;

    protected $createRules = [
    ];

    public function getModel()
    {
        return User::class;
    }

    public function index(array $data)
    {

    }

    public function show(array $data)
    {

    }

    public function create(array $data)
    {

    }

    public function delete($name)
    {

    }

    public function search(string $name, ?int $perPage)
    {
        $perPage = is_null($perPage) ? self::PER_PAGE_ITEM : $perPage;
        $users = User::where(function ($query) use ($name) {
            $query->where('name', 'LIKE', '%'.$name.'%')
                ->where('socialite_type', '!=', SocialiteType::GUEST->name)
                ->orWhere('user_name', 'LIKE', '%'.$name.'%');
        })->paginate($perPage);

        return $users;
    }

    public function getFollower(User $user, $perPage, $page)
    {
        if (! isset($perPage) || $perPage > 25) {
            $perPage = self::PER_PAGE_ITEM;
        }

        $users = $user->followers()->orderbyDesc('followers.created_at');

        return $this->customPaginate($users, $perPage, $page);
    }

    public function getFollowing(User $user, $perPage, $page)
    {
        if (! isset($perPage) || $perPage > 25) {
            $perPage = self::PER_PAGE_ITEM;
        }

        $users = $user->following()->orderbyDesc('followers.created_at');

        return $this->customPaginate($users, $perPage, $page);
    }

    public function getUserTrending($perPage, $userLogin = null, int $page)
    {
        $perPage = $this->getPerPage($perPage);
        $users = $this->query()->whereHas('videos', function ($query) {
            $query->where('status', VideoStatus::Public->value);
        });

        if ($userLogin) {
            $users->where('id', '!=', $userLogin->id);
        }

        $users = $this->customPaginate($users, $perPage, $page);

        $users->map(function ($user, $key) {
            $user->videos = $user->latestVideo;
        });

        return $users;
    }

    public function searchUserBE(string $name, ?int $perPage)
    {
        $perPage = is_null($perPage) ? self::PER_PAGE_ITEM : $perPage;
        $users = User::where(function ($query) use ($name) {
            $query->where('name', 'LIKE', '%'.$name.'%')
                ->orWhere('user_name', 'LIKE', '%'.$name.'%')
                ->orWhere('email', 'LIKE', '%'.$name.'%');
        })->paginate($perPage);

        return $users;
    }

    public function updateUser(User $user, array $data)
    {
        return $this->update($user, $data);
    }

    public function getAdmin(string $keyword = null, $perPage = null)
    {
        $perPage = is_null($perPage) ? self::PER_PAGE_ITEM : $perPage;

        if (!is_null($keyword)) {
            return Admin::where(function ($query) use ($keyword) {
                $query->where('name', 'LIKE', '%'.$keyword.'%')
                    ->orWhere('email', 'LIKE', '%'.$keyword.'%');
            })->paginate($perPage);
        } 

        return Admin::get();
    }

    public function getAdminById(string $id)
    {
        return Admin::find($id);
    }

    public function updateAdmin(Admin $admin, array $data)
    {
        return $admin->update($data);
    }

    public function getTopUser(int $perPage = null)
    {
        $perPage = $this->getPerPage($perPage);
        $users = User::with('videos')
            ->select(\DB::raw('COUNT(likes.id) AS likes_count'), 'users.*')
            ->join('videos', 'users.id', '=', 'videos.user_id')
            ->join('likes', 'videos.id', '=', 'likes.video_id')
            ->groupBy('users.id')
            ->orderByDesc('likes_count')
            ->paginate($perPage);

        return $users;
    }
}
