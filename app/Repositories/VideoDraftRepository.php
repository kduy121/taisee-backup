<?php

namespace App\Repositories;

use App\Models\User;
use App\Models\VideoDraft;
use App\Traits\PerPage;

class VideoDraftRepository extends BaseRepository
{
    use PerPage;

    public function getModel()
    {
        return VideoDraft::class;
    }

    public function getByColumn($value, $column_name = 'id')
    {
        return $this->query()->where($column_name, $value);
    }

    public function getVideoDrafts(User $user, $perPage, int $page)
    {
        $perPage = $this->getPerPage($perPage);

        $videoDrafts = VideoDraft::where('user_id', $user->id)
            ->orderBy('created_at', 'desc');

        $videoDrafts = $this->customPaginate($videoDrafts, $perPage, $page);

        return $videoDrafts;
    }

    public function countVideoDrafts(User $user)
    {
        $videoCount = VideoDraft::where('user_id', $user->id)->count();

        return $videoCount;
    }

    public function getDetailVideoDraft(string $videoId)
    {
        return VideoDraft::find($videoId);
    }

    public function updateVideoDraft(VideoDraft $videoDraft, array $data)
    {
        $dataUpdate = [
            'title' => $data['title'] ?? null,
            'description' => $data['description'] ?? null,
            'video_privacy' => $data['video_privacy'],
        ];

        $videoDraft->update($dataUpdate);

        return $videoDraft;
    }

    public function deleteVideoDraft(VideoDraft $videoDraft)
    {
        return $videoDraft->delete();
    }
}
