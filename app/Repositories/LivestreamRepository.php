<?php

namespace App\Repositories;

use App\Enums\LivestreamStatus;
use App\Exceptions\LivestreamException;
use App\Models\Livestream;
use App\Models\User;
use App\Traits\PerPage;

class LivestreamRepository extends BaseRepository
{
    use PerPage;

    public function getModel()
    {
        return Livestream::class;
    }

    public function getAllLivestreams(int $perPage = null, $page)
    {
        $perPage = $this->getPerPage($perPage);

        $livestream = Livestream::where('status', LivestreamStatus::Public->value)
            ->orderBy('created_at', 'desc');

        return $this->customPaginate($livestream, $perPage, $page);
    }

    public function createLivestream(array $data, User $user)
    {
        $isLive = ($data['status'] == LivestreamStatus::Public->value);

        try {
            $livestream = Livestream::updateOrCreate(
                [
                    'streaming_path' => $data['streaming_path'],
                    'user_id' => $user->id,
                ],
                [
                    'cover' => $data['cover'],
                    'title' => $data['title'],
                    'introduction' => $data['introduction'],
                    'status' => $data['status'],
                    // 'pause_time' => $data['pause_time'],
                    'livestream_duration' => $data['livestream_duration'],
                ]
            );

            $user->is_live = $isLive;
            $user->save();

            return $livestream;
        } catch (\Throwable $th) {
            throw new LivestreamException(LivestreamException::CREATE_FAIL);
        }
    }

    public function getById(string $id)
    {
        return Livestream::find($id);
    }

    public function updateLivestream(array $data, User $user)
    {
        $isLive = ($data['status'] == LivestreamStatus::Public->value);
        $livestream = Livestream::find($data['livestream_id']);
        $livestream->update($data);

        $user->is_live = $isLive;
        $user->save();

        return $livestream;
    }

    public function validateData(string $livestreamId): bool
    {
        $livestream = Livestream::find($livestreamId);

        if (! $livestream) {
            throw new LivestreamException(LivestreamException::LIVESTREAM_NOT_FOUND);
        }

        return true;
    }
}
