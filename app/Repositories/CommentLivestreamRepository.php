<?php

namespace App\Repositories;

use App\Enums\LivestreamStatus;
use App\Exceptions\CommentLivestreamException;
use App\Exceptions\LivestreamException;
use App\Models\CommentLivestream;
use App\Models\Livestream;
use App\Traits\PerPage;

class CommentLivestreamRepository extends BaseRepository
{
    use PerPage;

    protected $createRules = [
    ];

    public function getModel()
    {
        return CommentLivestream::class;
    }

    public function getAllComments(int $perPage = null, string $livestreamId, int $page)
    {
        $perPage = $this->getPerPage($perPage);

        $comments = CommentLivestream::where('livestream_id', $livestreamId)->orderBy('created_at', 'desc');

        return $this->customPaginate($comments, $perPage, $page);
    }

    public function createComment(array $data)
    {
        $comment = $this->create($data);

        if (! $comment) {
            throw new CommentLivestreamException(CommentLivestreamException::CREATE_FAIL);
        }

        return $comment;
    }

    public function updateCommentLivestream(array $data)
    {
        $comment = CommentLivestream::find($data['comment_id']);
        $comment->update($data);

        return $comment;
    }

    public function validateData(string $userId, string $commentId): bool
    {
        $comment = CommentLivestream::where([
            ['user_id', $userId],
            ['id', $commentId],
        ])->first();

        if (! $comment) {
            throw new CommentLivestreamException(CommentLivestreamException::COMMENT_FORBIDDEN);
        }

        $this->validateLiveStream($comment->livestream_id, $userId);

        return true;
    }

    public function validateLiveStream(string $livestreamId, string $userId = null): bool
    {
        $livestream = Livestream::find($livestreamId);

        if ($livestream && $livestream->status == LivestreamStatus::Practice->value &&
            $userId != $livestream->user_id) {
            throw new CommentLivestreamException(CommentLivestreamException::FORBIDDEN);
        }

        if (! $livestream) {
            throw new LivestreamException(LivestreamException::LIVESTREAM_NOT_FOUND);
        }

        return true;
    }
}
