<?php

namespace App\Repositories;

use App\Exceptions\CustomModelNotFoundException;
use App\Models\User;

class SocialiteRepository extends BaseRepository
{
    protected $createRules = [
    ];

    public function getModel()
    {
        return User::class;
    }

    public function validateUserName(string $userName): void
    {
        $valdate = User::where('email', $userName)->first();

        if ($valdate) {
            throw new CustomModelNotFoundException('user name already exists.');
        }
    }
}
