<?php

namespace App\Repositories;

use App\Models\Hashtag;
use App\Traits\PerPage;

class HashtagRepository extends BaseRepository
{
    use PerPage;

    protected $createRules = [
    ];

    public function getModel()
    {
        return Hashtag::class;
    }

    public function getHashtag(string $hashtag = null, int $perPage = null, int $page)
    {
        $perPage = $this->getPerPage($perPage);

        if ($hashtag) {
            $query = $this->query()->where('name', 'LIKE', "%$hashtag%");
        } else {
            $query = $this->query();
        }

        $query->orderBy('updated_at', 'desc');

        return $this->customPaginate($query, $perPage, $page);
    }
    public function createHashtag(array $hashtags)
    {
        foreach ($hashtags as $name) {
            $data['name'] = $name;
            $condition['name'] = $name;
            $query = $this->query();

            $query->updateOrCreate($condition, $data);
        }

        return true;
    }

    public function getById(string $id)
    {
        $hashtag = $this->query()->find($id);

        return $hashtag;
    }

    public function updateHashtag(Hashtag $hashtag, array $data)
    {
        return $hashtag->update($data);
    }
}
