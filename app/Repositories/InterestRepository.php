<?php

namespace App\Repositories;

use App\Models\Interest;
use App\Models\User;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class InterestRepository extends BaseRepository
{
    protected $createRules = [
        'interest' => 'required',
    ];

    public function getModel()
    {
        return Interest::class;
    }

    public function index(array $data)
    {

    }

    public function show(array $data)
    {

    }

    public function create(array $data)
    {
        // $validator = $this->validate($data, $this->createRules);
        $user = Auth::user();

        // if ($validator->fails()) {
        //     $this->errors = $validator->errors()->all();

        //     return response()->json([
        //         'errors' => [
        //             $this->errors
        //         ]
        //     ], Response::HTTP_UNPROCESSABLE_ENTITY);
        // }

        // $interests = Interest::whereJsonContains('interest->fitness_health->', $data['interest'])->get();
        // $existingInterests = $interests->pluck('interest')->all();
        // $newInterests = array_diff($data['interest'], $existingInterests);

        // foreach ($newInterests as $interest) {
        //     $interest = Interest::create([
        //         'interest' => $interest,
        //         'created_by' => $user->id,
        //     ]);
        // }

        // return response()->json([
        //     'status' => 'Interest create success',
        //     'interest' => $newInterests,
        // ], Response::HTTP_CREATED);

        foreach ($data['interest'] as $value) {
            $key = $this->lowCase($value);

            $interestData = [];
            $interestData['interest'] = [
                'key' => $key,
                'ja' => '', // Todo
                'en' => $value,
            ];

            $interest = Interest::create([
                'interest' => $interestData,
                'created_by' => $user->id,
            ]);
        }

        return response()->json([
            'status' => 'Interest create success',
        ], Response::HTTP_CREATED);
    }

    public function attachData(User $user, array $data): void
    {
        if (! $data) {
            return;
        }

        $interests = Interest::where(function ($query) use ($data) {
            foreach ($data as $key) {
                $query->orWhereJsonContains('interest->interest->key', $key);
            }
        })->get();

        $user->interests()->syncWithoutDetaching($interests->pluck('id'));
    }

    public function lowCase(string $value)
    {
        return Str::of($value)
            ->lower()
            ->replaceMatches('/[^a-zA-Z0-9]+/', '_')
            ->trim('_')
            ->toString();
    }

    public function update($model, array $data)
    {

    }

    public function delete($name)
    {
        $interest = Interest::where('name', $name)->get();
        $user = auth()->user();

        if ($interest) {
            $user->interests()->detach($interest->id);
        }
    }
}
