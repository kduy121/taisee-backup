<?php

namespace App\Repositories;

use App\Exceptions\PermissionException;
use App\Models\Permission;
use App\Traits\PerPage;

class PermissionRepository extends BaseRepository
{
    use PerPage;

    protected $createRules = [
    ];

    public function getModel()
    {
        return Permission::class;
    }

    public function all(int $perPgae, int $page = 1)
    {
        $roles = $this->query();

        return $this->customPaginate($roles, $perPgae, $page);
    }

    public function find($id)
    {
        $role = Permission::find($id);

        if (! $role) {
            throw new PermissionException(PermissionException::NOT_FOUND);
        }

        return Permission::findOrFail($id);
    }

    public function create($data)
    {
        return Permission::create($data);
    }

    public function update($id, $data)
    {
        $role = $this->find($id);
        $role->update($data);

        return $role;
    }

    public function delete($id)
    {
        $role = $this->find($id);
        $role->delete();
    }
}
