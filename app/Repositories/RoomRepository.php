<?php

namespace App\Repositories;

use App\Enums\ChatType;
use App\Exceptions\ChatException;
use App\Exceptions\UserException;
use App\Models\Room;
use App\Models\RoomMember;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class RoomRepository extends BaseRepository
{
    public function getModel()
    {
        return Room::class;
    }

    public function getRooms($perPage, int $page)
    {
        $userId = Auth::user()->id;

        if (! isset($perPage) || $perPage > 25) {
            $perPage = self::PER_PAGE_ITEM;
        }

        $roomId = Room::select('rooms.*')
            ->join('room_members', 'rooms.id', '=', 'room_members.room_id')
            // ->join('messages', 'rooms.id', '=', 'messages.room_id')
            ->join('messages', 'rooms.id', '=', 'rooms.id')
            ->where(function ($query) {
                $query->where('rooms.type', 'GROUP')
                    ->orWhere(function ($query) {
                        $query->where('rooms.type', 'PRIVATE')
                            ->where('rooms.id', '=', \DB::raw('messages.room_id'));
                    });
            })
            ->where('room_members.user_id', $userId)
            ->groupBy('rooms.id', 'room_members.pinned')
            ->orderBy('room_members.pinned', 'desc')
            ->orderBy('rooms.updated_at', 'desc')
            // ->orderBy('messages.updated_at', 'desc')
            ->get()
            ->pluck('id')
            ->toArray();

        $filteredRoomId = array_unique($roomId);

        $rooms = Room::whereIn('id', $filteredRoomId)
            ->orderByRaw("FIELD(id, '".implode("','", $filteredRoomId)."')");

        $rooms = $this->customPaginate($rooms, $perPage, $page);

        foreach ($rooms as $room) {
            $room->messages = [$room->messages()->orderBy('updated_at', 'desc')->first()];
            $room->members = $room->members;
        }

        return $rooms;
    }

    public function checkAuthor(string $roomId): bool
    {
        $userId = Auth::user()->id;

        $isAuthor = Room::where([
            ['user_id', $userId],
            ['id', $roomId],
        ])->first();

        if (! $isAuthor) {
            throw new ChatException(ChatException::FORBIDDEN);
        }

        return true;
    }

    public function getUserName($userId)
    {
        $user = User::find($userId);

        if (! $user) {
            throw new UserException(UserException::USER_NOT_FOUND);
        }

        return $user->name;
    }

    public function syncData(Room $room, array $members)
    {
        try {
            \DB::beginTransaction();

            $room->members()->attach($room->user_id, [
                'is_admin' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);
            $room->members()->attach($members, [
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);

            \DB::commit();
        } catch (\Throwable $th) {
            \DB::rollback();
            throw new ChatException(ChatException::CAN_NOT_ADD_MEMBER);
        }
    }

    public function leaveRoom(string $roomId)
    {
        $userId = Auth::user()->id;
        $room = $this->find($roomId);

        if ($room) {
            $isMember = $room->members->contains('id', $userId);

            if ($isMember) {
                $room->members()->detach($userId);
                $room->decrement('total_member');

                return response()->json(['message' => 'Leave room successfully.']);
            }

            throw new ChatException(ChatException::USER_NOT_MEMBER);
        }

        throw new ChatException(ChatException::ROOM_NOT_FOUND);
    }

    public function addMember(string $roomId, array $memberIds)
    {
        $room = $this->find($roomId);

        if (empty($room)) {
            throw new ChatException(ChatException::ROOM_NOT_FOUND);
        }

        foreach ($memberIds as $memberId) {
            $room->members()->attach($memberId);
            $room->increment('total_member');
        }

        return $room;
    }

    public function checkMember(string $roomId, string $memberId): bool
    {
        $user = User::find($memberId);
        if (! $user) {
            throw new UserException(UserException::USER_NOT_FOUND);
        }

        $isInRoom = RoomMember::where([
            ['user_id', $memberId],
            ['room_id', $roomId],
        ])->exists();

        if (! $isInRoom) {
            return true;
        }

        throw new ChatException(ChatException::MEMBER_ALREADY);
    }

    public function pinRoom(string $roomId, bool $pinned = true)
    {
        $userId = Auth::user()->id;

        if ($pinned) {
            $maxPinned = RoomMember::select('pinned')->orderBy('pinned', 'desc')->first();

            RoomMember::where([
                ['user_id', $userId],
                ['room_id', $roomId],
            ])->update([
                'pinned' => $maxPinned->pinned + 1,
            ]);
        } else {
            RoomMember::where([
                ['user_id', $userId],
                ['room_id', $roomId],
            ])->update([
                'pinned' => 0,
            ]);
        }
    }

    public function isGroupChat(string $roomId)
    {
        $room = Room::where([
            ['id', $roomId],
            ['type', ChatType::from('group')->name],
        ])->first();

        if ($room) {
            return $room;
        }

        return false;
    }

    public function checkMemberInRoom(string $roomId, string $memberId): bool
    {
        $user = User::find($memberId);

        if (! $user) {
            throw new UserException(UserException::USER_NOT_FOUND);
        }

        $isInRoom = RoomMember::where([
            ['user_id', $memberId],
            ['room_id', $roomId],
        ])->exists();

        if ($isInRoom) {
            return true;
        }

        return false;
    }

    public function getRoomDetail(string $roomId)
    {
        $room = Room::find($roomId);

        if ($room) {
            $room->members = $room->getFirstRoomMembers($room);
            $room->messages = [$room->messages()->latest('updated_at')->first()];

            // Init Members
            $initMembers = [];

            foreach ($room->thumb_ids as $userId) {
                $user = User::where('id', $userId)->first();
                $isAdmin = 0;
                if ($room->user_id == $userId) {
                    $isAdmin = 1;
                }

                if (! $isAdmin) {
                    $isMember = RoomMember::select('is_admin')->where([
                        ['room_id', $room->id],
                        ['user_id', $user->id],
                    ])->first();
                }

                if ($user) {
                    $initMembers[] = [
                        'id' => $user->id,
                        'name' => $user->name,
                        'email' => $user->email,
                        'is_admin' => $isMember->is_admin ?? $isAdmin,
                        'user_name' => $user->user_name,
                        'avatar_url' => $user->avatar_url,
                    ];
                }
            }

            $room->init_members = $initMembers;
        }

        return $room;
    }

    public function checkRoomPrivate(string $userId, string $memberId)
    {
        $adminMember = $this->query()
            ->join('room_members', 'rooms.id', '=', 'room_members.room_id')
            ->where('rooms.user_id', $userId)
            ->where([
                ['type', ChatType::from('private')->name],
                ['room_members.user_id', $memberId],
            ])->first();

        $member = $this->query()
            ->join('room_members', 'rooms.id', '=', 'room_members.room_id')
            ->where('rooms.user_id', $memberId)
            ->where([
                ['type', ChatType::from('private')->name],
                ['room_members.user_id', $userId],
            ])->first();

        if (! $adminMember && $member) {
            return $member;
        }

        if ($adminMember && ! $member) {
            return $adminMember;
        }
    }

    public function checkRoom(string $roomId): void
    {
        $room = Room::where('type', 'PRIVATE')->find($roomId);

        if ($room) {
            throw new ChatException(ChatException::CAN_NOT_ADD_MEMBER);
        }
    }

    public function checkRoomExist(string $roomId)
    {
        $room = Room::find($roomId);

        return $room;
    }
}
