<?php

namespace App\Repositories;

use App\Models\Area;
use App\Models\User;

class AreaRepository extends BaseRepository
{
    protected $createRules = [
    ];

    public function getModel()
    {
        return Area::class;
    }

    public function index(array $data)
    {

    }

    public function show(array $data)
    {

    }

    public function create(array $data)
    {

    }

    public function syncData(User $user, string $area): bool
    {
        $area = Area::whereJsonContains('address->province->en', $area)
            ->orWhereJsonContains('address->province->ja', $area)
            ->first();

        if ($area) {
            $user->areas()->sync($area->id);

            return true;
        }

        return false;
    }

    public function update($model, array $data)
    {

    }

    public function delete($name)
    {

    }
}
