<?php

namespace App\Repositories;

/**
 * Interface RepositoryContract.
 *
 * Modified from: https://github.com/kylenoland/laravel-base-repository
 */
interface RepositoryContract
{
    public function getModel();

    public function getNew(array $attributes = []);

    public function getByColumn($item, $column);

    public function getSelectData($field_name);

    public function getPaginated($limit = 25, array $with = null, array $condition = null, int $page, $order_by = 'created_at', $sort = 'desc');

    public function getCount();

    public function getErrors();

    public function getValidator();

    public function find($id);

    public function getListPaginated($filterOptions);

    public function filterAndSortForQuery($filterOptions);

    public function query();

    public function validate($data = [], $rules = []);

    public function create(array $data);

    public function delete($id);
}
