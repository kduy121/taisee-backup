<?php

namespace App\Repositories;

use App\Exceptions\GeneralException;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;
use Validator;

class BaseRepository implements RepositoryContract
{
    const PER_PAGE_ITEM = 25;

    const PAGINATE_DEFAULT = 0; // 1: default laravel

    protected $model;

    protected $table;

    protected $errors;

    protected $validator;

    protected $columns = [];

    protected $createRules = [];

    protected $updateRules = [];

    public function __construct()
    {
        $this->initModel();
        $this->table = $this->model->getTable();
        /**
         * Get the table column names.
         */
        $this->getColumns();

        if (empty($this->updateRules)) {
            $this->updateRules = $this->createRules;
        }
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        return $this->query()->get();
    }

    protected function initModel()
    {
        $cls = $this->getModel();

        if (! class_exists($cls)) {
            throw new \Exception("Class $cls is not found!");
        }

        return $this->model = new $cls();
    }

    public function getModel()
    {
        throw new \Exception('The model undefined! Need override getModel function!');
    }

    /**
     * Instantiate a new model instance.
     */
    public function getNew(array $attributes = [])
    {
        return $this->model->newInstance($attributes);
    }

    /**
     * Get Paginated.
     *
     * @param  string  $active
     * @param  string  $order_by
     * @param  string  $sort
     * @return mixed
     */
    public function getPaginated($per_page = self::PER_PAGE_ITEM, array $relation = null, array $condition = null, int $page,
        $order_by = 'created_at', $sort = 'desc'): LengthAwarePaginator
    {
        if ($relation) {
            $query = $this->query()->with($relation)->orderBy($order_by, $sort);
        }

        if ($condition) {
            $column = $condition['column'];
            $value = $condition['value'];
            $operations = $condition['operations'];
            $query = $this->query()->with($relation)->orderBy($order_by, $sort)->where($column, $operations, $value);
        }

        return $this->customPaginate($query, $per_page, $page);
    }

    /**
     * @return mixed
     */
    public function getCount()
    {
        return $this->query()->count();
    }

    /**
     * @return mixed
     */
    public function find($id)
    {
        return $this->query()->find($id);
    }

    /**
     * Find Record based on specific column.
     *
     * @param  string  $value
     * @param  string  $column_name
     * @return mixed
     */
    public function getByColumn($value, $column_name = 'id')
    {
        return $this->query()->where($column_name, $value)->first();
    }

    /**
     * @return \Closure|\Illuminate\Database\Query\Builder|\Illuminate\Database\Query\Expression|string $column
     */
    public function query()
    {
        $model = $this->getModel();

        return call_user_func($model.'::query');
    }

    /**
     * Generate drop-down select data with basic IDs.
     *
     * @param  string  $field_name
     * @return array
     */
    public function getSelectData($field_name = 'name')
    {
        $collection = $this->getAll();
        $model = $this->getModel();

        return call_user_func($model.'::getItems', $collection, $field_name);
    }

    /**
     * get error.
     */
    public function getErrors()
    {
        return $this->errors;
    }

    public function getValidator()
    {
        return $this->validator;
    }

    /**
     * Get the table columns.
     */
    private function getColumns()
    {
        /** @var \Illuminate\Database\MySqlConnection $connection */
        $this->columns = DB::getSchemaBuilder()->getColumnListing($this->table);
        if (empty($this->columns)) {
            $this->columns = $this->model->getFillable();
        }
        $this->columns = array_flip($this->columns);
    }

    /**
     * Popular data.
     *
     * @param  mixed  $model
     * @param  mixed  $data
     * @return Model
     *
     * @throws Exception
     */
    protected function populate(&$model, $data)
    {
        if (is_object($data)) {
            $data = (array) $data;
        }

        if (! is_array($data)) {
            throw new UndefinedDataException();
        }
        /**
         * Check if column is exist.
         */
        foreach ($data as $key => $value) {
            if (! isset($this->columns[$key])) {
                continue;
            }
            $model->{$key} = $value;
        }
    }

    /**
     * validate data with the rules.
     */
    public function validate($data = [], $rules = [])
    {
        return Validator::make($data, $rules);
    }

    public function create(array $data)
    {
        $validator = $this->validate($data, $this->createRules);
        if ($validator->fails()) {
            $this->errors = $validator->errors()->all();
            $this->validator = $validator;

            return false;
        }

        $model = $this->getNew();

        return \DB::transaction(function () use ($model, $data) {
            $this->populate($model, $data);

            if ($model->save()) {
                return $model;
            }

            throw new GeneralException(__('exceptions.backend.access.general.create_error'));
        });
    }

    public function update($model, array $data)
    {
        $validator = $this->validate($data, $this->updateRules);
        if ($validator->fails()) {
            $this->errors = $validator->errors()->all();
            $this->validator = $validator;

            return false;
        }

        return \DB::transaction(function () use ($model, $data) {
            if ($model->update($data)) {
                return $model;
            }
            throw new \Exception("Update failed for model ID: {$model->id}");
        });
    }

    public function delete($id)
    {
        if (empty($id)) {
            return false;
        }

        return $this->model->destroy($id);
    }

    public function getListPaginated($filterOptions)
    {
        $query = $this->query();

        return $query->paginate(self::PER_PAGE_ITEM);
    }

    public function filterAndSortForQuery($filterOptions)
    {
        $query = $this->query();
        if ($filterOptions) {
            if (isset($filterOptions['name'])) {
                $query->where('name', 'like', '%'.$filterOptions['name'].'%');
            }

            if (isset($filterOptions['status'])) {
                $query->where('status', $filterOptions['status']);
            }

            $sortValue = $filterOptions['sort'] ?? null;
            if (isset($sortValue)) {
                if ($sortValue === 'desc') {
                    $query->orderBy('sort', 'desc');
                } elseif ($sortValue === 'asc') {
                    $query->orderBy('sort', 'asc');
                } elseif (is_numeric($sortValue)) {
                    $query->where('sort', $filterOptions['sort']);
                }
            } else {
                $query->orderBy('id', 'desc');
            }
        } else {
            $query->orderBy('id', 'desc');
        }

        return $query;
    }

    public function getTableName()
    {
        return $this->table;
    }

    public function customPaginate($query, $perPage, $page)
    {
        if (self::PAGINATE_DEFAULT) {
            return $query->paginate($perPage);
        }

        $result = $query->skip(($page - 1) * $perPage)->take($perPage)->get();
        $total = $result->count();

        return new LengthAwarePaginator($result, $total, $perPage, $page, [
            'path' => LengthAwarePaginator::resolveCurrentPath(),
            'pageName' => 'page',
        ]);
    }
}
