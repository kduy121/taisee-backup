<?php

namespace App\Repositories;

use App\Exceptions\RoleException;
use App\Models\Role;
use App\Traits\PerPage;

class RoleRepository extends BaseRepository
{
    use PerPage;

    public function getModel()
    {
        return Role::class;
    }

    public function all(int $perPgae, int $page = 1)
    {
        $roles = $this->query();

        return $this->customPaginate($roles, $perPgae, $page);
    }

    public function find($id)
    {
        $role = Role::find($id);

        if (! $role) {
            throw new RoleException(RoleException::NOT_FOUND);
        }

        return Role::findOrFail($id);
    }

    public function create($data)
    {
        $role = Role::create($data);

        if (! empty($data['permission'])) {
            $role->syncPermissions($data['permission']);
        }

        return $role;
    }

    public function update($id, $data)
    {
        $role = $this->find($id);
        $role->update($data);
        $role->syncPermissions($data['permission']);

        return $role;
    }

    public function delete($id)
    {
        $role = $this->find($id);
        $role->delete();
    }
}
