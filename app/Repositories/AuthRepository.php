<?php

namespace App\Repositories;

use App\Exceptions\AuthException;
use App\Models\RefreshToken;
use App\Models\User;

class AuthRepository extends BaseRepository
{
    public function getModel()
    {
        return RefreshToken::class;
    }

    public function getRefreshToken(User $user, string $deviceId)
    {
        $res = $this->query()->where([
            ['device_id', $deviceId],
            ['user_id', $user->id],
        ])->first();

        return $res;
    }

    public function createRefreshToken(User $user, string $deviceId, string $refreshToken)
    {
        $conditions = [
            'user_id' => $user->id,
            'device_id' => $deviceId,
        ];

        $data = [
            'user_id' => $user->id,
            'device_id' => $deviceId,
            'refresh_token' => $refreshToken,
        ];

        try {
            RefreshToken::updateOrCreate($conditions, $data);
        } catch (\Throwable $th) {
            throw new AuthException(AuthException::CREATE_REFRESH_TOKEN_FAIL);
        }
    }

    public function deleteToken($refreshToken)
    {
        $res = $this->query()->where('refresh_token', $refreshToken)->delete();

        return $res;
    }

    public function getUser(string $userId): User
    {
        return User::find($userId);
    }
}
