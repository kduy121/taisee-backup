<?php

namespace App\Repositories;

use App\Exceptions\ChatException;
use App\Models\Message;
use App\Models\MessageDelete;
use App\Models\Room;
use App\Models\RoomMember;
use App\Models\User;
use App\Models\UserSeen;
use App\Traits\PerPage;
use Illuminate\Support\Facades\Auth;

class MessageRepository extends BaseRepository
{
    use PerPage;

    public function getModel()
    {
        return Message::class;
    }

    public function checkMember(string $roomId, string $memberId): bool
    {
        $isInRoom = RoomMember::where([
            ['user_id', $memberId],
            ['room_id', $roomId],
        ])->exists();

        if (! $isInRoom) {
            throw new ChatException(ChatException::USER_NOT_MEMBER);
        }

        return true;
    }

    public function checkRoom(string $roomId): bool
    {
        $isRoom = Room::find($roomId);

        if (! $isRoom) {
            throw new ChatException(ChatException::ROOM_NOT_FOUND);
        }

        return true;
    }

    public function getMessage(string $roomId, string $memberId, $perPage, int $page)
    {
        if (! isset($perPage) || $perPage > 25) {
            $perPage = self::PER_PAGE_ITEM;
        }

        // Check User Has Delete Message
        $messageDelete = MessageDelete::where([
            ['room_id', $roomId],
            ['user_id', $memberId],
        ])->first();

        if ($messageDelete) {
            $deletedAt = $messageDelete->deleted_at;
        }

        $query = Message::with('user', 'room', 'room.userSeens')->where('room_id', $roomId)
            ->orderBy('created_at', 'desc');

        if (isset($deletedAt)) {
            $query->where('timestamp', '>', $deletedAt);
        }

        $messages = $this->customPaginate($query, $perPage, $page);

        return $messages;
    }

    public function searchUser(string $keyword = null, int $limit = null, int $perPage = null, int $page = 1)
    {
        $userId = Auth::user()->id;
        $perPage = $this->getPerPage($perPage);

        $usersQuery = User::select('users.*')
            ->join('followers', 'users.id', '=', 'followers.following_id')
            ->where('followers.follower_id', $userId)
            ->where('followers.is_friend', true)
            ->orderBy('users.created_at', 'desc');

        if (isset($keyword)) {
            $users = $usersQuery->where(function ($query) use ($keyword) {
                $query->where('name', 'LIKE', "%$keyword%")
                    ->orWhere('user_name', 'LIKE', "%$keyword%");
            });
        }

        if (isset($limit)) {
            return $users = $usersQuery->limit($limit)->get();
        }

        if (isset($perPage)) {
            return $this->customPaginate($usersQuery, $perPage, $page);
        }

        $users = $usersQuery->get();

        return $users;
    }

    public function searchMessage(string $keyword, ?int $limit)
    {
        $userId = Auth::user()->id;
        $rooms = Room::with('members')->whereHas('members', function ($query) {
            $query->where('user_id', Auth::user()->id);
        })
            ->get();

        $RoomMessages = [];

        foreach ($rooms as $room) {
            $messages = $room->messages()
                ->where('content', 'LIKE', "%$keyword%")
                ->latest()
                ->limit(1)
                ->get();

            if ($messages->isNotEmpty()) {
                $room['messages'] = $messages;
                $room['room_member'] = $room['members'];
                unset($room['members']);
                $RoomMessages[] = $room;
            }
        }

        $RoomMessages = array_slice($RoomMessages, 0, $limit);

        return $RoomMessages;
    }

    public function topRooms(string $keyword, ?int $perPage, int $page)
    {
        $userId = Auth::user()->id;
        $perPage = is_null($perPage) ? self::PER_PAGE_ITEM : $perPage;

        $rooms = Room::whereHas('members', function ($query) use ($userId) {
            $query->where('user_id', $userId);
        })
            ->whereHas('messages', function ($query) use ($keyword) {
                $query->where('content', 'like', "%$keyword%");
            });

        $rooms = $this->customPaginate($rooms, $perPage, $page);

        foreach ($rooms as $room) {
            $room->messages = [$room->messages()->where('content', 'like', "%$keyword%")->first()];
            $room->members = $room->members->take(2);
        }

        return $rooms;
    }

    public function deleteMessage(string $roomId, string $userId)
    {
        $userId = Auth::user()->id;

        try {
            $messageDelete = MessageDelete::updateOrCreate(
                [
                    'room_id' => $roomId,
                    'user_id' => $userId,
                ],
                [
                    'deleted_at' => now()->timestamp,
                    'updated_at' => now(),
                ]
            );

            return $messageDelete;
        } catch (\Throwable $th) {
            throw new ChatException(ChatException::DELETE_MESSAGE_FAIL);
        }
    }

    public function seenMessage(string $roomId, int $latestMessages): void
    {
        $userId = Auth::user()->id;

        $seenMessage = UserSeen::updateOrCreate(
            ['room_id' => $roomId, 'user_id' => $userId],
            ['seen_at' => $latestMessages]
        );

        if (! $seenMessage) {
            throw new ChatException(ChatException::CREATE_SEEN_MESSAGE_FAIL);
        }
    }

    public function countUnreadMessage()
    {
        $user = Auth::user();
        $userSeen = UserSeen::where('user_id', $user->id)->get();
        $userRooms = $user->rooms;
        $countRoom = 0;

        foreach ($userRooms as $rooms) {
            $seenRoom = UserSeen::where([
                ['user_id', $user->id],
                ['room_id', $rooms->id],
            ])->first();

            if ($seenRoom) {
                $seenAt = $seenRoom->seen_at;
                $message = $rooms->messages()->where('user_id', '!=', $user->id)->latest()->first();

                if (! $message) {
                    continue;
                }

                $messageTimestamp = $message->timestamp;

                if ($seenAt < $messageTimestamp) {
                    $countRoom++;

                    continue;
                }

                continue;
            }

            if (! $seenRoom && $rooms->lastMessage &&
                $rooms->lastMessage->user_id != $user->id
            ) {
                $countRoom++;
            }
        }

        return $countRoom;
    }

    public function sendMessage(array $data)
    {
        $message = $this->create($data);
        $room = Room::find($message->room->id);
        $room->updated_at = $message->updated_at;
        $room->save();

        return $message;
    }
}
