<?php

namespace App\Repositories;

use App\Enums\VideoStatus;
use App\Models\Hashtag;
use App\Models\User;
use App\Models\Video;
use App\Traits\PerPage;
use Illuminate\Pagination\LengthAwarePaginator;

class VideoRepository extends BaseRepository
{
    use PerPage;

    protected $createRules = [
    ];

    public function getModel()
    {
        return Video::class;
    }

    public function index(array $data)
    {

    }

    public function show(string $videoId)
    {
        return $this->query()->find($videoId);
    }

    public function update($model, array $data)
    {

    }

    public function deleteVideo(Video $video)
    {
        return $video->delete();
    }

    public function getByColumn($value, $column_name = 'id')
    {
        return $this->query()->where($column_name, $value);
    }

    public function getVideoFollowing(User $user, $perPage, int $page)
    {
        $perPage = $this->getPerPage($perPage);
        $followingUsers = $user->following->first();

        if (is_null($followingUsers)) {
            $emptyPaginator = new LengthAwarePaginator([], 0, $perPage);

            return $emptyPaginator;
        }

        $videos = Video::select('videos.*')
            ->join('followers', 'videos.user_id', '=', 'followers.following_id')
            ->where('followers.follower_id', $user->id)
            ->where('videos.status', VideoStatus::Public->value);

        $videos = $this->customPaginate($videos, $perPage, $page);

        return $videos;
    }

    public function getFriendVideos(User $user, $perPage, int $page)
    {
        $perPage = $this->getPerPage($perPage);

        $friendVideos = Video::select('videos.*')
            ->join('followers', 'videos.user_id', '=', 'followers.following_id')
            ->where('followers.follower_id', $user->id)
            ->where('followers.is_friend', true)
            ->where('videos.status', VideoStatus::Public->value);

        $friendVideos = $this->customPaginate($friendVideos, $perPage, $page);

        if (is_null($friendVideos)) {
            $emptyPaginator = new LengthAwarePaginator([], 0, $perPage);

            return $emptyPaginator;
        }

        return $friendVideos;
    }

    public function getVideo(string $videoId)
    {
        return $this->query()->find($videoId);
    }

    public function changeStatus(Video $video, string $status)
    {
        $video->status = $status;
        $video->save();

        return $video;
    }

    public function getVideoPrivate(string $userId, int $perPage = null, int $page)
    {
        $perPage = $this->getPerPage($perPage);

        $videos = $this->query()->where([
            ['user_id', $userId],
            ['status', VideoStatus::Private->value],
        ])
            ->orderBy('created_at', 'desc');

        $videos = $this->customPaginate($videos, $perPage, $page);

        return $videos;
    }

    public function syncHashtag(array $hashtags, Video $video)
    {
        try {
            \DB::beginTransaction();

            foreach ($hashtags as $hashtag) {
                $hashtag = Hashtag::firstOrCreate(['name' => $hashtag]);
                $video->hashtags()->attach($hashtag->id);
            }

            \DB::commit();
        } catch (\Throwable $th) {
            \DB::rollback();

            return false;
        }

        return true;
    }

    public function searchByHashtag(string $hashtag, int $perPage = null)
    {
        $perPage = $this->getPerPage($perPage);

        $videos = $this->query()->select('videos.*')
            ->whereHas('hashtags', function ($query) use ($hashtag) {
                $query->where('name', $hashtag);
            })->where('status', VideoStatus::Public->value)->paginate($perPage);

        return $videos;
    }

    public function updateVideo(Video $video, array $data)
    {
        $video->update($data);

        return $video;
    }

    public function getVideosBE(int $perPage = null)
    {
        $perPage = $this->getPerPage($perPage);
        $videos = Video::whereIn('status', [VideoStatus::Public->value, VideoStatus::Private->value])
            ->paginate($perPage);

        return $videos;
    }

    public function search(string $keyword = null, int $perPage = null)
    {
        $perPage = is_null($perPage) ? self::PER_PAGE_ITEM : $perPage;

        return Video::where(function ($query) use ($keyword) {
            $query->where('id', 'LIKE', '%' . $keyword . '%')
                ->orWhere('title', 'LIKE', '%' . $keyword . '%')
                ->orWhere('description', 'LIKE', '%' . $keyword . '%');
        })->paginate($perPage);
    }

    public function getTopVideos(int $perPage = null)
    {
        $perPage = $this->getPerPage($perPage);
        $videos = Video::withCount('likes')
            ->orderByDesc('likes_count')
            ->paginate($perPage);

        return $videos;
    }
}
