<?php

namespace App\Repositories;

use App\Exceptions\CollectionException;
use App\Exceptions\VideoException;
use App\Models\CollectionVideo;
use Illuminate\Support\Facades\Config;

class CollectionVideoRepository extends BaseRepository
{
    protected $createRules = [
    ];

    public function getModel()
    {
        return CollectionVideo::class;
    }

    public function createOrUpdate(string $userId, string $videoId, string $collectionId = null)
    {
        try {
            $collectionVideo = CollectionVideo::updateOrCreate(
                [
                    'video_id' => $videoId,
                    'user_id' => $userId,
                ],
                [
                    'collection_id' => $collectionId,
                ]
            );

            return $collectionVideo;
        } catch (\Throwable $th) {
            throw new CollectionException(CollectionException::BOOKMARK_VIDEO_FAIL);
        }
    }

    public function deleteByCollectionIdUserId(string $userId, string $collectionId)
    {
        $query = $this->query()->where('user_id', $userId)->where('collection_id', $collectionId);

        return $query->update(['collection_id' => null]);
    }

    public function getByUserIdVideoId(string $userId, string $videoId)
    {
        return CollectionVideo::query()->where('user_id', $userId)->where('video_id', $videoId)->first();
    }

    public function checkVideoExistence(string $fieldName, string $fieldValue, $videoId): bool
    {
        $dataCondition = [
            [$fieldName, $fieldValue],
            ['video_id', $videoId],
        ];

        if ($fieldName == 'user_id') {
            $dataCondition[] = ['collection_id', null];
        }

        $isExists = CollectionVideo::where($dataCondition)->first();

        if ($isExists) {
            throw new VideoException(VideoException::VIDEO_ALREADY_EXISTS);
        }

        return true;
    }

    public function countVideoByCollectionId(string $collectionId)
    {
        return $this->query()->where('collection_id', $collectionId)->count();
    }

    public function countVideoByUserId(string $userId)
    {
        return $this->query()->where('user_id', $userId)->count();
    }

    public function countByVideoId(string $videoId)
    {
        return $this->query()->where('video_id', $videoId)->count();
    }

    protected function buildQueryGetVideo(string $userId, string $collectionId = null)
    {
        $query = CollectionVideo::with('video')->where('user_id', $userId);

        if ($collectionId !== Config::get('app.all_post_id')) {
            if ($collectionId) {
                $query->where('collection_id', $collectionId);
            } else {
                $query->where('collection_id', null);
            }
        }

        return $query->orderBy('created_at', 'desc');
    }

    public function getOneVideo(string $userId, string $collectionId = null)
    {
        $query = $this->buildQueryGetVideo($userId, $collectionId);

        return $query->first();
    }

    public function getVideo(string $userId, string $collectionId = null, $perPage = 10, int $page)
    {
        if (! isset($perPage) || $perPage > 25) {
            $perPage = self::PER_PAGE_ITEM;
        }
        $query = $this->buildQueryGetVideo($userId, $collectionId);
        return $this->customPaginate($query, $perPage, $page);
    }
}
