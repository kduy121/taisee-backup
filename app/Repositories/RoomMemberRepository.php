<?php

namespace App\Repositories;

use App\Models\RoomMember;

class RoomMemberRepository extends BaseRepository
{
    public function getModel()
    {
        return RoomMember::class;
    }

    public function getMember($value, $column_name, $perPage, int $page)
    {
        if (! isset($perPage) || $perPage > 25) {
            $perPage = self::PER_PAGE_ITEM;
        }

        $members = $this->query()->with('user')->where($column_name, $value);

        return $this->customPaginate($members, $perPage, $page);
    }

    public function getByRoomIdUserIds(string $roomId, array $userIds)
    {
        return $this->query()->where('room_id', $roomId)->whereIn('user_id', $userIds)->get();
    }
}
