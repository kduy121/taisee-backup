<?php

namespace App\Repositories;

use App\Exceptions\CollectionException;
use App\Models\Collection;

class CollectionRepository extends BaseRepository
{
    protected $createRules = [
    ];

    public function getModel()
    {
        return Collection::class;
    }

    public function updateThumbByLatestVideoId(string $videoId, string $thumb = null)
    {
        try {
            $collection = Collection::where('latest_video_id', $videoId)
                ->update([
                    'thumbnail' => $thumb,
                ]);

            return $collection;
        } catch (\Throwable $th) {
            throw new CollectionException(CollectionException::CAN_NOT_CHANGE_THUMB);
        }
    }

    public function findByname($name): Collection
    {
        $collection = Collection::with('user', 'videos')->where('name', $name)->first();

        if (! $collection) {
            throw new CollectionException(CollectionException::COLLECTION_NOT_FOUND);
        }

        return $collection;
    }

    public function getAllWithPaginate(string $userId, string $excludeCollectionId = null, $perPage, int $page)
    {
        if (! isset($perPage) || $perPage > 25) {
            $perPage = self::PER_PAGE_ITEM;
        }

        $query = Collection::with(['videos' => function ($query) {
            $query->orderBy('created_at', 'desc');
        }])
            ->where('user_id', $userId);

        if (! empty($excludeCollectionId)) {
            $query->where('id', '<>', $excludeCollectionId);
        }

        $query = $query->orderBy('created_at', 'desc');

        return $this->customPaginate($query, $perPage, $page);
    }

    public function checkCollectionOwnership(string $userId, string $collectionId): bool
    {
        $isOwner = Collection::where([
            ['id', $collectionId],
            ['user_id', $userId],
        ])->first();

        if (! $isOwner) {
            throw new CollectionException(CollectionException::COLLECTION_FORBIDDEN);
        }

        return true;
    }

    public function getCollection(string $userId, string $collectionId)
    {
        $collection = Collection::where([
            ['id', $collectionId],
            ['user_id', $userId],
        ])->first();

        if (! $collection) {
            throw new CollectionException(CollectionException::COLLECTION_NOT_FOUND);
        }

        return $collection;
    }
}
