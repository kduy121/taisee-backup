<?php

namespace App\Repositories;

use App\Models\Like;

class LikeRepository extends BaseRepository
{
    protected $createRules = [
    ];

    public function getModel()
    {
        return Like::class;
    }

    public function index(array $data)
    {

    }

    public function show(array $data)
    {

    }

    public function update($model, array $data)
    {

    }

    public function delete($name)
    {

    }
}
