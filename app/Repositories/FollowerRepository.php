<?php

namespace App\Repositories;

use App\Models\Follower;

class FollowerRepository extends BaseRepository
{
    protected $createRules = [
    ];

    public function getModel()
    {
        return Follower::class;
    }

    public function getByFollowerIdFollowingId(string $followerId, string $followingId)
    {
        return $this->query()->where('follower_id', $followerId)->where('following_id', $followingId)->first();
    }
}
