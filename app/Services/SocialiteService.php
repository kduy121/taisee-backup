<?php

namespace App\Services;

use App\Repositories\SocialiteRepository;

class SocialiteService
{
    public function __construct(private SocialiteRepository $socialiteRepository)
    {
    }

    public function generateUserName(string $email = null)
    {
        if ($email) {
            $username = explode('@', $email)[0];

            if (strlen($username) > 16) {
                $username = substr($username, 0, 16);
            }

            $randomString = $this->generateRandomString();
            $username = $username."_$randomString";

            $this->validateUserName($username);

            return $username;
        }

        $timestamp = time();
        $username = $timestamp;
        $randomString = $this->generateRandomString();
        $username = $username."_$randomString";

        $this->validateUserName($username);

        return $username;
    }

    public function generateRandomString($length = 5)
    {
        $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';

        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }

        return $randomString;
    }

    public function validateUserName(string $userName)
    {
        return $this->socialiteRepository->validateUserName($userName);
    }
}
