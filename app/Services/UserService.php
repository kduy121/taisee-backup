<?php

namespace App\Services;

use App\Exceptions\CustomModelNotFoundException;
use App\Exceptions\UserException;
use App\Models\Admin;
use App\Models\User;
use App\Repositories\FollowerRepository;
use App\Repositories\MessageRepository;
use App\Repositories\UserRepository;
use App\Repositories\VideoDraftRepository;
use App\Repositories\VideoRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Storage;

class UserService
{
    private int $perPage;

    public function __construct(
        private UserRepository $userRepository, private VideoRepository $videoRepository,
        private FollowerRepository $followerRepository, private MessageRepository $messageRepository,
        private VideoDraftRepository $videoDraftRepository)
    {
        $this->perPage = config('constants.per_page_item');
    }

    public function getById(string $userId)
    {
        return $this->userRepository->find($userId);
    }

    public function followUser(string $userId, string $profileId)
    {
        // Check following id
        $profileUser = $this->userRepository->find($profileId);
        if (empty($profileUser)) {
            throw new UserException(UserException::USER_NOT_FOUND);
        }

        $follow = $this->followerRepository->getByFollowerIdFollowingId($userId, $profileId);
        $followBack = $this->followerRepository->getByFollowerIdFollowingId($profileId, $userId);
        $isFriend = empty($followBack) ? false : true;

        if (empty($follow)) {

            $follow = $this->followerRepository->create([
                'follower_id' => $userId,
                'following_id' => $profileId,
                'is_friend' => $isFriend,
            ]);

            if ($isFriend) {
                $followBack->is_friend = true;
                $followBack->save();
            }

        } else {
            $this->followerRepository->delete($follow->id);
            $follow->follower_id = null;

            if ($isFriend) {
                $followBack->is_friend = false;
                $followBack->save();
            }
        }

        return [
            'follow' => $follow,
            'follow_back' => $followBack,
        ];
    }

    public function searchUser(string $keyword, ?int $perPage)
    {
        return $this->userRepository->search($keyword, $perPage);
    }

    public function findUserByUsernameId(string $keyword)
    {
        $user = $this->userRepository->getByColumn($keyword, $column_name = 'user_name');
        if (! $user) {
            $user = $this->userRepository->find($keyword);
        }

        if (! $user) {
            throw new CustomModelNotFoundException('User not found.');
        }

        return $user;
    }

    public function getFollower($perPage = 25, int $page)
    {
        $user = Auth::user();

        if (! $user) {
            throw new CustomModelNotFoundException('User not found.');
        }

        $followers = $this->userRepository->getFollower($user, $perPage, $page);

        foreach ($followers as $index => $follower) {
            $isFriend = $follower->pivot->is_friend;

            if ($isFriend == 1) {
                $followers->forget($index);
            }
        }

        return $followers;
    }

    public function getFollowing($perPage = 25, int $page)
    {
        $user = Auth::user();

        if (! $user) {
            throw new CustomModelNotFoundException('User not found.');
        }

        $following = $this->userRepository->getFollowing($user, $perPage, $page);

        return $following;
    }

    public function getUserTrending($perPage = null, $userLogin = null, int $page)
    {
        $perPage = $perPage ?? $this->perPage;

        $users = $this->userRepository->getUserTrending($perPage, $userLogin, $page);

        return $users;
    }

    public function appendUserInfo(User $user, array $extraInfo)
    {
        // append is show trending users
        $followingVideo = $this->videoRepository->getVideoFollowing($user, $perPage = 1, $page = 1);
        $user->isShowTrending = empty($followingVideo->total()) ? true : false;

        // append friend video info
        $friendVideo = $this->videoRepository->getFriendVideos($user, $perPage = 1, $page = 1);
        $user->hasFriendVideo = empty($friendVideo->total()) ? false : true;

        $friend = $this->messageRepository->searchUser($keyword = null, $perPage = 1);
        $user->hasFriend = empty($friend->count()) ? false : true;

        if (! empty($extraInfo['count_draft'])) {
            $user->countDraft = $this->videoDraftRepository->countVideoDrafts($user);
        }

        if (! empty($extraInfo['count_msg'])) {
            $user->countMsg = $this->messageRepository->countUnreadMessage();
        }

        return $user;
    }

    public function searchUserBE(string $keyword, ?int $perPage)
    {
        return $this->userRepository->search($keyword, $perPage);
    }

    public function updateUser(User $user, array $data)
    {
        return $this->userRepository->updateUser($user, $data);
    }

    public function getAdmin(string $keyword = null, $perPage)
    {
        return $this->userRepository->getAdmin($keyword, $perPage);
    }

    public function getAdminById(string $id)
    {
        $user = $this->userRepository->getAdminById($id);

        if (!$user) {
            throw new UserException(UserException::USER_NOT_FOUND);
        }
        
        return $user;
    }

    public function updateAdmin(Admin $admin, array $data)
    {
        if (isset($data['password']) && ($data['password'] !== $data['confirm_password'])) {
            throw new UserException(UserException::PASSWORD_FAIL);
        } 

        if (isset($data['password'])){
            $data['password'] = Hash::make($data['password']);
        } else {
            unset($data['password']);
        }

        if (isset($data['avatar_file'])) {
            $path = $data['avatar_file']->store("public/" . Config::get('app.avatar_upload_path'));
            $path = Storage::url($path);
            $url = asset($path);
            $data['avatar_url'] = $url;
        }

        $response = $this->userRepository->updateAdmin($admin, $data);

        if (!$response) {
            throw new UserException(UserException::UPDATE_FAIL);
        }
        
        return $response;
    }

    public function getTopUser(int $perPage = null)
    {
        return $this->userRepository->getTopUser($perPage);
    }
}
