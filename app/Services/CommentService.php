<?php

namespace App\Services;

use App\Enums\VideoStatus;
use App\Exceptions\CustomModelNotFoundException;
use App\Models\Comment;
use App\Repositories\CommentRepository;
use App\Repositories\VideoRepository;
use Illuminate\Support\Facades\Auth;

class CommentService
{
    public function __construct(private CommentRepository $repo, private VideoRepository $videoRepository)
    {
    }

    /**
     * Get comments by video ID.
     *
     * @param  int  $perPage
     */
    public function searchByVideoId(string $videoId, ?int $perPage)
    {
        $comment = $this->repo->searchByVideoId($videoId, $perPage);
        $userId = Auth::guard('api')->user()->id ?? null;

        if (! $comment->isEmpty() && $comment[0]->video->status == VideoStatus::Private->value
            && $comment[0]->video->user_id != $userId) {
            throw new CustomModelNotFoundException('Video not found.');
        }

        return $comment;
    }

    public function create(array $data): Comment
    {
        $data['user_id'] = Auth::user()->id;

        $video = $this->videoRepository->show($data['video_id']);
        if (! $video || ($video->status == VideoStatus::Private->value && $video->user_id != Auth::user()->id)) {
            throw new CustomModelNotFoundException('Video not found.');
        }

        $comment = $this->repo->create($data);

        if (! $comment) {
            throw new CustomModelNotFoundException('Create has fail.', 500);
        }

        return $comment;
    }

    public function delete(array $data): void
    {
        $userId = Auth::user()->id;

        // Check comment
        $this->repo->checkComment($data['comment_id'], $data['video_id'], $userId);
        $this->repo->delete($data);
    }

    public function update(array $data)
    {
        $userId = Auth::user()->id;

        // Check comment
        $comment = $this->repo->checkComment($data['comment_id'], $data['video_id'], $userId);
        $comment = $this->repo->update($comment, $data);

        return $comment;
    }
}
