<?php

namespace App\Services;

use App\Repositories\RoleRepository;

class RoleService
{
    private int $perPage;

    public function __construct(
        private RoleRepository $repo)
    {
        $this->perPage = config('constants.per_page_item');
    }

    public function getAllRoles()
    {
        return $this->repo->all($this->perPage);
    }

    public function getRoleById($id)
    {
        return $this->repo->find($id);
    }

    public function createRole($data)
    {
        return $this->repo->create($data);
    }

    public function updateRole($id, $data)
    {
        return $this->repo->update($id, $data);
    }

    public function deleteRole($id)
    {
        $this->repo->delete($id);
    }
}
