<?php

namespace App\Services;

use App\Repositories\PermissionRepository;

class PermissionService
{
    private int $perPage;

    public function __construct(
        private PermissionRepository $repo)
    {
        $this->perPage = config('constants.per_page_item');
    }

    public function getAllPermissions()
    {
        return $this->repo->all($this->perPage);
    }

    public function getPermissionById($id)
    {
        return $this->repo->find($id);
    }

    public function createPermission($data)
    {
        return $this->repo->create($data);
    }

    public function updatePermission($id, $data)
    {
        return $this->repo->update($id, $data);
    }

    public function deletePermission($id)
    {
        $this->repo->delete($id);
    }
}
