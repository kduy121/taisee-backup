<?php

namespace App\Services;

use App\Exceptions\CommentLivestreamException;
use App\Repositories\CommentLivestreamRepository;
use App\Repositories\UserRepository;

class CommentLivestreamService
{
    public function __construct(
        private CommentLivestreamRepository $repo,
        private UserRepository $userRepository)
    {
    }

    public function getAllComments(int $perPage = null, string $livestreamId, string $userId = null, int $page)
    {
        $this->repo->validateLiveStream($livestreamId, $userId);

        return $this->repo->getAllComments($perPage, $livestreamId, $page);
    }

    public function createComment(array $data)
    {
        $this->repo->validateLiveStream($data['livestream_id'], $data['user_id']);
        $comment = $this->repo->createComment($data);

        if (! $comment) {
            throw new CommentLivestreamException(CommentLivestreamException::CREATE_FAIL);
        }

        return $comment;
    }

    public function updateCommentLivestream(array $data)
    {
        $this->repo->validateData($data['user_id'], $data['comment_id']);
        $comment = $this->repo->updateCommentLivestream($data);

        return $comment;
    }

    public function deleteCommentLivestream(array $data)
    {
        $this->repo->validateData($data['user_id'], $data['comment_id']);

        return $this->repo->delete($data['comment_id']);
    }
}
