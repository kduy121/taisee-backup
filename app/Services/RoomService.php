<?php

namespace App\Services;

use App\Enums\ChatType;
use App\Exceptions\ChatException;
use App\Exceptions\UserException;
use App\Models\Room;
use App\Repositories\RoomRepository;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Auth;

class RoomService
{
    public function __construct(private RoomRepository $roomRepo, private UserRepository $userRepo)
    {
    }

    public function createRoom(array $data): Room
    {
        if (in_array($data['user_id'], $data['member'])) {
            $data['member'] = array_diff($data['member'], [$data['user_id']]);
            $data['thumb_ids'] = $data['member'];
        }

        array_unshift($data['thumb_ids'], $data['user_id']);

        $data['type'] = count($data['member']) > 1 ? ChatType::from('group')->name : ChatType::from('private')->name;
        $data['total_member'] = count($data['member']) + 1;
        $data['avatar_ids'] = array_slice($data['member'], 0, 2);
        $data['thumb_ids'] = array_slice($data['thumb_ids'], 0, 4);
        $data['init_total_member'] = count($data['member']) + 1;

        // Check chat private is exists
        if (count($data['member']) == 1) {
            $userId = Auth::user()->id;
            $room = $this->roomRepo->checkRoomPrivate($userId, $data['member'][0]);

            if ($room) {
                return $room;
            }
        }

        foreach ($data['member'] as $user) {
            $user = $this->userRepo->find($user);

            if (! $user) {
                throw new UserException(UserException::USER_NOT_FOUND);
            }
        }

        try {
            \DB::beginTransaction();

            $room = $this->roomRepo->create($data);

            if (! $room) {
                throw new ChatException(ChatException::CREATE_ROOM_FAIL);
            }

            $this->roomRepo->syncData($room, $data['member']);

            \DB::commit();
        } catch (\Throwable $th) {
            \DB::rollback();
            throw new ChatException(ChatException::CAN_NOT_ADD_MEMBER);
        }

        return $room;
    }

    public function getRooms($perPage = 25, int $page)
    {
        return $this->roomRepo->getRooms($perPage, $page);
    }

    public function deleteRoom(string $roomId)
    {
        // Check author
        $this->roomRepo->checkAuthor($roomId);

        return $this->roomRepo->delete($roomId);
    }

    public function leaveRoom(string $roomId)
    {
        return $this->roomRepo->leaveRoom($roomId);
    }

    public function addMember(string $roomId, array $memberIds)
    {
        // Check author
        // $this->roomRepo->checkAuthor($roomId);

        // Check room
        $this->roomRepo->checkRoom($roomId);

        // Check member already in room
        foreach ($memberIds as $memberId) {
            $this->roomRepo->checkMember($roomId, $memberId);
        }

        return $this->roomRepo->addMember($roomId, $memberIds);
    }

    public function pinRoom(string $roomId, bool $pinnedType = true)
    {
        // Check room
        $user = Auth::user();
        $room = $this->roomRepo->find($roomId);

        if (! $room) {
            throw new ChatException(ChatException::ROOM_NOT_FOUND);
        }

        // Check member in room
        $isMember = $room->members->where('id', $user->id)->toArray();

        if (! count($isMember)) {
            throw new ChatException(ChatException::USER_NOT_MEMBER);
        }

        if (($pinnedType && array_shift($isMember)['pivot']['pinned']) || (! $pinnedType && ! array_shift($isMember)['pivot']['pinned'])) {
            $statusText = $pinnedType ? 'pinned' : 'un-pinned';

            if ($statusText == 'pinned') {
                throw new ChatException(ChatException::ROOM_ALREADY_PINNED);
            }

            throw new ChatException(ChatException::ROOM_ALREADY_UNPINNED);
        }

        $this->roomRepo->pinRoom($room->id, $pinnedType);

        return $room;
    }

    public function changeRoomName(string $roomId, string $name)
    {
        $memberId = Auth::user()->id;
        $data['name'] = $name;

        // Check room is group
        $isRoom = $this->roomRepo->checkRoomExist($roomId);

        if (! $isRoom) {
            throw new ChatException(ChatException::ROOM_NOT_FOUND);
        }

        $room = $this->roomRepo->isGroupChat($roomId);

        if (! $room) {
            throw new ChatException(ChatException::ROOM_NOT_GROUP);
        }

        // Check member already in room
        $isMember = $this->roomRepo->checkMemberInRoom($roomId, $memberId);

        if (! $isMember) {
            throw new ChatException(ChatException::MEMBER_NOT_IN_ROOM);
        }

        $response = $this->roomRepo->update($room, $data);

        return $response;
    }

    public function getRoomDetail(string $roomId, string $userId)
    {
        $isMember = $this->roomRepo->checkMemberInRoom($roomId, $userId);

        if (! $isMember) {
            throw new ChatException(ChatException::FORBIDDEN);
        }

        $room = $this->roomRepo->getRoomDetail($roomId);

        if (! $room) {
            throw new ChatException(ChatException::ROOM_NOT_FOUND);
        }

        return $room;
    }
}
