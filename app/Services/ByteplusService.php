<?php

namespace App\Services;

use App\Enums\VideoStatus;
use App\Exceptions\ByteplusException;
use App\Exceptions\VideoException;
use App\Repositories\UserRepository;
use App\Repositories\VideoDraftRepository;
use App\Repositories\VideoRepository;
use Byteplus\Service\Vod\Models\Request\VodGetPlayInfoRequest;
use Byteplus\Service\Vod\Vod;
use Illuminate\Http\Request;
use Illuminate\Support\Env;
use Illuminate\Support\Facades\Log;

class ByteplusService
{
    const TOKEN_EXPIRED_TIME = 300;

    const GET_PLAYINFO_THUMB = '1';

    const GET_PLAYINFO_SSL = '1';

    const GET_PLAYINFO_FORMAT = 'mp4';

    private $client;

    public function __construct(private VideoRepository $videoRepository,
        private VideoDraftRepository $videoDraftRepository,
        private UserRepository $userRepository,
        private VideoService $videoService)
    {
        $accessKey = Env::get('BYTEPLUS_ACCESS_KEY');
        $secretKey = Env::get('BYTEPLUS_SECRET_KEY');
        if (empty($accessKey) || empty($secretKey)) {
            throw new ByteplusException(ByteplusException::MISSING_BYTEPLUS_CONFIG);
        }

        $this->client = Vod::getInstance();
        $this->client->setAccessKey($accessKey);
        $this->client->setSecretKey($secretKey);
    }

    public function getUploadConfig(): array
    {
        $tokenConfig = $this->client->getUploadVideoAuthWithExpiredTime(self::TOKEN_EXPIRED_TIME);
        if (empty($tokenConfig)) {
            throw new ByteplusException(ByteplusException::CAN_NOT_GET_UPLOAD_TOKEN);
        }

        return $tokenConfig;
    }

    public function handlerUpload(Request $request)
    {
        $params = $request->getContent();
        Log::debug('Handle upload callback: '.$params);

        $params = json_decode($params, true);
        $videoId = isset($params['Data']['Vid']) ? $params['Data']['Vid'] : '';
        $callbackAgrs = isset($params['Data']['CallbackArgs']) ? $params['Data']['CallbackArgs'] : '';
        if (empty($videoId) || empty($callbackAgrs)) {
            throw new ByteplusException(ByteplusException::INVALID_CALLBACK_CONTENT);
        }

        $callbackAgrs = json_decode($callbackAgrs, true);
        if (is_null($callbackAgrs) || ! isset($callbackAgrs['user_id'], $callbackAgrs['video_title'], $callbackAgrs['video_description'])) {
            throw new ByteplusException(ByteplusException::CALLBACK_ARG_MISSING);
        }

        $videoStatus = isset($callbackAgrs['video_status']) ? $callbackAgrs['video_status'] : VideoStatus::Public->value;
        $videoPrivacy = isset($callbackAgrs['video_privacy']) ? $callbackAgrs['video_privacy'] : VideoStatus::Public->value;
        if (! VideoStatus::tryFrom((int) $videoStatus)) {
            throw new ByteplusException(ByteplusException::INVALID_VIDEO_STATUS);
        }

        if ($videoStatus === VideoStatus::Draft->value) {
            $video = $this->videoDraftRepository->getByColumn($videoId, 'byteplus_video_id')->first();
        } else {
            $video = $this->videoRepository->getByColumn($videoId, 'byteplus_video_id')->first();
        }
        if (! empty($video)) {
            Log::debug('Video exist, byteplus_video_id: '.$videoId);

            return $video;
        }

        $userInfo = $this->userRepository->find($callbackAgrs['user_id']);
        if (empty($userInfo)) {
            throw new ByteplusException(ByteplusException::USER_NOT_FOUND);
        }

        $playInfo = $this->getPlayInfo($videoId);
        $playInfoMain = $playInfo['Result']['PlayInfoList'][0];
        $metaInfo = $playInfoMain;
        unset($metaInfo['MainPlayUrl']);
        unset($metaInfo['BackupPlayUrl']);

        $videoArr = [
            'user_id' => $callbackAgrs['user_id'],
            'title' => $callbackAgrs['video_title'],
            'description' => $callbackAgrs['video_description'],
            'file_path' => $playInfoMain['MainPlayUrl'],
            'streaming_path' => $playInfoMain['MainPlayUrl'],
            'thumbnail' => $playInfo['Result']['PosterUrl'],
            'byteplus_video_id' => $videoId,
            'meta' => json_encode($metaInfo),
            'status' => $videoStatus,
        ];

        if ($videoStatus === VideoStatus::Draft->value) {
            unset($videoArr['status']);
            $videoArr['video_privacy'] = $videoPrivacy;
            $result = $this->videoDraftRepository->create($videoArr);
        } else {
            $result = $this->videoRepository->create($videoArr);

            // Hashtag
            $hashtags = $this->videoService->getHashtagsFromDescription($videoArr['description']);
            $syncHashtag = $this->videoRepository->syncHashtag($hashtags, $result);

            if (! $syncHashtag) {
                throw new VideoException(VideoException::SYNC_HASHTAG_FAIL);
            }
        }
        if (empty($result)) {
            throw new ByteplusException(ByteplusException::CAN_NOT_CREATE_VIDEO);
        }

        return $params;
    }

    public function getPlayInfo(string $videoId)
    {
        if (empty($videoId)) {
            throw new ByteplusException(ByteplusException::VIDEO_ID_IS_EMPTY);
        }

        $req = new VodGetPlayInfoRequest();
        $req->setVid($videoId);
        $req->setNeedThumbs(self::GET_PLAYINFO_THUMB);
        $req->setFormat(self::GET_PLAYINFO_FORMAT);
        $req->setSsl(self::GET_PLAYINFO_SSL);

        try {
            $response = $this->client->getPlayInfo($req);
        } catch (Exception $e) {
            throw $e;
        }

        if ($response != null && $response->getResponseMetadata() != null && $response->getResponseMetadata()->getError() != null) {
            $errorMsg = $response->getResponseMetadata()->getError()->serializeToJsonString();
            throw new ByteplusException(ByteplusException::DEFAULT_ERR_CODE, $errorMsg);
        }

        $playInfo = $response->serializeToJsonString();
        Log::debug('Playinfo: '.$playInfo);

        $playInfo = json_decode($playInfo, true);

        return $playInfo;
    }
}
