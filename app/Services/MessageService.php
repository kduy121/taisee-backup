<?php

namespace App\Services;

use App\Exceptions\ChatException;
use App\Repositories\MessageRepository;
use App\Repositories\RoomMemberRepository;
use Illuminate\Support\Facades\Auth;

class MessageService
{
    const LIMIT = 4;

    public function __construct(private MessageRepository $messageRepo, private RoomMemberRepository $roomMemberRepository)
    {
    }

    public function sendMessage(array $data)
    {
        // Check room
        $this->messageRepo->checkRoom($data['room_id']);

        // Check member
        $this->messageRepo->checkMember($data['room_id'], $data['user_id']);

        $message = $this->messageRepo->sendMessage($data);

        if (! $message) {
            throw new ChatException(ChatException::SEND_MESSAGE_FAIL);
        }

        return $message;
    }

    public function getMessage(string $roomId, $perPage = 25, int $page)
    {
        $userId = Auth::user()->id;

        // Check user
        $this->messageRepo->checkMember($roomId, $userId);

        // Get messages
        return $this->messageRepo->getMessage($roomId, $userId, $perPage, $page);
    }

    public function searchTop(string $keyword)
    {
        $limit = self::LIMIT;

        $users = $this->messageRepo->searchUser($keyword, $limit);
        $messages = $this->messageRepo->searchMessage($keyword, $limit);

        $result['users'] = $users;
        $result['messages'] = $messages;

        return $result;
    }

    public function topUser(string $keyword, ?int $perPage, int $page)
    {
        $users = $this->messageRepo->searchUser($keyword, null, $perPage, $page);

        return $users;
    }

    public function topRooms(string $keyword, ?int $perPage, int $page)
    {
        $rooms = $this->messageRepo->topRooms($keyword, $perPage, $page);

        return $rooms;
    }

    public function searchFriend(string $keyword = null, string $checkRoomId = null, string $perPage = null, int $page)
    {
        $users = $this->messageRepo->searchUser($keyword, null, $perPage, $page);
        if (empty($users)) {
            return $users;
        }
        if (empty($checkRoomId)) {
            return $users;
        }

        $userIds = [];
        foreach ($users as $user) {
            $userIds[] = $user->id;
        }
        $roomMembers = $this->roomMemberRepository->getByRoomIdUserIds($checkRoomId, $userIds);
        if (empty($roomMembers)) {
            return $users;
        }

        foreach ($users as $user) {
            foreach ($roomMembers as $roomMember) {
                if ($roomMember->user_id != $user->id) {
                    continue;
                }
                $user->in_room = true;
            }
        }

        return $users;
    }

    public function deleteMessage(string $roomId)
    {
        $userId = Auth::user()->id;

        $this->messageRepo->checkRoom($roomId);
        $this->messageRepo->checkMember($roomId, $userId);

        $message = $this->messageRepo->deleteMessage($roomId, $userId);

        return $message;
    }

    public function seenMessage(string $roomId, int $latestMessages)
    {
        $this->messageRepo->seenMessage($roomId, $latestMessages);
    }

    public function countUnreadMessage()
    {
        return $this->messageRepo->countUnreadMessage();
    }
}
