<?php

namespace App\Services;

use App\Models\Hashtag;
use App\Repositories\HashtagRepository;
use App\Exceptions\HashtagException;

class HashtagService
{
    const PER_PAGE_ITEM = 10;

    private int $perPage;

    public function __construct(
        private HashtagRepository $repo)
    {
        $this->perPage = config('constants.per_page_item');
    }

    public function getHashtag(?string $hashtag, int $perPage = null, int $page)
    {
        $hashtag = trim($hashtag);

        return $this->repo->getHashtag($hashtag, $perPage, $page);
    }

    public function createHashtag(string $hashtag)
    {
        $hashtags = $this->generateHashtag($hashtag);
        
        return $this->repo->createHashtag($hashtags);
    }

    public function getById(string $id)
    {
        $hashtag = $this->repo->getById($id);

        if (!$hashtag) {
            throw new HashtagException(HashtagException::HASHTAG_NOT_FOUND);
        }

        return $hashtag;
    }

    public function updateHashtag(Hashtag $hashtag, array $data)
    {
        $hashtags = $this->generateHashtag($data['name']);

        if (count($hashtags)) {
            $data['name'] = array_shift($hashtags);
        }

        $response = $this->repo->updateHashtag($hashtag, $data);

        if (!$response) {
            throw new HashtagException(HashtagException::UPDATE_FAIL);
        }
        
        return $response;
    }

    public function generateHashtag(string $hashtag)
    {
        $content = preg_split('/((?:#|@|[^\s]+)(?:[^\s=+\-!$%^&*(),.~`;:<>?\'"]+))/', $hashtag, -1, PREG_SPLIT_DELIM_CAPTURE);

        $hashtags = [];
        foreach ($content as $value) {
            if (trim($value) === '#') {
                continue;
            }

            if (strpos($value, '#') !== false) {
                $subValues = explode('#', $value);
                $subValues = array_filter($subValues);
                $hashtags = array_merge($hashtags, $subValues);
            }
        }

        $hashtags = array_unique($hashtags);

        return $hashtags;
    }
}
