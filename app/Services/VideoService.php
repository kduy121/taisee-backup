<?php

namespace App\Services;

use App\Enums\VideoStatus;
use App\Exceptions\UserException;
use App\Exceptions\VideoException;
use App\Models\User;
use App\Models\Video;
use App\Repositories\CollectionRepository;
use App\Repositories\CollectionVideoRepository;
use App\Repositories\UserRepository;
use App\Repositories\VideoDraftRepository;
use App\Repositories\VideoRepository;

class VideoService
{
    const PER_PAGE_ITEM = 10;

    private int $perPage;

    public function __construct(
        private VideoRepository $videoRepository,
        private CollectionRepository $collectionRepository,
        private CollectionVideoRepository $collectionVideoRepository,
        private UserRepository $userRepo,
        private VideoDraftRepository $videoDraftRepository)
    {
        $this->perPage = config('constants.per_page_item');
    }

    public function getVideos(string $userId = null, int $perPage, array $relations, int $page)
    {
        $condition = [
            'column' => 'status',
            'operations' => '!=',
            'value' => VideoStatus::Private->value,
        ];
        $videos = $this->videoRepository->getPaginated($perPage, $relations, $condition, $page);
        $videos = $this->populateInfoVideos($userId, $videos);

        return $videos;
    }

    public function getDetail(string $userId = null, string $videoId): Video
    {
        $video = $this->videoRepository->find($videoId);

        if ($video->status == VideoStatus::Private->value && ($userId != $video->user_id)) {
            throw new VideoException(VideoException::VIDEO_NOT_FOUND);
        }

        if (is_null($video)) {
            throw new VideoException(VideoException::VIDEO_NOT_FOUND);
        }

        $video = $this->populateInfoVideo($userId, $video);

        return $video;
    }

    public function getVideoByUser(string $userId = null, string $profileId, ?int $perPage, int $page)
    {
        $perPage = is_null($perPage) ? self::PER_PAGE_ITEM : $perPage;

        // Check user
        $isValidate = $this->userRepo->find($profileId);

        if (is_null($isValidate)) {
            throw new UserException(UserException::USER_NOT_FOUND);
        }

        $videos = $this->videoRepository->getByColumn($profileId, 'user_id')
            ->where('status', VideoStatus::Public->value)
            ->orderBy('created_at', 'desc');

        $videos = $this->videoRepository->customPaginate($videos, $perPage, $page);

        $videos = $this->populateInfoVideos($userId, $videos);

        return $videos;
    }

    protected function populateInfoVideos(string $userId = null, $videos)
    {
        foreach ($videos as $video) {
            $video = $this->populateInfoVideo($userId, $video);
        }

        return $videos;
    }

    protected function populateInfoVideo(string $userId = null, Video $video)
    {
        // check is bookmark
        if (is_null($userId)) {
            $video->is_bookmark = false;
        } else {
            $bookmark = $this->collectionVideoRepository->getByUserIdVideoId($userId, $video->id);
            $video->is_bookmark = empty($bookmark) ? false : true;
        }

        // count total bookmark
        $totalBookmark = $this->collectionVideoRepository->countByVideoId($video->id);
        $video->total_bookmark = $totalBookmark;

        return $video;
    }

    public function getVideoFollowing($perPage, User $user, int $page)
    {
        $perPage = $perPage ?? $this->perPage;

        $Videofollowing = $this->videoRepository->getVideoFollowing($user, $perPage, $page);

        $videos = $this->populateInfoVideos($user->id, $Videofollowing);

        return $videos;
    }

    public function getFriendVideos($perPage, User $user, int $page)
    {
        $perPage = is_null($perPage) ? self::PER_PAGE_ITEM : $perPage;

        $videos = $this->videoRepository->getFriendVideos($user, $perPage, $page);

        return $videos;
    }

    public function changeStatus(User $user, string $videoId, string $status)
    {
        // Check author
        $video = $this->videoRepository->getVideo($videoId);

        if (! $video) {
            throw new VideoException(VideoException::VIDEO_NOT_FOUND);
        }

        if ($user->id !== $video->user_id) {
            throw new VideoException(VideoException::NOT_AUTHOR);
        }

        if ($video->status == $status) {
            throw new VideoException(VideoException::VIDEO_ALREADY_EXISTS);
        }

        // update collection thumb
        switch ($status) {
            case VideoStatus::Private->value:
                $this->collectionRepository->updateThumbByLatestVideoId($videoId);
                break;

            case VideoStatus::Public->value:
                $this->collectionRepository->updateThumbByLatestVideoId($videoId, $video->thumbnail);
                break;

            default:
                break;
        }

        return $this->videoRepository->changeStatus($video, $status);
    }

    public function getVideoDrafts($perPage, User $user, int $page)
    {
        $perPage = is_null($perPage) ? self::PER_PAGE_ITEM : $perPage;

        $videos = $this->videoDraftRepository->getVideoDrafts($user, $perPage, $page);

        return $videos;
    }

    public function publishVideos(array $data, int $status, User $user)
    {
        $publishedVideo = $this->publishVideo($data, $status, $user);
        
        return $publishedVideo;
    }

    public function publishVideo(array $dataPublish, int $status, $user)
    {
        $videoDraft = $this->videoDraftRepository->find($dataPublish['video_id']);

        if (! $videoDraft) {
            throw new VideoException(VideoException::VIDEO_DRAFT_NOT_FOUND);
        }

        if ($user->id !== $videoDraft->user_id) {
            throw new VideoException(VideoException::NOT_AUTHOR);
        }

        $data = [
            'user_id' => $videoDraft->user_id,
            'title' => $dataPublish['title'] ?? $videoDraft->title,
            'description' => $dataPublish['description'] ?? $videoDraft->description,
            'file_path' => $videoDraft->file_path,
            'streaming_path' => $videoDraft->streaming_path,
            'thumbnail' => $videoDraft->thumbnail,
            'byteplus_video_id' => $videoDraft->byteplus_video_id,
            'meta' => json_encode($videoDraft->meta),
            'status' => $status,
        ];

        $video = $this->videoRepository->create($data);
        if (empty($video)) {
            throw new VideoException(VideoException::FAILED_PUBLISH_VIDEO);
        }

        // Hashtag
        $hashtags = $this->getHashtagsFromDescription($video['description']);
        $syncHashtag = $this->videoRepository->syncHashtag($hashtags, $video);

        if (! $syncHashtag) {
            throw new VideoException(VideoException::SYNC_HASHTAG_FAIL);
        }

        $videoDraft->delete();

        return $videoDraft;
    }

    public function verifyVideoDraft(string $videoId, User $user)
    {
        $videoDraft = $this->videoDraftRepository->getDetailVideoDraft($videoId);

        if (! $videoDraft) {
            throw new VideoException(VideoException::VIDEO_DRAFT_NOT_FOUND);
        }

        if ($user->id !== $videoDraft->user_id) {
            throw new VideoException(VideoException::NOT_AUTHOR);
        }

        return $videoDraft;
    }

    public function getDetailVideoDraft(string $videoId, User $user)
    {
        $video = $this->verifyVideoDraft($videoId, $user);

        return $video;
    }

    public function updateVideoDraft(array $data, User $user)
    {
        $videoDraft = $this->verifyVideoDraft($data['video_id'], $user);
        $videoDraft = $this->videoDraftRepository->updateVideoDraft($videoDraft, $data);

        return $videoDraft;
    }

    public function deleteVideoDraft(array $videoIds, User $user)
    {
        try {
            \DB::beginTransaction();

            foreach ($videoIds as $videoId) {
                $videoDraft = $this->verifyVideoDraft($videoId, $user);
                $videoDraft = $this->videoDraftRepository->deleteVideoDraft($videoDraft);
            }

            \DB::commit();

        } catch (\Exception $e) {
            \DB::rollBack();
            throw $e;
        }
    }

    public function getVideoPrivate(string $userId, int $perPage = null, int $page)
    {
        return $this->videoRepository->getVideoPrivate($userId, $perPage, $page);
    }

    public function getHashtagsFromDescription($description)
    {
        $content = preg_split('/((?:#|@|[^\s]+)(?:[^\s=+\-!$%^&*(),.~`;:<>?\'"]+))/', $description, -1, PREG_SPLIT_DELIM_CAPTURE);

        $hashtags = [];
        foreach ($content as $value) {
            if (strpos($value, '#') !== false) {
                $subValues = explode('#', $value);
                $subValues = array_filter($subValues);
                $hashtags = array_merge($hashtags, $subValues);
            }
        }

        $hashtags = array_unique($hashtags);

        return $hashtags;
    }

    public function searchByHashtag(string $hashtag, int $perPage = null)
    {
        $hashtag = trim($hashtag);

        return $this->videoRepository->searchByHashtag($hashtag, $perPage);
    }

    public function updateStatus(Video $video, array $data)
    {
        $video = $this->videoRepository->updateVideo($video, $data);

        return $video;
    }

    public function getVideosBE(int $perPage = null)
    {
        return $this->videoRepository->getVideosBE($perPage);
    }

    public function deleteVideo(Video $video)
    {
        $res = $this->videoRepository->deleteVideo($video);

        if (!$res) {
            throw new VideoException(VideoException::VIDEO_NOT_FOUND);
        }

        return $res;
    }

    public function search(string $keyword = null)
    {
        return $this->videoRepository->search($keyword);
    }

    public function getTopVideos(int $perPage = null)
    {
        return $this->videoRepository->getTopVideos($perPage);
    }
}
