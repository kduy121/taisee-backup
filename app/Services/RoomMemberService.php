<?php

namespace App\Services;

use App\Exceptions\ChatException;
use App\Repositories\RoomMemberRepository;
use App\Repositories\RoomRepository;
use Illuminate\Support\Facades\Auth;

class RoomMemberService
{
    public function __construct(private RoomMemberRepository $repo, private RoomRepository $roomRepo)
    {
    }

    public function getMember(string $roomId, $perPage = 25, int $page)
    {
        // Check room
        $user = Auth::user();
        $room = $this->roomRepo->find($roomId);

        if (! $room) {
            throw new ChatException(ChatException::ROOM_NOT_FOUND);
        }

        $members = $this->repo->getMember($roomId, 'room_id', $perPage, $page);

        return $members;
    }
}
