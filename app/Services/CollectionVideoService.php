<?php

namespace App\Services;

use App\Enums\VideoStatus;
use App\Exceptions\CollectionException;
use App\Exceptions\VideoException;
use App\Models\CollectionVideo;
use App\Repositories\CollectionRepository;
use App\Repositories\CollectionVideoRepository;
use App\Repositories\VideoRepository;

class CollectionVideoService
{
    public function __construct(
        private CollectionRepository $collectionRepository,
        private CollectionVideoRepository $collectionVideoRepository,
        private VideoRepository $videoRepository)
    {
    }

    public function bookmarkVideo(string $userId, string $videoId, string $collectionId = null): CollectionVideo
    {
        $collectionVideos = $this->bookmarkVideos($userId, [$videoId], $collectionId);

        return isset($collectionVideos[0]) ? $collectionVideos[0] : null;
    }

    public function bookmarkVideos(string $userId, array $videoIds, string $collectionId = null)
    {
        $oldCollectionIds = [];
        foreach ($videoIds as $videoId) {
            $video = $this->videoRepository->find($videoId);
            if (empty($video) || ($video->status == VideoStatus::Private->value && $video->user_id != $userId)) {
                throw new VideoException(VideoException::VIDEO_NOT_FOUND);
            }
            $oldCollection = $this->collectionVideoRepository->getByUserIdVideoId($userId, $videoId);
            if (! empty($oldCollection)) {
                $oldCollectionIds[] = $oldCollection->collection_id;
            }
        }

        // Check collection id
        if (! empty($collectionId)) {
            $collection = $this->collectionRepository->find($collectionId);

            if (empty($collection)) {
                throw new CollectionException(CollectionException::COLLECTION_NOT_FOUND);
            }
        }

        // Bookmark video
        $collectionVideos = [];
        foreach ($videoIds as $videoId) {
            $collectionVideos[] = $this->collectionVideoRepository->createOrUpdate($userId, $videoId, $collectionId);
        }
        $this->refreshCollectionInfo($userId, $collectionId);
        foreach ($oldCollectionIds as $oldCollectionId) {
            $this->refreshCollectionInfo($userId, $oldCollectionId);
        }

        return $collectionVideos;
    }

    public function unbookmarkVideo($userId, $videoId)
    {
        $video = $this->videoRepository->find($videoId);
        if (empty($video)) {
            throw new VideoException(VideoException::VIDEO_NOT_FOUND);
        }

        $collectionVideo = $this->collectionVideoRepository->getByUserIdVideoId($userId, $videoId);
        if (empty($collectionVideo)) {
            throw new CollectionException(CollectionException::COLLECTION_NOT_FOUND);
        }

        $result = $collectionVideo->delete();
        $this->refreshCollectionInfo($userId, $collectionVideo->collection_id);

        return $result;

    }

    public function getVideo(string $userId, string $collectionId = null, $perPage = 10, int $page)
    {
        $collectionVideos = $this->collectionVideoRepository->getVideo($userId, $collectionId, $perPage, $page);

        foreach ($collectionVideos as $collectionVideo) {
            $video = $collectionVideo->video;

            if ($video && $video->status == VideoStatus::Private->value && $video->user_id != $userId) {
                foreach ($video->getFillable() as $attribute) {
                    $video->{$attribute} = null;
                }
            }
        }

        return $collectionVideos;
    }

    protected function refreshCollectionInfo(string $userId, string $collectionId = null)
    {
        if (is_null($collectionId)) {
            return;
        }

        $collection = $this->collectionRepository->find($collectionId);
        if (empty($collection)) {
            throw new CollectionException(CollectionException::COLLECTION_NOT_FOUND);
        }

        $collectionVideo = $this->collectionVideoRepository->getOneVideo($userId, $collectionId);
        $video = ! empty($collectionVideo) ? $this->videoRepository->find($collectionVideo->video_id) : null;
        $thumnail = null;
        if (! empty($video) && $video->status === VideoStatus::Public->value) {
            $thumnail = $video->thumbnail;
        }

        $countVideo = $this->collectionVideoRepository->countVideoByCollectionId($collectionId);

        $result = $this->collectionRepository->update($collection, [
            'thumbnail' => $thumnail,
            'video_count' => $countVideo,
            'latest_video_id' => $video->id ?? null,
        ]);

        return $result;
    }
}
