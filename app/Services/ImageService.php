<?php

namespace App\Services;

use App\Exceptions\ImageException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class ImageService
{
    public function handleUpload(string $userId, Request $request)
    {
        $content = $request->input('content');
        $fileType = $request->input('file_type');

        $decodedImage = base64_decode($content);
        if ($decodedImage === false) {
            throw new ImageException(ImageException::FILE_IS_NOT_BASE64);
        }

        $imageSize = strlen($decodedImage) / 1024 / 1024;
        $maxSize = Config::get('app.max_file_upload');
        if ($imageSize > $maxSize) {
            throw new ImageException(ImageException::MAXSIZE_UPLOAD, 'Max size upload is '.$maxSize.'MB');
        }

        $fileName = sprintf('%s_%s.%s', $userId, time(), $fileType);
        $filePath = sprintf('%s/%s/%s', public_path(), Config::get('app.avatar_upload_path'), $fileName);
        file_put_contents($filePath, $decodedImage);

        $resulst = sprintf('%s/%s/%s', Config::get('app.url'), Config::get('app.avatar_upload_path'), $fileName);

        return $resulst;
    }

    public function deleteImageUpload(string $imagePath)
    {
        $parser = parse_url($imagePath);
        if (empty($parser['path'])) {
            return false;
        }

        $filePath = public_path().$parser['path'];
        if (! file_exists($filePath)) {
            return false;
        }

        return unlink($filePath);
    }
}
