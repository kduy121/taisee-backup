<?php

namespace App\Services;

use App\Exceptions\AuthException;
use App\Models\User;
use App\Repositories\AuthRepository;
use DateTimeImmutable;
use Lcobucci\JWT\Encoding\ChainedFormatter;
use Lcobucci\JWT\Encoding\JoseEncoder;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\Signer\Key\InMemory;
use Lcobucci\JWT\Token\Builder;
use Lcobucci\JWT\Token\Parser;
use Lcobucci\JWT\UnencryptedToken;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthService
{
    private $expireTokenTime;

    private $signingKey;

    public function __construct(private AuthRepository $authRepo)
    {
        $this->expireTokenTime = config('constants.refresh_token.expire_token_time');
        $this->signingKey = config('constants.refresh_token.signingKey');
    }

    public function createRefreshToken(User $user, string $deviceId, bool $isUpdate = false)
    {
        if (! $isUpdate) {
            $refreshToken = $this->authRepo->getRefreshToken($user, $deviceId);

            if ($refreshToken && $this->checkValidToken($refreshToken->refresh_token, $deviceId)) {
                return $refreshToken->refresh_token;
            }
        }

        $tokenBuilder = (new Builder(new JoseEncoder(), ChainedFormatter::default()));
        $algorithm = new Sha256();
        $signingKey = InMemory::base64Encoded($this->signingKey);

        $now = new DateTimeImmutable();
        $refreshToken = $tokenBuilder
            ->identifiedBy($user->id)
            ->issuedAt($now)
            ->canOnlyBeUsedAfter($now->modify('+1 second'))
            ->expiresAt($now->modify("+ $this->expireTokenTime minutes"))
            ->withClaim('finger_hash', md5($deviceId))
            ->withClaim('sub_id', $user->id)
            ->getToken($algorithm, $signingKey);

        $refreshToken = $refreshToken->toString();

        if (! $refreshToken) {
            throw new AuthException(AuthException::CREATE_TOKEN_FAIL);
        }

        $this->authRepo->createRefreshToken($user, $deviceId, $refreshToken);

        return $refreshToken;
    }

    public function checkToken(string $deviceId, User $user, string $refreshToken)
    {
        $token = $this->authRepo->getRefreshToken($user, $deviceId);
        if (empty($token)) {
            throw new AuthException(AuthException::REFRESH_TOKEN_NOT_FOUND);
        }
        if ($token->refresh_token !== $refreshToken) {
            throw new AuthException(AuthException::REFRESH_TOKEN_NOT_FOUND);
        }
    }

    public function getUser(string $userId)
    {
        return $this->authRepo->getUser($userId);
    }

    public function refreshToken(string $refreshToken, string $deviceId)
    {
        $isValid = $this->checkValidToken($refreshToken, $deviceId);
        if (! $isValid) {
            $this->authRepo->deleteToken($refreshToken);
            throw new AuthException(AuthException::REFRESH_TOKEN_INVALID);
        }

        // Get user by token
        $payload = $this->parsToken($refreshToken);
        $user = $this->getUser($payload['sub_id']);

        // Check token exists in db
        $this->checkToken($deviceId, $user, $refreshToken);

        // Generate new token
        $newToken = JWTAuth::fromUser($user);

        // Generate new refresh token
        $updateToken = true;
        $newRefreshToken = $this->createRefreshToken($user, $deviceId, $updateToken);

        return [
            'newToken' => $newToken,
            'newRefreshToken' => $newRefreshToken,
            'user' => $user,
        ];
    }

    protected function checkValidToken(string $refreshToken, string $deviceId)
    {
        $payload = $this->parsToken($refreshToken);

        if (! isset($payload['exp'], $payload['finger_hash'], $payload['sub_id'])) {
            return false;
        }

        // Check ttl
        $isValid = $this->checkTTL($payload['exp']);
        if (! $isValid) {
            return false;
        }

        // check device id
        $deviceIdHash = $payload['finger_hash'];
        if (md5($deviceId) !== $deviceIdHash) {
            return false;
        }

        return true;
    }

    public function parsToken(string $token)
    {
        $parser = new Parser(new JoseEncoder());
        $token = $parser->parse($token);
        assert($token instanceof UnencryptedToken);
        $payload = $token->claims()->all();

        return $payload;
    }

    /**
     * Check the expiration time and return the result.
     *
     * @return bool
     *   - false if the expiration time has passed.
     *   - true if the expiration time is still valid.
     */
    public function checkTTL($expirationTime): bool
    {
        $expirationTime = $expirationTime->getTimestamp();
        $currentTimestamp = time();

        if ($expirationTime < $currentTimestamp) {
            return false;
        }

        return true;
    }
}
