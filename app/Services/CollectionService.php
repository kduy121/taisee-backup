<?php

namespace App\Services;

use App\Enums\VideoStatus;
use App\Exceptions\CollectionException;
use App\Models\Collection;
use App\Repositories\CollectionRepository;
use App\Repositories\CollectionVideoRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

class CollectionService
{
    public function __construct(private CollectionRepository $collectionRepository,
        private CollectionVideoRepository $collectionVideoRepository)
    {
    }

    public function getCollection(string $userId, string $excludeCollectionId = null, int $perPage = 10, int $page = 1)
    {
        $collections = $this->collectionRepository->getAllWithPaginate($userId, $excludeCollectionId, $perPage, $page);
        $count = $this->collectionVideoRepository->countVideoByUserId($userId);

        if ($page > 1 || ! empty($excludeCollectionId)
            || (empty($collections->total()) && $count === 0)) {
            return $collections;
        }

        $collectionVideo = $this->collectionVideoRepository->getOneVideo($userId, Config::get('app.all_post_id'));
        $thumnail = null;
        $video = ! empty($collectionVideo) ? $collectionVideo->video : null;
        if ((! empty($video) && $video->status === VideoStatus::Public->value) || ($video && $video->user_id == $userId)) {
            $thumnail = $video->thumbnail;
        }

        $allPost = new Collection([
            'id' => Config::get('app.all_post_id'),
            'name' => 'All Posts',
            'user_id' => $userId,
            'public' => 1,
            'thumbnail' => $thumnail,
            'video_count' => $count,
        ]);

        // Get thumbnail of owner video
        foreach ($collections as $collection) {
            $videoCount = count($collection->videos);
            if ($videoCount) {
                if ($collection->videos[0]->user_id == $userId) {
                    $collection->thumbnail = $collection->videos[0]->thumbnail;
                }
            }
        }

        $collections->prepend($allPost);

        return $collections;
    }

    public function createCollection(array $data, string $userId): Collection
    {
        $data['user_id'] = $userId;
        $collection = $this->collectionRepository->create($data);
        $collection->load('user', 'videos');

        if (! $collection) {
            throw new CollectionException(CollectionException::COLLECTION_CREATE_ERR);
        }

        return $collection;
    }

    public function delete(string $userId, string $collectionId)
    {
        // check owner of collection
        $this->collectionRepository->checkCollectionOwnership($userId, $collectionId);
        $this->collectionVideoRepository->deleteByCollectionIdUserId($userId, $collectionId);

        return $this->collectionRepository->delete($collectionId);
    }

    public function renameCollection(string $collectionId, string $name)
    {
        $userId = Auth::user()->id;
        $data['name'] = $name;

        $collection = $this->collectionRepository->getCollection($userId, $collectionId);

        return $this->collectionRepository->update($collection, $data);
    }
}
