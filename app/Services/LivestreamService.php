<?php

namespace App\Services;

use App\Enums\LivestreamStatus;
use App\Exceptions\LivestreamException;
use App\Models\User;
use App\Repositories\LivestreamRepository;
use App\Repositories\UserRepository;

class LivestreamService
{
    public function __construct(
        private LivestreamRepository $livestreamRepository,
        private UserRepository $userRepository)
    {
    }

    public function getAllLivestreams(int $perPage = null, int $page)
    {
        return $this->livestreamRepository->getAllLivestreams($perPage, $page);
    }

    public function createLivestream(array $data, User $user)
    {
        if (empty($data['cover'])) {
            $data['cover'] = $data['avatar_url'];
        }

        $livestream = $this->livestreamRepository->createLivestream($data, $user);

        if (! $livestream) {
            throw new LivestreamException(LivestreamException::CREATE_FAIL);
        }

        return $livestream;
    }

    public function getLivestreamById(string $id, string $userId = null)
    {
        $livestream = $this->livestreamRepository->getById($id);

        if (! $livestream || ($livestream && $livestream->status == LivestreamStatus::Practice->value &&
            $userId != $livestream->user_id)) {
            throw new LivestreamException(LivestreamException::LIVESTREAM_NOT_FOUND);
        }

        return $livestream;
    }

    public function updateLivestream(array $data, User $user)
    {
        $this->livestreamRepository->validateData($data['livestream_id']);
        $livestream = $this->livestreamRepository->updateLivestream($data, $user);

        return $livestream;
    }

    public function deleteLivestream(array $data)
    {
        $this->livestreamRepository->validateData($data['livestream_id']);

        return $this->livestreamRepository->delete($data['livestream_id']);
    }
}
