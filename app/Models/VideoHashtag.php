<?php

namespace App\Models;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VideoHashtag extends Model
{
    use HasFactory, Uuids;

    protected $table = 'video_hashtags';

    public $incrementing = false;

    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'video_id',
        'hashtag_id',
    ];

    public function video()
    {
        return $this->belongsTo(Video::class);
    }

    public function hashtag()
    {
        return $this->belongsTo(Hashtag::class);
    }
}
