<?php

namespace App\Models;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CollectionVideo extends Model
{
    use HasFactory, Uuids;

    protected $table = 'collection_videos';

    public $incrementing = false;

    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'collection_id',
        'video_id',
        'user_id',
        'created_at',
        'updated_at',
    ];

    public function collection()
    {
        return $this->belongsTo(Collection::class);
    }

    public function video()
    {
        return $this->belongsTo(Video::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
