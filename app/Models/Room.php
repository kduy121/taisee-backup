<?php

namespace App\Models;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    use HasFactory, Uuids;

    public $incrementing = false;

    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'user_id',
        'type',
        'avatar_ids',
        'thumb_ids',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'avatar_ids' => 'array',
        'thumb_ids' => 'array',
    ];

    public function members()
    {
        return $this->belongsToMany(User::class, 'room_members', 'room_id', 'user_id')
            ->withPivot('is_admin', 'created_at', 'pinned');
    }

    public function messages()
    {
        return $this->hasMany(Message::class);
    }

    public function lastMessage()
    {
        return $this->hasOne(Message::class)->orderBy('updated_at', 'desc');
    }

    public function getFirstRoomMembers($room)
    {
        return $room->members()
            ->orderBy('room_members.created_at', 'desc')
            ->take(10)
            ->get();
    }

    public function userSeens()
    {
        return $this->hasMany(UserSeen::class);
    }
}
