<?php

namespace App\Models;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VideoDraft extends Model
{
    use HasFactory, Uuids;

    public $incrementing = false;

    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'video_privacy',
        'user_id',
        'title',
        'description',
        'file_path',
        'streaming_path',
        'thumbnail',
        'byteplus_video_id',
        'meta',
    ];

    public function getMetaAttribute($value)
    {
        return json_decode($value, true);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
