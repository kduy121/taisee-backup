<?php

namespace App\Models;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;

class Collection extends Model
{
    use Uuids;

    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'user_id',
        'thumbnail',
        'video_count',
        'latest_video_id',
        'public',
    ];

    protected $attributes = [
        'public' => 1,
        'video_count' => 0,
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function videos()
    {
        return $this->belongsToMany(Video::class, 'collection_videos');
    }
}
