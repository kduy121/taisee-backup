<?php

namespace App\Models;

// use App\Models\Traits\Scopes\PermissionScopes;
// use Stancl\Tenancy\Database\Concerns\CentralConnection;
use Spatie\Permission\Models\Permission as BasePermission;

/**
 * Class Permission.
 */
class Permission extends BasePermission
{
    // use CentralConnection, PermissionScopes;

    protected $table = 'permissions';

    protected $guarded = [];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'display_name', 'sort', 'guard_name', 'group_id'];
}
