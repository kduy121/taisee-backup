<?php

namespace App\Models;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Video extends Model
{
    use HasFactory, Uuids, SoftDeletes;

    public $incrementing = false;

    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'title',
        'description',
        'file_path',
        'streaming_path',
        'thumbnail',
        'status',
        'byteplus_video_id',
        'meta',
        'is_live',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function likes()
    {
        return $this->hasMany(Like::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function collections()
    {
        return $this->belongsToMany(Collection::class, 'collection_videos');
    }

    public function collectionVideos()
    {
        return $this->hasMany(CollectionVideo::class);
    }

    public function getMetaAttribute($value)
    {
        return json_decode($value, true);
    }

    public function hashtags()
    {
        return $this->belongsToMany(Hashtag::class, 'video_hashtags');
    }

    public function videoHashtags()
    {
        return $this->hasMany(VideoHashtag::class);
    }
}
