<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use App\Enums\SocialiteType;
use App\Enums\VideoStatus;
use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Config;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasPermissions;
use Spatie\Permission\Traits\HasRoles;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use HasApiTokens, HasFactory, Notifiable, SoftDeletes, Uuids; // HasRoles, HasPermissions

    public $incrementing = false;

    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'remember_token',
        'socialite_id',
        'socialite_type',
        'device_info',
        'date_of_birth',
        'gender',
        'created_by',
        'updated_by',
        'deleted_by',
        'user_name',
        'bio',
        'avatar_url',
        'status',
        'lang',
    ];

    protected $guarded = [
        'remember_token',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
        'deleted_at',
        'created_at',
        'updated_at',
        'email_verified_at',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
        'device_info' => 'json',
        'is_live' => 'boolean',
        'status' => 'boolean',
    ];

    public function trashed()
    {
        return $this->deleted_at !== null;
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function getAccountType(): SocialiteType
    {
        return SocialiteType::from($this->socialite_type);
    }

    public function setAccountType(SocialiteType $socialite_type)
    {
        $this->socialite_type = $socialite_type->name;
    }

    public function interests()
    {
        return $this->belongsToMany(Interest::class, 'user_interest');
    }

    public function areas()
    {
        return $this->belongsToMany(Area::class);
    }

    public function videos()
    {
        return $this->hasMany(Video::class);
    }

    public function likes()
    {
        return $this->hasMany(Like::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function followers()
    {
        return $this->belongsToMany(User::class, 'followers', 'following_id', 'follower_id')
            ->withPivot('is_friend');
    }

    public function following()
    {
        return $this->belongsToMany(User::class, 'followers', 'follower_id', 'following_id')
            ->withPivot('is_friend');
    }

    public function collections()
    {
        return $this->hasMany(Collection::class);
    }

    public function collectionVideos()
    {
        return $this->hasMany(CollectionVideo::class);
    }

    public function rooms()
    {
        return $this->belongsToMany(Room::class, 'room_members', 'user_id', 'room_id')
            ->withPivot('is_admin');
    }

    public function messages()
    {
        return $this->hasMany(Message::class);
    }

    public function getAvatarUrlAttribute($value)
    {
        return is_null($value) ? sprintf('%s/%s', Config::get('app.url'), Config::get('app.avatar_default_path')) : $value;
    }

    public function latestVideo()
    {
        return $this->hasOne(Video::class)->where('status', VideoStatus::Public->value)->latest('created_at');
    }

    public function refreshToken()
    {
        return $this->hasOne(RefreshToken::class);
    }

    public function videoDrafts(int $perPage)
    {
        return $this->hasMany(VideoDraft::class)->paginate($perPage);
    }
}
