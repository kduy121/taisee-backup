<?php

namespace App\Api\Controllers;

// use EllipseSynergie\ApiResponse\Laravel\Response;
use App\Exceptions\CommonException;
use App\Http\Response\CustomResponse;
use App\Repositories\BaseRepository;
use EllipseSynergie\ApiResponse\Contracts\Response;
use Illuminate\Http\Request;
use Illuminate\Http\Response as ResponseStatus;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Validator as ValidatorInstance;

/**
 * @OA\Info(title="Taisee API", version="0.1")
 */
class ApiController extends BaseController
{
    /**
     * @var Request
     */
    public $request;

    /**
     * @var Response
     */
    public $response;

    /**
     * @var BaseRepository
     */
    public $repo;

    public $transformer;

    public $expireTokenTime;

    public function __construct(Response $response, Request $request)
    {
        $this->response = $response;
        $this->request = $request;
        $this->repo = $this->getRepo();
        $this->transformer = $this->getTransformer();
        $this->expireTokenTime = config('constants.token.expire_token_time');
    }

    public function getRepo()
    {
    }

    public function getTransformer()
    {
        throw new \Exception('The transformer undefined! Need override getTransformer function!');
    }

    public function destroy($id)
    {

    }

    public function show($id)
    {

    }

    protected function respondWithToken($token, $user = null, $message = null, $refreshToken = null)
    {
        return response()->json([
            'data' => [
                'message' => $message,
                'user' => $user,
                'token' => $token,
                'refresh_token' => $refreshToken,
                'expires_in' => auth('api')->factory()->getTTL() * 1000,
            ],
        ], 200);
    }

    protected function respondFail()
    {
        return response()->json([
            'data' => [
                'message' => 'Oops! Access token verification failed. Please double-check the token and give it another try',
            ],
        ], 401);
    }

    protected function respondUnauthorized()
    {
        return response()->json([
            'data' => [
                'status' => 'fail',
                'message' => CustomResponse::$statusTexts[CustomResponse::HTTP_UNAUTHORIZED],
                'code' => CustomResponse::HTTP_UNAUTHORIZED,
            ],
        ], ResponseStatus::HTTP_UNAUTHORIZED);
    }

    protected function validateFail($validator)
    {
        throw new CommonException(CommonException::DEFAULT_ERR_CODE);
    }

    /**
     * validate data with the rules.
     */
    public function validate($data = [], $rules = []): ValidatorInstance
    {
        return Validator::make($data, $rules);
    }
}
