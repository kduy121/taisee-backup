<?php

namespace App\Api\Controllers;

use App\Http\Requests\Livestream\CreateCommentLivestreamRequest;
use App\Http\Requests\Livestream\DeleteCommentLivestreamRequest;
use App\Http\Requests\Livestream\GetCommentLivestreamRequest;
use App\Http\Requests\Livestream\UpdateCommentLivestreamRequest;
use App\Services\CommentLivestreamService;
use App\Transformers\CommentLivestreamTransformer;
use EllipseSynergie\ApiResponse\Contracts\Response as ApiResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class CommentLivestreamController extends ApiController
{
    public function __construct(
        ApiResponse $response,
        Request $request,
        private CommentLivestreamService $service,
        private CommentLivestreamTransformer $transformer2
    ) {
        parent::__construct($response, $request);
    }

    public function getTransformer()
    {
        return new CommentLivestreamTransformer();
    }

    /**
     * @OA\GET(
     *     path="/api/livestream/comment",
     *     tags={"Comment Livestream"},
     *     summary="Get comment livestream",
     *     security={{"bearerAuth":{}}},
     *
     *     @OA\Parameter(
     *         name="livestream_id",
     *         in="query",
     *         required=true,
     *
     *         @OA\Schema(type="string"), example="ceebe02f-8d26-43ef-b544-287f74d55bbd", description="livestream_id"),
     *
     *     @OA\Response(response="200", description="success", @OA\JsonContent(ref="livestream-response.json#/comment-response")),
     * )
     */
    public function index(GetCommentLivestreamRequest $request)
    {
        $perPage = $request->per_page;
        $page = $request->page ?? 1;
        $livestreamId = $request->livestream_id;
        $userId = Auth::guard('api')->user()->id ?? null;

        $comment = $this->service->getAllComments($perPage, $livestreamId, $userId, $page);

        return $this->response->withPaginator($comment, $this->transformer);
    }

    /**
     * @OA\POST(
     *     path="/api/livestream/comment",
     *     tags={"Comment Livestream"},
     *     summary="Create comment Livestream",
     *     security={{"bearerAuth":{}}},
     *
     *     @OA\RequestBody(@OA\JsonContent(ref="livestream-request.json#/comment-create")),
     *
     *     @OA\Response(response="200", description="success", @OA\JsonContent(ref="livestream-response.json#/comment-response")),
     *     @OA\Response(response="422", description="Unprocessable Content"),
     *     @OA\Response(response="500", description="Err", @OA\JsonContent(ref="livestream-response.json#/err"))
     * )
     */
    public function create(CreateCommentLivestreamRequest $request)
    {
        $user = Auth::user();
        $data = $request->all();
        $data['user_id'] = $user->id;

        $comment = $this->service->createComment($data);

        return $this->response->withItem($comment, $this->transformer2);
    }

    /**
     * @OA\PUT(
     *     path="/api/livestream/comment",
     *     tags={"Comment Livestream"},
     *     summary="Update comment Livestream",
     *     security={{"bearerAuth":{}}},
     *
     *     @OA\RequestBody(@OA\JsonContent(ref="livestream-request.json#/comment-update")),
     *
     *     @OA\Response(response="200", description="success", @OA\JsonContent(ref="livestream-response.json#/comment-response")),
     *     @OA\Response(response="422", description="Unprocessable Content"),
     *     @OA\Response(response="500", description="Err", @OA\JsonContent(ref="livestream-response.json#/err"))
     * )
     */
    public function updateComment(UpdateCommentLivestreamRequest $request)
    {
        $data = $request->all();
        $data['user_id'] = Auth::user()->id;

        $comment = $this->service->updateCommentLivestream($data);

        return $this->response->withItem($comment, $this->transformer);
    }

    /**
     * @OA\DELETE(
     *     path="/api/livestream/comment",
     *     tags={"Comment Livestream"},
     *     summary="Delete comment livestream",
     *     security={{"bearerAuth":{}}},
     *
     *     @OA\RequestBody(@OA\JsonContent(ref="livestream-request.json#/comment-id")),
     *
     *     @OA\Response(response="200", description="success", @OA\JsonContent(ref="livestream-response.json#/delete")),
     *     @OA\Response(response="500", description="Err", @OA\JsonContent(ref="livestream-response.json#/err"))
     * )
     */
    public function delete(DeleteCommentLivestreamRequest $request)
    {
        $commentId = $request->comment_id;
        $data = $request->all();
        $data['user_id'] = Auth::user()->id;

        $this->service->deleteCommentLivestream($data);

        return response()->json([
            'message' => "Comment: $commentId delete successful",
        ], Response::HTTP_OK);
    }
}
