<?php

namespace App\Api\Controllers;

use App\Models\Area;
use App\Models\Interest;
use App\Repositories\InterestRepository;
use App\Transformers\InterestTransformer;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

class CommonController extends ApiController
{
    /**
     * @var \App\Repositories\InterestRepository
     */
    public $repo;

    public function getRepo()
    {
        return new InterestRepository();
    }

    public function getTransformer()
    {
        return new InterestTransformer();
    }

    public function __construct()
    {
        // $this->middleware('auth.token', []);
    }

    /**
     * @OA\GET(
     *     path="/api/common",
     *     tags={"Other"},
     *     summary="Get common",
     *
     *     @OA\Response(response="200", description="success", @OA\JsonContent(ref="other-response.json#/common-index")),
     * )
     */
    public function index(Request $request)
    {
        $lang = $request->lang ?? 'en';

        $interest = $this->interest($lang);
        $areas = $this->areas($lang);

        return response()->json([
            'data' => [
                'policy' => 13,
                'interest' => $interest,
                'areas' => $areas,
            ],
        ], Response::HTTP_OK);
    }

    public function areas($lang)
    {
        $areas = Area::all();

        $transformedAreas = [];

        foreach ($areas as $area) {
            $keyName = Str::snake($area->address['province']['en']);
            $name = $area->address['province'][$lang];
            $transformedAreas[$keyName] = $name;
        }

        return $transformedAreas;
    }

    public function interest($lang)
    {
        $data = Interest::all();

        $responseData = [];
        foreach ($data as $item) {
            $collection = new Collection($item['interest']);
            $key = $collection['interest']['key'];
            $value = $collection['interest'][$lang];
            $responseData[$key] = $value;
        }

        return $responseData;
    }
}
