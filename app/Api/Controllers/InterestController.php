<?php

namespace App\Api\Controllers;

use App\Models\Interest;
use App\Repositories\InterestRepository;
use App\Transformers\InterestTransformer;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Str;

class InterestController extends ApiController
{
    /**
     * @var \App\Repositories\InterestRepository
     */
    public $repo;

    public function getRepo()
    {
        return new InterestRepository();
    }

    public function getTransformer()
    {
        return new InterestTransformer();
    }

    public function index()
    {
        $user = auth()->user();
        $data = Interest::all();

        $responseData = [];
        foreach ($data as $item) {
            $snakeCase = Str::snake($item->name);
            $responseData[$snakeCase] = $item->name;
        }

        return response()->json([
            'data' => [
                'interest' => $responseData,
            ],
        ], Response::HTTP_OK);
    }

    public function show($id)
    {

    }

    /**
     * @OA\POST(
     *     path="/api/interest/store",
     *     tags={"Other"},
     *     summary="Insert interest",
     *
     *     @OA\RequestBody(@OA\JsonContent(ref="other-request.json#/interest")),
     *
     *     @OA\Response(response="200", description="success", @OA\JsonContent(ref="other-response.json#/interest")),
     *     @OA\Response(response="401", description="Unauthorized", @OA\JsonContent(ref="user-response.json#/invalid")),
     * )
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $data['interest'] = array_map(function ($item) {
            $item = (str_replace('_', ' ', $item));

            return ucfirst($item);
        }, $data['interest']);

        return $this->repo->create($data);
    }

    public function update(Request $request)
    {
        // $data = Interest::findOrFail($id);
        // $data->update($request->all());

        // return response()->json([
        //     'status' => 'success',
        //     'data' => $data,
        // ], 200);
    }

    public function destroy($name)
    {
        // $this->repo->create($data);
        // $data = Interest::findOrFail($id);
        // $data->delete();

        // return response()->json([
        //     'status' => 'success',
        //     'message' => 'Interest deleted successfully.',
        // ], 200);
    }
}
