<?php

namespace App\Api\Controllers;

use App\Services\CollectionService;
use App\Transformers\CollectionTransformer;
use EllipseSynergie\ApiResponse\Contracts\Response as ApiResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class CollectionController extends ApiController
{
    public function __construct(ApiResponse $response, Request $request, private CollectionService $service)
    {
        parent::__construct($response, $request);
    }

    protected $rulesCreate = [
        'name' => 'required|string|max:30',
        'public' => 'sometimes|bool',
    ];

    protected $rulesSaveVideo = [
        'collection_id' => 'nullable|string',
        'video_id' => 'required|string',
    ];

    protected $rulesDelete = [
        'collection_id' => 'required|string',
    ];

    protected $rulesRename = [
        'collection_id' => 'required|string',
        'name' => 'required|string|max:30',
    ];

    public function getRepo()
    {
    }

    public function getTransformer()
    {
        return new CollectionTransformer();
    }

    /**
     * @OA\GET(
     *     path="/api/video/collection",
     *     tags={"Video - Bookmark"},
     *     summary="Get video collection",
     *     security={{"bearerAuth":{}}},
     *
     *     @OA\Parameter(
     *         name="per_page",
     *         in="query",
     *         required=false,
     *
     *         @OA\Schema(type="number"), example="25", description="perpage"),
     *
     *     @OA\Parameter(
     *         name="page",
     *         in="query",
     *         required=false,
     *
     *         @OA\Schema(type="number"), example="1", description="page"),
     *
     *     @OA\Response(response="200", description="success", @OA\JsonContent(ref="video-response.json#/collection")),
     *     @OA\Response(response=401, description="Unauthorized", @OA\JsonContent(ref="user-response.json#/invalid")),
     * )
     */
    public function getCollection(Request $request)
    {
        $perPage = $request->input('per_page');
        $page = $request->input('page', 1);
        $excludeCollectionId = $request->input('exclude_collection_id', '');
        $userId = Auth::user()->id;

        $collection = $this->service->getCollection($userId, $excludeCollectionId, $perPage, $page);

        return $this->response->withPaginator($collection, $this->transformer);
    }

    /**
     * @OA\POST(
     *     path="/api/video/collection",
     *     tags={"Video - Bookmark"},
     *     summary="Create collection",
     *     security={{"bearerAuth":{}}},
     *
     *     @OA\RequestBody(@OA\JsonContent(ref="video-request.json#/create-video-collection")),
     *
     *     @OA\Response(response="200", description="success", @OA\JsonContent(ref="video-response.json#/collection")),
     *     @OA\Response(response="401", description="Unauthorized", @OA\JsonContent(ref="user-response.json#/invalid"))
     * )
     */
    public function create(Request $request)
    {
        $data = $request->all();
        $userId = Auth::user()->id;

        $validator = $this->validate($data, $this->rulesCreate);

        if ($validator->fails()) {
            return $this->validateFail($validator);
        }

        $collection = $this->service->createCollection($data, $userId);

        return $this->response->withItem($collection, $this->transformer);
    }

    /**
     * @OA\DELETE(
     *     path="/api/video/collection",
     *     tags={"Video - Bookmark"},
     *     summary="Delete collection",
     *     security={{"bearerAuth":{}}},
     *
     *     @OA\RequestBody(@OA\JsonContent(ref="video-request.json#/delete-collection")),
     *
     *     @OA\Response(response="200", description="success", @OA\JsonContent(ref="video-response.json#/delete")),
     *     @OA\Response(response=401, description="Unauthorized", @OA\JsonContent(ref="user-response.json#/invalid")),
     *     @OA\Response(response=403, description="Forbidden."),
     * )
     */
    public function delete(Request $request)
    {
        $userId = Auth::user()->id;

        $validator = $this->validate($request->all(), $this->rulesDelete);

        if ($validator->fails()) {
            return $this->validateFail($validator);
        }

        $this->service->delete($userId, $request->collection_id);

        return response()->json([
            'data' => [
                'message' => "Collection: $request->collection_id delete successful",
            ],
        ], Response::HTTP_OK);
    }

    /**
     * @OA\PUT(
     *     path="/api/video/collection",
     *     tags={"Video - Bookmark"},
     *     summary="Rename collection",
     *     security={{"bearerAuth":{}}},
     *
     *     @OA\RequestBody(@OA\JsonContent(ref="video-request.json#/rename-collection")),
     *
     *     @OA\Response(response="200", description="success", @OA\JsonContent(ref="video-response.json#/collection-video")),
     *     @OA\Response(response="401", description="Unauthorized", @OA\JsonContent(ref="user-response.json#/invalid")),
     *     @OA\Response(response="403", description="Forbidden."),
     * )
     */
    public function renameCollection(Request $request)
    {
        $collectionId = $request->collection_id;
        $name = $request->name;

        $validator = $this->validate($request->all(), $this->rulesRename);

        if ($validator->fails()) {
            return $this->validateFail($validator);
        }

        $collection = $this->service->renameCollection($collectionId, $name);

        return $this->response->withItem($collection, $this->transformer);
    }
}
