<?php

namespace App\Api\Controllers;

use App\Enums\VideoStatus;
use App\Enums\Lang;
use App\Exceptions\VideoException;
use App\Http\Requests\User\SearchUser;
use App\Http\Requests\User\UpdateStatus;
use App\Http\Requests\User\GetAdmin;
use App\Http\Requests\User\UpdateAdmin;
use App\Models\Like;
use App\Models\Admin;
use App\Models\User;
use App\Models\Video;
use App\Repositories\AreaRepository;
use App\Repositories\LikeRepository;
use App\Repositories\UserRepository;
use App\Services\ImageService;
use App\Services\UserService;
use App\Transformers\AdminTransformer;
use App\Transformers\FollowerTransformer;
use App\Transformers\FollowTransformer;
use App\Transformers\LikeTransformer;
use App\Transformers\UserTransformer;
use EllipseSynergie\ApiResponse\Contracts\Response as ApiResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Magic;
use Tymon\JWTAuth\Facades\JWTAuth;

class UserController extends ApiController
{
    public function __construct(
        ApiResponse $response,
        Request $request,
        private UserService $service,
        private ImageService $imageService,
        private FollowerTransformer $followerTransformer,
        private AdminTransformer $adminTransformer
    ) {
        parent::__construct($response, $request);
    }

    public function getRepo()
    {
        return new UserRepository();
    }

    public function getTransformer()
    {
        return new UserTransformer();
    }

    /**
     * @OA\POST(
     *     path="/api/update-user",
     *     tags={"User"},
     *     summary="Update user",
     *     security={{"bearerAuth":{}}},
     *
     *     @OA\RequestBody(@OA\JsonContent(ref="user-request.json#/update")),
     *
     *     @OA\Response(response="200", description="Create success", @OA\JsonContent(ref="user-response.json#/user/success")),
     *     @OA\Response(response="401", description="Unauthorized", @OA\JsonContent(ref="user-response.json#/invalid"))
     * )
     */
    public function update(Request $request)
    {
        $user = Auth::user();
        $lang = Lang::EN->value.','.Lang::JA->value;

        $validator = Validator::make($request->all(), [
            'user_name' => [
                'sometimes',
                'string',
                'regex:/^[^\s]+$/',
                'max:24',
                'min:1',
                Rule::unique('users')->ignore($user->id),
            ],
            'lang' => "sometimes|string|in:$lang"
        ]);

        if ($validator->fails()) {
            return $this->validateFail($validator);
        }

        if (empty($request->all())) {
            return response()->json([
                'status' => false,
                'message' => 'TODO',
                // TODO:
            ]);
        }

        if ($request->has('user_name')) {
            $user->updated_by = $user->id;
            $user->user_name = $request->input('user_name');
        }

        if ($request->has('bio')) {
            $user->updated_by = $user->id;
            $user->bio = $request->input('bio');
        }

        if ($request->has('name')) {
            $user->updated_by = $user->id;
            $user->name = $request->input('name');
        }

        if ($request->has('date_of_birth')) {
            $user->updated_by = $user->id;
            $user->date_of_birth = $request->input('date_of_birth');
        }

        if ($request->has('gender')) {
            $user->updated_by = $user->id;
            $user->gender = $request->input('gender');
        }

        if ($request->has('bio')) {
            $user->updated_by = $user->id;
            $user->bio = $request->input('bio');
        }

        $oldAvatarUrl = '';
        if ($request->has('avatar_url') && $user->avatar_url !== $request->input('avatar_url')) {
            $user->updated_by = $user->id;
            $oldAvatarUrl = $user->avatar_url;
            $user->avatar_url = $request->input('avatar_url');
        }

        if (isset($request->area)) {
            $areaRepo = new AreaRepository;
            $user->updated_by = $user->id;

            // Upcase area
            $firstChar = mb_substr($request->area, 0, 1);
            $restOfString = mb_substr($request->area, 1);
            $area = mb_convert_case($firstChar, MB_CASE_TITLE, 'UTF-8').$restOfString;

            $response = $areaRepo->syncData($user, $area);

            if (! $response) {
                return response()->json([
                    'status' => false,
                    'message' => 'Update area has fail',
                ], 500);
            }
        }

        // Language
        if ($request->has('lang')) {
            $user->updated_by = $user->id;
            $user->lang = $request->input('lang');
        }

        $user->save();

        if (! $user) {
            return response()->json([
                'status' => false,
            ]);
        }

        if (! empty($oldAvatarUrl) && strpos(Config::get('app.avatar_default_path'), $oldAvatarUrl) !== false) {
            $this->imageService->deleteImageUpload($oldAvatarUrl);
        }

        return response()->json([
            'data' => [
                'status' => true,
                'message' => 'User updated successfully',
                'user' => $user,
            ],
        ]);
    }

    public function delete(User $user)
    {
        $user->delete();

        return response()->json([
            'data' => [
                'message' => 'Delete user success',
            ],
        ], Response::HTTP_OK);
    }

    /**
     * @OA\GET(
     *     path="/api/check-email",
     *     tags={"User"},
     *     summary="Check if the email exists",
     *
     *     @OA\Parameter(
     *         name="email",
     *         in="query",
     *         required=true,
     *
     *         @OA\Schema(type="string", format="email"), example="abc@yahoo.com", description="Email to be checked"),
     *
     *     @OA\Response(response=200, description="Success response", @OA\JsonContent(ref="user-response.json#/check-email")),
     *     @OA\Response(response=422, description="Unprocessable Content", @OA\JsonContent(ref="user-response.json#/miss-param"))
     * )
     */
    public function checkEmail(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:100',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'data' => [
                    'message' => 'Email is required',
                ],
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $user = User::where('email', $request->email)->first();

        if ($user) {
            return response()->json([
                'data' => [
                    'exists' => true,
                ],
            ], Response::HTTP_OK);
        }

        return response()->json([
            'data' => [
                'exists' => false,
            ],
        ], Response::HTTP_OK);
    }

    /**
     * @OA\POST(
     *     path="/api/delete-user",
     *     tags={"User"},
     *     summary="Delete User By ID For Tester",
     *
     *     @OA\Parameter(
     *         name="id",
     *         in="query",
     *         required=true,
     *
     *         @OA\Schema(type="string"), example="aabc-123", description="Uuid"),
     *
     *     @OA\Response(response=200, description="Success response", @OA\JsonContent(ref="user-response.json#/delete-user"))
     * )
     */
    public function deleteUserForTester(Request $request)
    {
        $id = $request->id;
        $user = User::where('id', $id)->first();

        if ($user) {
            $user->forceDelete();

            return response()->json([
                'data' => [
                    'message' => 'Delete user success',
                ],
            ], Response::HTTP_OK);
        }

        return response()->json([
            'data' => [
                'message' => 'User not found',
            ],
        ], Response::HTTP_NOT_FOUND);
    }

    /**
     * @OA\GET(
     *     path="/api/get-user-info",
     *     tags={"User"},
     *     summary="Get User Info",
     *
     *     @OA\Response(response=200, description="Success response", @OA\JsonContent(ref="auth-response.json#/user-transformer")),
     * )
     */
    public function index()
    {
        $perPage = config('constants.per_page_item');
        $users = User::paginate($perPage);

        return $this->response->withPaginator($users, $this->transformer);
    }

    /**
     * @OA\POST(
     *     path="/api/update-password",
     *     tags={"User"},
     *     summary="Update Password",
     *     security={{"bearerAuth":{}}},
     *
     *     @OA\RequestBody(@OA\JsonContent(ref="user-request.json#/update-password")),
     *
     *     @OA\Response(response="200", description="Update success", @OA\JsonContent(ref="user-response.json#/update-password")),
     *     @OA\Response(response="401", description="Unauthorized", @OA\JsonContent(ref="user-response.json#/invalid"))
     * )
     */
    public function updatePasswordByEmail(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'password' => 'required',
            'accessToken' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'error' => $validator->errors(),
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        // Verify token with Magic Link
        $magic = Magic::user()->get_metadata_by_token($request->accessToken);

        if ($magic) {
            $email = $magic->content->data->email;

            // Check user
            $user = User::where('email', $email)->first();

            if (! $user) {
                return response()->json([
                    'data' => [
                        'message' => 'User not found',
                    ],
                ], Response::HTTP_NOT_FOUND);
            }

            $user->password = Hash::make($request->password);
            $user->save();

            $token = JWTAuth::fromUser($user);

            return $this->respondWithToken($token, $user, 'Update password successfully');
        }

        return response()->json([
            'data' => [
                'message' => 'Access token has been failed',
            ],
        ], Response::HTTP_UNAUTHORIZED);
    }

    /**
     * @OA\POST(
     *     path="/api/like-action",
     *     tags={"User - Like"},
     *     summary="Action Like Video",
     *     security={{"bearerAuth":{}}},
     *
     *     @OA\RequestBody(@OA\JsonContent(ref="user-request.json#/like-video")),
     *
     *     @OA\Response(response="200", description="success", @OA\JsonContent(ref="user-response.json#/like-action/success")),
     *     @OA\Response(response="401", description="Unauthorized", @OA\JsonContent(ref="user-response.json#/invalid")),
     *     @OA\Response(response="500", description="Video not exitst", @OA\JsonContent(ref="user-response.json#/like-action/fail"))
     * )
     */
    public function likeAction(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'video_id' => 'required|string',
        ]);

        if ($validator->fails()) {
            return $this->validateFail($validator);
        }

        $user = Auth::user();
        $userId = $user->id;
        $videoId = $request->video_id;
        $likeTransform = new LikeTransformer;

        // Check video has exists
        $existingVideo = Video::where('id', $videoId)->first();

        if (is_null($existingVideo) || ($existingVideo->status == VideoStatus::Private->value && $existingVideo->user_id != $userId)) {
            throw new VideoException(VideoException::VIDEO_NOT_FOUND);
        }

        // Check user has like video
        $existingLike = Like::where('user_id', $userId)
            ->where([
                ['video_id', $videoId],
                ['user_id', $userId],
            ])
            ->first();

        if ($existingLike) {
            $existingLike->delete();

            $like = $user->likes->where('video_id', $videoId);

            return $this->response->withItem($like, $likeTransform);

        }

        $data = [
            'user_id' => $userId,
            'video_id' => $videoId,
        ];

        $likeRepo = new LikeRepository;
        $likeRepo->create($data);

        $like = $user->likes->where('video_id', $videoId);

        return $this->response->withItem($like, $likeTransform);
    }

    /**
     * @OA\POST(
     *     path="/api/follow-user",
     *     tags={"User - Follow"},
     *     summary="Follow User",
     *     security={{"bearerAuth":{}}},
     *
     *     @OA\RequestBody(@OA\JsonContent(ref="user-request.json#/follow-user")),
     *
     *     @OA\Response(response="200", description="success", @OA\JsonContent(ref="user-response.json#/follow-action/success")),
     *     @OA\Response(response="401", description="Unauthorized", @OA\JsonContent(ref="user-response.json#/invalid")),
     *     @OA\Response(response="400", description="User not found", @OA\JsonContent(ref="user-response.json#/follow-action/fail"))
     * )
     */
    public function followUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'following_id' => 'required|string',
        ]);

        if ($validator->fails()) {
            return $this->validateFail($validator);
        }

        $followTransform = new FollowTransformer;
        $followerId = Auth::user()->id;
        $followingId = $request->following_id;

        $follow = $this->service->followUser($followerId, $followingId);

        return $this->response->withItem($follow, $followTransform);
    }

    /**
     * @OA\GET(
     *     path="/api/user/search",
     *     tags={"User"},
     *     summary="Search User",
     *
     *     @OA\Parameter(
     *         name="name",
     *         in="query",
     *         required=true,
     *
     *         @OA\Schema(type="string"), example="aabc-123", description="name of user"),
     *
     *     @OA\Parameter(
     *         name="per_page",
     *         in="query",
     *         required=false,
     *
     *         @OA\Schema(type="number"), example="25", description="per page"),
     *
     *     @OA\Response(response=200, description="success", @OA\JsonContent(ref="auth-response.json#/user-transformer")),
     *     @OA\Response(response=422, description="Unprocessable Content")
     * )
     */
    public function searchUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'per_page' => 'sometimes|numeric',
        ]);

        if ($validator->fails()) {
            return $this->validateFail($validator);
        }

        $keyword = str_replace('_', '\_', $request->name);
        $perPage = $request->has('per_page') ? $request->per_page : null;

        $users = $this->service->searchUser($keyword, $perPage);

        return $this->response->withPaginator($users, $this->transformer);
    }

    /**
     * @OA\GET(
     *     path="/api/profile",
     *     tags={"User"},
     *     summary="Get profile",
     *
     *     @OA\Parameter(
     *         name="keyword",
     *         in="query",
     *         required=true,
     *
     *         @OA\Schema(type="string"), example="aabc-123", description="name of user"),
     *
     *     @OA\Response(response=200, description="success", @OA\JsonContent(ref="auth-response.json#/user-transformer")),
     *     @OA\Response(response=422, description="Unprocessable Content")
     * )
     */
    public function getProfile(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'keyword' => 'required|string',
        ]);

        if ($validator->fails()) {
            return $this->validateFail($validator);
        }

        $user = $this->service->findUserByUsernameId($request->keyword);

        return $this->response->withItem($user, $this->transformer);
    }

    /**
     * @OA\GET(
     *     path="/api/user/get-follower",
     *     tags={"User - Follow"},
     *     summary="Get Follower",
     *     security={{"bearerAuth":{}}},
     *
     *     @OA\Parameter(
     *         name="per_page",
     *         in="query",
     *         required=true,
     *
     *         @OA\Schema(type="number"), example="25", description="per page"),
     *
     *     @OA\Response(response=200, description="success", @OA\JsonContent(ref="user-response.json#/follow-action/success")),
     *     @OA\Response(response=401, description="Unauthorized", @OA\JsonContent(ref="user-response.json#/invalid"))
     * )
     */
    public function getFollower(Request $request)
    {
        $perPage = $request->input('per_page');
        $page = $request->input('page', 1);
        $follower = $this->service->getFollower($perPage, $page);

        return $this->response->withPaginator($follower, $this->followerTransformer);
    }

    /**
     * @OA\GET(
     *     path="/api/user/get-following",
     *     tags={"User - Follow"},
     *     summary="Get Following",
     *     security={{"bearerAuth":{}}},
     *
     *     @OA\Parameter(
     *         name="per_page",
     *         in="query",
     *         required=true,
     *
     *         @OA\Schema(type="number"), example="25", description="per page"),
     *
     *     @OA\Response(response=200, description="success", @OA\JsonContent(ref="user-response.json#/follow-action/success")),
     *     @OA\Response(response=401, description="Unauthorized", @OA\JsonContent(ref="user-response.json#/invalid"))
     * )
     */
    public function getFollowing(Request $request)
    {
        $perPage = $request->input('per_page');
        $page = $request->input('page', 1);
        $following = $this->service->getFollowing($perPage, $page);

        return $this->response->withPaginator($following, $this->followerTransformer);
    }

    /**
     * @OA\GET(
     *     path="/api/user/trending",
     *     tags={"User - Follow"},
     *     summary="Get User Trending",
     *
     *     @OA\Parameter(
     *         name="per_page",
     *         in="query",
     *         required=true,
     *
     *         @OA\Schema(type="number"), example="25", description="per page"),
     *
     *     @OA\Response(response=200, description="success", @OA\JsonContent(ref="auth-response.json#/user-transformer")),
     * )
     */
    public function getUserTrending(Request $request)
    {
        $userLogin = Auth::guard('api')->user();
        $perPage = $request->input('per_page');
        $page = $request->page ?? 1;
        $users = $this->service->getUserTrending($perPage, $userLogin, $page);

        return $this->response->withPaginator($users, $this->transformer);
    }

    public function searchUserBE(SearchUser $request)
    {
        $keyword = str_replace('_', '\_', $request->name);
        $perPage = $request->has('per_page') ? $request->per_page : null;

        $users = $this->service->searchUser($keyword, $perPage);

        return $this->response->withPaginator($users, $this->transformer);
    }

    public function updateUser(User $user, UpdateStatus $request)
    {
        $data = $request->all();
        $users = $this->service->updateUser($user, $data);

        return $this->response->withItem($users, $this->transformer);
    }

    public function getAdmin(GetAdmin $request)
    {
        $keyword = str_replace('_', '\_', $request->keyword);
        $perPage = $request->has('per_page') ? $request->per_page : null;
        $users = $this->service->getAdmin($keyword, $perPage);

        return $this->response->withPaginator($users, $this->adminTransformer);
    }

    public function getAdminById($id)
    {
        $users = $this->service->getAdminById($id);

        return $this->response->withItem($users, $this->adminTransformer);
    }

    public function updateAdmin(Admin $admin, UpdateAdmin $request)
    {
        $data = $request->all();
        $this->service->updateAdmin($admin, $data);

        return response()->json([
            'data' => [
                'message' => 'update successfully',
            ],
        ], Response::HTTP_OK);
    }

    public function getTopUser(Request $request)
    {
        $perPage = $request->per_page;
        $users = $this->service->getTopUser($perPage);

        return $this->response->withPaginator($users, $this->transformer);
    }
}
