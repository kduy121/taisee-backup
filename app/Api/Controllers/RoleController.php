<?php

namespace App\Api\Controllers;

use App\Http\Requests\Role\CreateRole;
use App\Http\Requests\Role\UpdateRole;
use App\Repositories\RoleRepository;
use App\Services\RoleService;
use App\Transformers\RoleTransformer;
use EllipseSynergie\ApiResponse\Contracts\Response as ApiResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class RoleController extends ApiController
{
    public function __construct(ApiResponse $response,
        Request $request,
        private RoleService $service,
        private RoleTransformer $roleTransformer)
    {
        parent::__construct($response, $request);
    }

    public function getRepo()
    {
        return new RoleRepository();
    }

    public function getTransformer()
    {
        return new RoleTransformer();
    }

    public function index()
    {
        $roles = $this->service->getAllRoles();

        return $this->response->withPaginator($roles, $this->roleTransformer);
    }

    public function show($id)
    {
        $role = $this->service->getRoleById($id);

        return $this->response->withItem($role, $this->roleTransformer);
    }

    public function store(CreateRole $request)
    {
        $data = $request->all();
        $role = $this->service->createRole($data);

        return $this->response->withItem($role, $this->roleTransformer);
    }

    public function update(UpdateRole $request)
    {
        $data = $request->all();
        $role = $this->service->updateRole($request->id, $data);

        return $this->response->withItem($role, $this->roleTransformer);
    }

    public function destroy($id)
    {
        $this->service->deleteRole($id);

        return response()->json([
            'data' => [
                'message' => 'delete successful',
            ],
        ], Response::HTTP_OK);
    }
}
