<?php

namespace App\Api\Controllers;

use App\Api\Clients\Socialite\VerifyFacebook;
use App\Api\Clients\Socialite\VerifyGoogle;
use App\Api\Clients\Socialite\VerifyLine;
use App\Api\Clients\Socialite\VerifyTwitter;
use App\Api\Clients\Socialite\VerifyYahoo;
use App\Enums\GenderType;
use App\Enums\SocialiteType;
use App\Enums\StateAccount;
use App\Models\User;
use App\Repositories\InterestRepository;
use App\Services\AuthService;
use App\Services\SocialiteService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Facades\JWTAuth;

class SocialiteController extends ApiController
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct(private SocialiteService $socialiteService, private AuthService $authService)
    {
        $this->expireTokenTime = config('constants.token.expire_token_time');
    }

    protected $rules = [
        'accessToken' => 'required_without_all:authorization',
        'authorization' => 'required_without_all:accessToken',
        'device_id' => 'sometimes|string',
    ];

    protected $rulesGoogle = [
        'idToken' => 'required|string',
    ];

    /**
     * @OA\POST(
     *     path="/api/socialite",
     *     tags={"Other"},
     *     summary="Verify Socialite",
     *
     *     @OA\RequestBody(@OA\JsonContent(ref="other-request.json#/socialite")),
     *
     *     @OA\Response(response="200", description="success", @OA\JsonContent(ref="user-response.json#/user/success")),
     *     @OA\Response(response="401", description="Unauthorized", @OA\JsonContent(ref="user-response.json#/invalid")),
     * )
     */
    public function verifySocialite(Request $request)
    {
        // TODO: validate
        $type = $request->type;

        if ($type == 'facebook') {
            return $this->verifyFacebook($request);
        } elseif ($type == 'google') {
            return $this->verifyGoogle($request);
        } elseif ($type == 'yahoo') {
            return $this->verifyYahoo($request);
        } elseif ($type == 'line') {
            return $this->verifyLine($request);
        } elseif ($type == 'twitter') {
            return $this->verifyTwitter($request);
        }

        // return $this->respondFail();
    }

    public function insertInterest(User $user, array $interest)
    {
        $interestRepo = new InterestRepository;
        $interestRepo->attachData($user, $interest);
    }

    public function verifyFacebook(Request $request)
    {
        $validator = Validator::make($request->all(), $this->rules);
        $deviceId = $request->device_id;

        if ($validator->fails()) {
            return $this->respondFail();
        }

        $client = new VerifyFacebook($request->accessToken);
        $response = $client->sendContext();

        if (! $response) {
            return $this->respondFail();
        }

        if ($request->has('user.date_of_birth')) {
            $dateOfBirth = Carbon::createFromFormat('Y-m-d H:i:s', $request->user['date_of_birth'], 'UTC');
        }

        if (! isset($response['email'])) {
            $hasUser = User::where('socialite_id', $response['id'])->first();
        } else {
            $hasUser = ! empty($response['email']) ? User::where('email', $response['email'])->first() : null;
        }

        $userData = [
            'name' => $response['name'],
            'email' => $response['email'] ?? null,
            'socialite_type' => SocialiteType::FACEBOOK->name,
            'socialite_id' => $response['id'],
            'gender' => $request->user['gender'] ?? GenderType::other->name,
            'device_info' => $request->device_info,
            // 'date_of_birth' => $dateOfBirth ?? null,
        ];

        if ($hasUser) {
            $user = $hasUser;
            $user['state_account'] = StateAccount::member->name;
        } else {
            // Check user name
            $userName = ! is_null($userData['email']) ? $this->socialiteService->generateUserName($userData['email']) : $this->socialiteService->generateUserName();
            $userData['user_name'] = $userName;

            $user = User::firstOrCreate($userData);
            $user['state_account'] = StateAccount::new->name;
        }

        $token = JWTAuth::fromUser($user);
        $message = 'Login successfully';

        // Insert Interest
        if ($request->has('user.interest')) {
            $this->insertInterest($user, $request->user['interest']);
        }

        $refreshToken = '';
        if ($deviceId) {
            $refreshToken = $this->authService->createRefreshToken($user, $deviceId);
        }

        return $this->respondWithToken($token, $user, $message, $refreshToken);
    }

    public function verifyGoogle(Request $request)
    {
        $accessToken = $request->input('accessToken');
        $idToken = $request->input('idToken');
        $token = '';
        $deviceId = $request->device_id;

        $accessToken = $request->input('accessToken');
        $idToken = $request->input('idToken');
        $validator = $accessToken ? Validator::make($request->all(), $this->rules) : Validator::make($request->all(), $this->rulesGoogle);
        $token = $accessToken ? $accessToken : $idToken;
        $type = $accessToken ? 'ios' : 'android';

        if ($validator->fails()) {
            return $this->respondFail();
        }

        $client = new VerifyGoogle($token, $type);
        $response = $client->sendContext();

        if (! $response) {
            return $this->respondFail();
        }

        $hasEmail = User::where('email', $response['email'])->first();
        $userData = [
            'email' => $response['email'] ?? null,
            'name' => $response['name'] ?? $response['email'],
            'socialite_type' => SocialiteType::GOOGLE->name,
            'socialite_id' => $response['sub'],
            'device_info' => $request->device_info,
        ];

        if ($hasEmail) {
            $user = $hasEmail;
            $user['state_account'] = StateAccount::member->name;
        } else {
            // Check user name
            $userName = ! is_null($userData['email']) ? $this->socialiteService->generateUserName($userData['email']) : $this->socialiteService->generateUserName();
            $userData['user_name'] = $userName;

            $user = User::firstOrCreate($userData);
            $user['state_account'] = StateAccount::new->name;
        }

        $token = JWTAuth::fromUser($user);
        $message = 'Login successfully';

        // Insert Interest
        if ($request->has('user.interest')) {
            $this->insertInterest($user, $request->user['interest']);
        }

        $refreshToken = '';
        if ($deviceId) {
            $refreshToken = $this->authService->createRefreshToken($user, $deviceId);
        }

        return $this->respondWithToken($token, $user, $message, $refreshToken);
    }

    public function verifyYahoo(Request $request)
    {
        $validator = Validator::make($request->all(), $this->rules);
        $deviceId = $request->device_id;

        if ($validator->fails()) {
            return $this->respondFail();
        }

        $client = new VerifyYahoo($request->accessToken);
        $response = $client->sendContext();

        if ($response) {
            // Check email has exists
            $hasEmail = User::where('email', $response['email'])->first();

            $userData = [
                'name' => $response['name'],
                'email' => $response['email'],
                'socialite_type' => SocialiteType::YAHOO->name,
                'socialite_id' => $response['sub'],
                'device_info' => $request->device_info,
            ];

            if ($hasEmail) {
                $user = $hasEmail;
                $user['state_account'] = StateAccount::member->name;
            } else {
                // Check user name
                $userName = ! is_null($userData['email']) ? $this->socialiteService->generateUserName($userData['email']) : $this->socialiteService->generateUserName();
                $userData['user_name'] = $userName;

                $user = User::firstOrCreate($userData);
                $user['state_account'] = StateAccount::new->name;
            }

            $token = JWTAuth::fromUser($user);
            $message = 'Login successfully';

            // Insert Interest
            if ($request->has('user.interest')) {
                $this->insertInterest($user, $request->user['interest']);
            }

            $refreshToken = '';
            if ($deviceId) {
                $refreshToken = $this->authService->createRefreshToken($user, $deviceId);
            }

            return $this->respondWithToken($token, $user, $message, $refreshToken);
        }

        return $this->respondFail();
    }

    public function verifyTwitter(Request $request)
    {
        $validator = Validator::make($request->all(), $this->rules);
        $deviceId = $request->device_id;

        if ($validator->fails()) {
            return $this->respondFail();
        }

        $accessToken = $request->accessToken;
        $platform = null;

        if ($request->has('authorization')) {
            $accessToken = $request->authorization;
            $platform = $request->device_info['PLATFORM'];
        }

        $client = new verifyTwitter($accessToken, $platform);
        $response = $client->sendContext();

        if ($response) {
            // Check user has exists
            $hasUser = User::where('socialite_id', $response['data']['id'])->first();

            $userData = [
                'name' => $response['data']['name'] ?? '',
                'socialite_type' => SocialiteType::TWITTER->name,
                'socialite_id' => $response['data']['id'],
                'device_info' => $request->device_info,
            ];

            if ($hasUser) {
                $user = $hasUser;
                $user['state_account'] = StateAccount::member->name;
            } else {
                // Check user name
                $userName = $this->socialiteService->generateUserName();
                $userData['user_name'] = $userName;

                $user = User::firstOrCreate($userData);
                $user['state_account'] = StateAccount::new->name;
            }

            $token = JWTAuth::fromUser($user);
            $message = 'Login successfully';

            // Insert Interest
            if ($request->has('user.interest')) {
                $this->insertInterest($user, $request->user['interest']);
            }

            $refreshToken = '';
            if ($deviceId) {
                $refreshToken = $this->authService->createRefreshToken($user, $deviceId);
            }

            return $this->respondWithToken($token, $user, $message, $refreshToken);
        }

        return $this->respondFail();
    }

    public function verifyLine(Request $request)
    {
        $validator = Validator::make($request->all(), $this->rules);
        $deviceId = $request->device_id;

        if ($validator->fails()) {
            return $this->respondFail();
        }

        $client = new VerifyLine($request->accessToken);
        $response = $client->sendContext();

        if ($response) {

            // Check user has exists
            $hasUser = User::where('socialite_id', $response['userId'])->first();

            $userData = [
                'name' => $response['displayName'],
                'email' => $response['email'] ?? null,
                'socialite_type' => SocialiteType::LINE->name,
                'socialite_id' => $response['userId'],
                'device_info' => $request->device_info,
            ];

            if ($hasUser) {
                $user = $hasUser;
                $user['state_account'] = StateAccount::member->name;
            } else {
                // Check user name
                $userName = ! is_null($userData['email']) ? $this->socialiteService->generateUserName($userData['email']) : $this->socialiteService->generateUserName();
                $userData['user_name'] = $userName;

                $user = User::firstOrCreate($userData);
                $user['state_account'] = StateAccount::new->name;
            }

            $token = JWTAuth::fromUser($user);
            $message = 'Login successfully';

            // Insert Interest
            if ($request->has('user.interest')) {
                $this->insertInterest($user, $request->user['interest']);
            }

            $refreshToken = '';
            if ($deviceId) {
                $refreshToken = $this->authService->createRefreshToken($user, $deviceId);
            }

            return $this->respondWithToken($token, $user, $message, $refreshToken);
        }

        return $this->respondFail();
    }
}
