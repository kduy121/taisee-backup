<?php

namespace App\Api\Controllers;

use App\Http\Requests\Hashtag\SearchTagByName;
use App\Http\Requests\Hashtag\CreateHashtag;
use App\Http\Requests\Hashtag\UpdateHashtag;
use App\Repositories\HashtagRepository;
use App\Services\HashtagService;
use App\Transformers\HashtagTransformer;
use EllipseSynergie\ApiResponse\Contracts\Response as ApiResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Hashtag;

class HashtagController extends ApiController
{
    public function __construct(ApiResponse $response,
        Request $request,
        private HashtagService $service,
        private HashtagTransformer $hashtagTransformer)
    {
        parent::__construct($response, $request);
    }

    public function getRepo()
    {
        return new HashtagRepository();
    }

    public function getTransformer()
    {
        return new HashtagTransformer();
    }

    /**
     * @OA\GET(
     *     path="/api/hashtag",
     *     tags={"Hashtag"},
     *     summary="Search tag by name",
     *
     *     @OA\Parameter(
     *         name="hashtag",
     *         in="query",
     *         required=true,
     *
     *         @OA\Schema(type="string"), example="#con_bo", description="hashtag"),
     *
     *     @OA\Parameter(
     *         name="per_page",
     *         in="query",
     *         required=false,
     *
     *         @OA\Schema(type="number"), example="1", description="page"),
     *
     *     @OA\Response(response="200", description="success", @OA\JsonContent(ref="hashtag-response.json#/response"))
     * )
     */
    public function getHashtag(SearchTagByName $request)
    {
        $perPage = $request->per_page;
        $page = $request->input('page', 1);
        $hashtag = $request->hashtag;

        $hashtags = $this->service->getHashtag($hashtag, $perPage, $page);

        return $this->response->withPaginator($hashtags, $this->hashtagTransformer);
    }

    public function createHashtag(CreateHashtag $request)
    {
        $hashtag = $request->hashtag;

        $this->service->createHashtag($hashtag);

        return response()->json([
            'data' => [
                'message' => 'create successfully',
            ],
        ], Response::HTTP_OK);
    }

    public function getById(Request $request)
    {
        $hashtagId = $request->id;

        $hashtag = $this->service->getById($hashtagId);

        return $this->response->withItem($hashtag, $this->hashtagTransformer);
    }

    public function updateHashtag(Hashtag $hashtag, UpdateHashtag $request)
    {
        $data = $request->all();
        $this->service->updateHashtag($hashtag, $data);

        return response()->json([
            'data' => [
                'message' => 'update successfully',
            ],
        ], Response::HTTP_OK);
    }

    public function delete(Hashtag $hashtag)
    {
        $hashtag->delete();

        return response()->json([
            'data' => [
                'message' => 'Delete hashtag success',
            ],
        ], Response::HTTP_OK);
    }
}
