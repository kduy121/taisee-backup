<?php

namespace App\Api\Controllers;

use App\Http\Requests\Permission\CreatePermission;
use App\Http\Requests\Permission\UpdatePermission;
use App\Repositories\PermissionRepository;
use App\Services\PermissionService;
use App\Transformers\PermissionTransformer;
use EllipseSynergie\ApiResponse\Contracts\Response as ApiResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PermissionController extends ApiController
{
    public function __construct(ApiResponse $response,
        Request $request,
        private PermissionService $service,
        private PermissionTransformer $permissionTransformer)
    {
        parent::__construct($response, $request);
    }

    public function getRepo()
    {
        return new PermissionRepository();
    }

    public function getTransformer()
    {
        return new PermissionTransformer();
    }

    public function index()
    {
        $roles = $this->service->getAllPermissions();

        return $this->response->withPaginator($roles, $this->permissionTransformer);
    }

    public function show($id)
    {
        $role = $this->service->getPermissionById($id);

        return $this->response->withItem($role, $this->permissionTransformer);
    }

    public function store(CreatePermission $request)
    {
        $data = $request->all();
        $role = $this->service->createPermission($data);

        return $this->response->withItem($role, $this->permissionTransformer);
    }

    public function update(UpdatePermission $request)
    {
        $data = $request->all();
        $role = $this->service->updatePermission($request->id, $data);

        return $this->response->withItem($role, $this->permissionTransformer);
    }

    public function destroy($id)
    {
        $this->service->deletePermission($id);

        return response()->json([
            'data' => [
                'message' => 'delete successful',
            ],
        ], Response::HTTP_OK);
    }
}
