<?php

namespace App\Api\Controllers;

use App\Http\Requests\Livestream\CreateLivestreamRequest;
use App\Http\Requests\Livestream\DeleteLivestreamRequest;
use App\Http\Requests\Livestream\GetLivestreamByIdRequest;
use App\Http\Requests\Livestream\UpdateLivestreamdRequest;
use App\Services\LivestreamService;
use App\Transformers\LivestreamTransformer;
use EllipseSynergie\ApiResponse\Contracts\Response as ApiResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class LivestreamController extends ApiController
{
    public function __construct(
        ApiResponse $response,
        Request $request,
        private LivestreamService $livestreamService,
        private LivestreamTransformer $livestreamTransformer
    ) {
        parent::__construct($response, $request);
    }

    public function getTransformer()
    {
        return new LivestreamTransformer();
    }

    /**
     * @OA\GET(
     *     path="/api/livestream",
     *     tags={"Livestream"},
     *     summary="Get all livestream",
     *     security={{"bearerAuth":{}}},
     *
     *     @OA\Parameter(
     *         name="page",
     *         in="query",
     *         required=false,
     *
     *         @OA\Schema(type="nunber"), example="1", description="page"),
     *
     *     @OA\Parameter(
     *         name="per_page",
     *         in="query",
     *         required=false,
     *
     *         @OA\Schema(type="nunber"), example="25", description="per_page"),
     *
     *     @OA\Response(response="200", description="success", @OA\JsonContent(ref="video-response.json#/video")),
     * )
     */
    public function index(Request $request)
    {
        $perPage = $request->per_page;
        $page = $request->page ?? 1;

        $livestreams = $this->livestreamService->getAllLivestreams($perPage, $page);

        return $this->response->withPaginator($livestreams, $this->livestreamTransformer);

    }

    /**
     * @OA\POST(
     *     path="/api/livestream",
     *     tags={"Livestream"},
     *     summary="Create Livestream",
     *     security={{"bearerAuth":{}}},
     *
     *     @OA\RequestBody(@OA\JsonContent(ref="livestream-request.json#/create")),
     *
     *     @OA\Response(response="200", description="success", @OA\JsonContent(ref="video-response.json#/video")),
     *     @OA\Response(response="422", description="Unprocessable Content")
     * )
     */
    public function create(CreateLivestreamRequest $request)
    {
        $user = Auth::user();
        $data = $request->all();
        $data['user_id'] = $user->id;
        $data['avatar_url'] = $user->avatar_url;

        $livestream = $this->livestreamService->createLivestream($data, $user);

        return $this->response->withItem($livestream, $this->livestreamTransformer);
    }

    /**
     * @OA\GET(
     *     path="/api/livestream/get-by-id",
     *     tags={"Livestream"},
     *     summary="Get livestream by id",
     *     security={{"bearerAuth":{}}},
     *
     *     @OA\Parameter(
     *         name="livestream_id",
     *         in="query",
     *         required=true,
     *
     *         @OA\Schema(type="string"), example="09812017-fc73-4f62-8c68-dea7a243ae59", description="livestream_id"),
     *
     *     @OA\Response(response="200", description="success", @OA\JsonContent(ref="video-response.json#/video")),
     * )
     */
    public function getById(GetLivestreamByIdRequest $request)
    {
        $livestreamId = $request->livestream_id;
        $userId = Auth::guard('api')->user()->id ?? null;

        $livestream = $this->livestreamService->getLivestreamById($livestreamId, $userId);

        return $this->response->withItem($livestream, $this->livestreamTransformer);
    }

    /**
     * @OA\PUT(
     *     path="/api/livestream",
     *     tags={"Livestream"},
     *     summary="Update Livestream",
     *     security={{"bearerAuth":{}}},
     *
     *     @OA\RequestBody(@OA\JsonContent(ref="livestream-request.json#/update")),
     *
     *     @OA\Response(response="200", description="success", @OA\JsonContent(ref="livestream-response.json#/response")),
     *     @OA\Response(response="422", description="Unprocessable Content"),
     *     @OA\Response(response="500", description="Err", @OA\JsonContent(ref="video-response.json#/video"))
     * )
     */
    public function updateLivestream(UpdateLivestreamdRequest $request)
    {
        $data = $request->all();
        $user = Auth::user();

        $livestream = $this->livestreamService->updateLivestream($data, $user);

        return $this->response->withItem($livestream, $this->livestreamTransformer);
    }

    /**
     * @OA\DELETE(
     *     path="/api/livestream",
     *     tags={"Livestream"},
     *     summary="Delete livestream",
     *     security={{"bearerAuth":{}}},
     *
     *     @OA\RequestBody(@OA\JsonContent(ref="livestream-request.json#/livestream-id")),
     *
     *     @OA\Response(response="200", description="success", @OA\JsonContent(ref="livestream-response.json#/delete")),
     *     @OA\Response(response="500", description="Err", @OA\JsonContent(ref="livestream-response.json#/err"))
     * )
     */
    public function delete(DeleteLivestreamRequest $request)
    {
        $livestreamId = $request->livestream_id;
        $data = $request->all();
        $data['user_id'] = Auth::user()->id;

        $this->livestreamService->deleteLivestream($data);

        return response()->json([
            'message' => "Livestream: $livestreamId delete successful",
        ], Response::HTTP_OK);
    }
}
