<?php

namespace App\Api\Controllers;

use App\Enums\VideoStatus;
use App\Http\Requests\Video\SearchByHashtag;
use App\Repositories\VideoRepository;
use App\Services\VideoService;
use App\Transformers\VideoDraftTransformer;
use App\Transformers\VideoTransformer;
use EllipseSynergie\ApiResponse\Contracts\Response as ApiResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Models\Video;
use App\Http\Requests\Video\UpdateVideo;

class VideosController extends ApiController
{
    public function __construct(ApiResponse $response,
        Request $request,
        private VideoService $videoService,
        private VideoDraftTransformer $videoDraftTransformer)
    {
        parent::__construct($response, $request);
    }

    protected $rulesGetVideoUser = [
        'user_id' => 'required|string',
        'per_page' => 'sometimes|numeric',
        'page' => 'sometimes|numeric',
    ];

    protected $rulesGet = [
        'per_page' => 'sometimes|numeric',
        'page' => 'sometimes|numeric',
    ];

    protected $rulesChanges = [
        'video_id' => 'required|string',
        'status' => 'required|numeric',
    ];

    protected $rulesPublish = [
        'video_id' => 'required|string',
        'status' => 'required|numeric',
    ];

    protected $rulesGetDetail = [
        'video_id' => 'required|string',
    ];

    protected $rulesUpdate = [
        'video_id' => 'required|string',
        'video_privacy' => 'required|numeric',
    ];

    protected $rulesDelete = [
        'video_ids' => 'required|array',
    ];

    public function getRepo()
    {
        return new VideoRepository();
    }

    public function getTransformer()
    {
        return new VideoTransformer();
    }

    /**
     * @OA\GET(
     *     path="/api/get-videos",
     *     tags={"Video"},
     *     summary="Get videos",
     *
     *     @OA\Parameter(
     *         name="page",
     *         in="query",
     *         required=false,
     *
     *         @OA\Schema(type="nunber"), example="1", description="page"),
     *
     *     @OA\Parameter(
     *         name="per_page",
     *         in="query",
     *         required=false,
     *
     *         @OA\Schema(type="nunber"), example="25", description="per_page"),
     *
     *     @OA\Response(response="200", description="success", @OA\JsonContent(ref="video-response.json#/video")),
     * )
     */
    public function getVideos(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'per_page' => 'sometimes|numeric',
        ]);

        if ($validator->fails()) {
            return $this->validateFail($validator);
        }

        $perPage = $request->has('per_page') ? $request->per_page : 10;
        $page = $request->page ?? 1;

        if ($request->has('per_page')) {
            $perPage = $request->per_page;
            $perPage = $perPage > 25 ? 10 : $perPage;
        }

        $videos = $this->videoService->getVideos($this->getUserId(), $perPage, ['likes', 'comments'], $page);

        return $this->response->withPaginator($videos, $this->transformer);
    }

    protected function getUserId()
    {
        $user = Auth::guard('api')->user();

        return ! empty($user) ? $user->id : null;
    }

    /**
     * @OA\GET(
     *     path="/api/video",
     *     tags={"Video"},
     *     summary="Get videos detail",
     *
     *     @OA\Parameter(
     *         name="video_id",
     *         in="query",
     *         required=false,
     *
     *         @OA\Schema(type="string"), example="81d891d7-556a-4f7d-9adf-abb7b003ab00", description="video id"),
     *
     *     @OA\Response(response="200", description="success", @OA\JsonContent(ref="video-response.json#/video")),
     *     @OA\Response(response="400", description="Video not found"),
     * )
     */
    public function getDetail(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'video_id' => 'required|string',
        ]);

        if ($validator->fails()) {
            return $this->validateFail($validator);
        }

        $videos = $this->videoService->getDetail($this->getUserId(), $request->video_id);

        return $this->response->withItem($videos, $this->transformer);
    }

    /**
     * @OA\GET(
     *     path="/api/video/user",
     *     tags={"Video"},
     *     summary="Get videos by user",
     *
     *     @OA\Parameter(
     *         name="user_id",
     *         in="query",
     *         required=false,
     *
     *         @OA\Schema(type="string"), example="de8c459d-5bc3-448b-b293-918e20036e93", description="user_id"),
     *
     *     @OA\Response(response="200", description="success", @OA\JsonContent(ref="video-response.json#/video")),
     *     @OA\Response(response=404, description="User not found", @OA\JsonContent(ref="video-response.json#/not-found")),
     * )
     */
    public function getVideosByUserId(Request $request)
    {
        $validator = Validator::make($request->all(), $this->rulesGetVideoUser);

        if ($validator->fails()) {
            return $this->validateFail($validator);
        }

        $perPage = $request->has('per_page') ? $request->per_page : null;
        $page = $request->page ?? 1;
        $videos = $this->videoService->getVideoByUser($this->getUserId(), $request->user_id, $perPage, $page);

        return $this->response->withPaginator($videos, $this->transformer);
    }

    /**
     * @OA\GET(
     *     path="/api/video/following",
     *     tags={"Video"},
     *     summary="Get Video Following",
     *     security={{"bearerAuth":{}}},
     *
     *     @OA\Parameter(
     *         name="per_page",
     *         in="query",
     *         required=false,
     *
     *         @OA\Schema(type="number"), example="25", description="perpage"),
     *
     *     @OA\Response(response="200", description="success", @OA\JsonContent(ref="video-response.json#/video")),
     *     @OA\Response(response=401, description="Unauthorized", @OA\JsonContent(ref="user-response.json#/invalid")),
     * )
     */
    public function getVideoFollowing(Request $request)
    {
        $perPage = $request->input('per_page');
        $page = $request->page ?? 1;
        $user = Auth::user();

        $videos = $this->videoService->getVideoFollowing($perPage, $user, $page);

        return $this->response->withPaginator($videos, $this->transformer);
    }

    /**
     * @OA\GET(
     *     path="/api/video/friend",
     *     tags={"Video"},
     *     summary="Get video friend",
     *     security={{"bearerAuth":{}}},
     *
     *     @OA\Parameter(
     *         name="per_page",
     *         in="query",
     *         required=false,
     *
     *         @OA\Schema(type="number"), example="25", description="perpage"),
     *
     *     @OA\Parameter(
     *         name="page",
     *         in="query",
     *         required=false,
     *
     *         @OA\Schema(type="number"), example="1", description="page"),
     *
     *     @OA\Response(response="200", description="success", @OA\JsonContent(ref="video-response.json#/video")),
     *     @OA\Response(response=401, description="Unauthorized", @OA\JsonContent(ref="user-response.json#/invalid")),
     * )
     */
    public function getFriendVideos(Request $request)
    {
        $perPage = $request->input('per_page');
        $page = $request->page ?? 1;
        $user = Auth::user();

        $videos = $this->videoService->getFriendVideos($perPage, $user, $page);

        return $this->response->withPaginator($videos, $this->transformer);
    }

    /**
     * @OA\POST(
     *     path="/api/video/change",
     *     tags={"Video"},
     *     summary="Change video status",
     *     security={{"bearerAuth":{}}},
     *
     *     @OA\RequestBody(@OA\JsonContent(ref="video-request.json#/change-status")),
     *
     *     @OA\Response(response="200", description="success", @OA\JsonContent(ref="video-response.json#/video")),
     *     @OA\Response(response="401", description="Unauthorized", @OA\JsonContent(ref="user-response.json#/invalid")),
     *     @OA\Response(response="404", description="Video not found.", @OA\JsonContent(ref="video-response.json#/not-found")),
     *     @OA\Response(response="403", description="You are not the author of this video."),
     * )
     */
    public function changeStatus(Request $request)
    {
        $status = VideoStatus::Public->value.','.VideoStatus::Private->value;
        $this->rulesChanges['status'] = $this->rulesChanges['status'].'|in:'.$status;
        $validator = Validator::make($request->all(), $this->rulesChanges);

        if ($validator->fails()) {
            return $this->validateFail($validator);
        }

        $user = Auth::user();
        $videoId = $request->video_id;
        $status = $request->status;

        $video = $this->videoService->changeStatus($user, $videoId, $status);

        return $this->response->withItem($video, $this->transformer);
    }

    /**
     * @OA\GET(
     *     path="/api/video/drafts",
     *     tags={"Video - Draft"},
     *     summary="Get video drafts",
     *     security={{"bearerAuth":{}}},
     *
     *     @OA\Parameter(
     *         name="per_page",
     *         in="query",
     *         required=false,
     *
     *         @OA\Schema(type="number"), example="25", description="perpage"),
     *
     *     @OA\Parameter(
     *         name="page",
     *         in="query",
     *         required=false,
     *
     *         @OA\Schema(type="number"), example="1", description="page"),
     *
     *     @OA\Response(response="200", description="success", @OA\JsonContent(ref="video-response.json#/video")),
     *     @OA\Response(response=401, description="Unauthorized", @OA\JsonContent(ref="user-response.json#/invalid")),
     * )
     */
    public function getVideoDrafts(Request $request)
    {
        $validator = Validator::make($request->all(), $this->rulesGet);

        if ($validator->fails()) {
            return $this->validateFail($validator);
        }

        $perPage = $request->per_page;
        $page = $request->page ?? 1;
        $user = Auth::user();

        $videos = $this->videoService->getVideoDrafts($perPage, $user, $page);

        return $this->response->withPaginator($videos, $this->videoDraftTransformer);
    }

    /**
     * @OA\POST(
     *     path="/api/video/draft/publish",
     *     tags={"Video - Draft"},
     *     summary="Publish draft video",
     *     security={{"bearerAuth":{}}},
     *
     *     @OA\RequestBody(@OA\JsonContent(ref="video-request.json#/publish-video")),
     *
     *     @OA\Response(response="200", description="success", @OA\JsonContent(ref="video-response.json#/video")),
     *     @OA\Response(response="401", description="Unauthorized", @OA\JsonContent(ref="user-response.json#/invalid")),
     *     @OA\Response(response="404", description="Video not found.", @OA\JsonContent(ref="video-response.json#/not-found")),
     *     @OA\Response(response="500", description="Failed to publish videos: Video draft not found."),
     * )
     */
    public function publish(Request $request)
    {
        $this->rulesPublish['status'] .= sprintf('|in:%s,%s', VideoStatus::Public->value, VideoStatus::Private->value);
        $validator = Validator::make($request->all(), $this->rulesPublish);

        if ($validator->fails()) {
            return $this->validateFail($validator);
        }

        $data['video_id'] = $request->video_id;
        $data['title'] = $request->title;
        $data['description'] = $request->description;
        $videoStatus = $request->status;
        $user = Auth::user();

        $publishedVideo = $this->videoService->publishVideos($data, $videoStatus, $user);

        return $this->response->withItem($publishedVideo, $this->videoDraftTransformer);
    }

    /**
     * @OA\GET(
     *     path="/api/video/draft",
     *     tags={"Video - Draft"},
     *     summary="Get detail video draft",
     *     security={{"bearerAuth":{}}},
     *
     *     @OA\Parameter(
     *         name="video_id",
     *         in="query",
     *         required=false,
     *
     *         @OA\Schema(type="string"), example="7f71c53c-6833-4b69-941b-68e070ea21e7", description="video id"),
     *
     *     @OA\Response(response="200", description="success", @OA\JsonContent(ref="video-response.json#/video")),
     *     @OA\Response(response=401, description="Unauthorized", @OA\JsonContent(ref="user-response.json#/invalid")),
     * )
     */
    public function getDetailVideoDraft(Request $request)
    {
        $validator = Validator::make($request->all(), $this->rulesGetDetail);

        if ($validator->fails()) {
            return $this->validateFail($validator);
        }

        $videoId = $request->video_id;
        $user = Auth::user();

        $video = $this->videoService->getDetailVideoDraft($videoId, $user);

        return $this->response->withItem($video, $this->videoDraftTransformer);
    }

    /**
     * @OA\PUT(
     *     path="/api/video/draft",
     *     tags={"Video - Draft"},
     *     summary="update draft video",
     *     security={{"bearerAuth":{}}},
     *
     *     @OA\RequestBody(@OA\JsonContent(ref="video-request.json#/update-video")),
     *
     *     @OA\Response(response="200", description="success", @OA\JsonContent(ref="video-response.json#/video")),
     *     @OA\Response(response="401", description="Unauthorized", @OA\JsonContent(ref="user-response.json#/invalid")),
     *     @OA\Response(response="404", description="Video not found.", @OA\JsonContent(ref="video-response.json#/not-found")),
     * )
     */
    public function updateVideoDraft(Request $request)
    {
        $this->rulesUpdate['video_privacy'] .= sprintf('|in:%s,%s', VideoStatus::Public->value, VideoStatus::Private->value);
        $validator = Validator::make($request->all(), $this->rulesUpdate);

        if ($validator->fails()) {
            return $this->validateFail($validator);
        }

        $data = $request->all();
        $user = Auth::user();

        $videoDraft = $this->videoService->updateVideoDraft($data, $user);

        return $this->response->withItem($videoDraft, $this->videoDraftTransformer);
    }

    /**
     * @OA\DELETE(
     *     path="/api/video/draft",
     *     tags={"Video - Draft"},
     *     summary="Delete video draft",
     *     security={{"bearerAuth":{}}},
     *
     *     @OA\RequestBody(@OA\JsonContent(ref="video-request.json#/delete-video")),
     *
     *     @OA\Response(response="200", description="success", @OA\JsonContent(ref="video-response.json#/delete")),
     *     @OA\Response(response=401, description="Unauthorized", @OA\JsonContent(ref="user-response.json#/invalid")),
     *     @OA\Response(response=500, description="Failed to delete videos: Video not found."),
     * )
     */
    public function deleteVideoDraft(Request $request)
    {
        $validator = Validator::make($request->all(), $this->rulesDelete);

        if ($validator->fails()) {
            return $this->validateFail($validator);
        }

        $videoIds = $request->video_ids;
        $user = Auth::user();

        $this->videoService->deleteVideoDraft($videoIds, $user);

        $videoIds = implode(',', $videoIds);

        return response()->json([
            'data' => [
                'message' => "Video Draft: $videoIds delete successful",
            ],
        ], Response::HTTP_OK);
    }

    /**
     * @OA\GET(
     *     path="/api/video/private",
     *     tags={"Video"},
     *     summary="Get video private",
     *     security={{"bearerAuth":{}}},
     *
     *     @OA\Parameter(
     *         name="per_page",
     *         in="query",
     *         required=false,
     *
     *         @OA\Schema(type="number"), example="25", description="perpage"),
     *
     *     @OA\Parameter(
     *         name="page",
     *         in="query",
     *         required=false,
     *
     *         @OA\Schema(type="number"), example="1", description="page"),
     *
     *     @OA\Response(response="200", description="success", @OA\JsonContent(ref="video-response.json#/video")),
     *     @OA\Response(response=401, description="Unauthorized", @OA\JsonContent(ref="user-response.json#/invalid")),
     * )
     */
    public function getVideoPrivate(Request $request)
    {
        $validator = Validator::make($request->all(), $this->rulesGet);

        if ($validator->fails()) {
            return $this->validateFail($validator);
        }

        $perPage = $request->per_page;
        $page = $request->page ?? 1;
        $userId = Auth::user()->id;

        $videos = $this->videoService->getVideoPrivate($userId, $perPage, $page);

        return $this->response->withPaginator($videos, $this->transformer);
    }

    /**
     * @OA\GET(
     *     path="/api/video/search-by-hashtag",
     *     tags={"Video"},
     *     summary="Search by hashtag",
     *
     *     @OA\Parameter(
     *         name="hashtag",
     *         in="query",
     *         required=true,
     *
     *         @OA\Schema(type="string"), example="#con bò", description="hashtag"),
     *
     *     @OA\Parameter(
     *         name="per_page",
     *         in="query",
     *         required=false,
     *
     *         @OA\Schema(type="number"), example="1", description="page"),
     *
     *     @OA\Response(response="200", description="success", @OA\JsonContent(ref="video-response.json#/video")),
     *     @OA\Response(response=401, description="Unauthorized", @OA\JsonContent(ref="user-response.json#/invalid")),
     * )
     */
    public function searchByHashtag(SearchByHashtag $request)
    {
        $perPage = $request->per_page;
        $hashtag = $request->hashtag;

        $videos = $this->videoService->searchByHashtag($hashtag, $perPage);

        return $this->response->withPaginator($videos, $this->transformer);
    }

    public function updateStatusVideo(Video $video, UpdateVideo $request)
    {
        $data = $request->all();
        $video = $this->videoService->updateStatus($video, $data);

        return $this->response->withItem($video, $this->transformer);
    }

    public function getVideosBE(Request $request)
    {
        $perPage = $request->per_page;
        $videos = $this->videoService->getVideosBE($perPage);

        return $this->response->withPaginator($videos, $this->transformer);
    }

    public function deleteVideo(Video $video)
    {
        $this->videoService->deleteVideo($video);

        return response()->json([
            'data' => [
                'message' => 'delete successfully',
            ],
        ], Response::HTTP_OK);
    }

    public function searchVideo(Request $request)
    {
        $keyword = $request->keyword;

        $videos = $this->videoService->search($keyword);

        return $this->response->withPaginator($videos, $this->transformer);
    }

    public function getTopVideos(Request $request)
    {
        $perPage = $request->per_page;
        $videos = $this->videoService->getTopVideos($perPage);

        return $this->response->withPaginator($videos, $this->transformer);
    }
}
