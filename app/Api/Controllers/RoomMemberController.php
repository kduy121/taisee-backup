<?php

namespace App\Api\Controllers;

use App\Services\RoomMemberService;
use App\Transformers\RoomMemberTransformer;
use EllipseSynergie\ApiResponse\Contracts\Response as ApiResponse;
use Illuminate\Http\Request;

class RoomMemberController extends ApiController
{
    public function __construct(ApiResponse $response, Request $request, private RoomMemberService $service)
    {
        parent::__construct($response, $request);
    }

    protected $rulesGet = [
        'room_id' => 'required|string',
        'per_page' => 'nullable|integer|min:1',
        'page' => 'nullable|integer|min:1',
    ];

    public function getRepo()
    {
    }

    public function getTransformer()
    {
        return new RoomMemberTransformer();
    }

    /**
     * @OA\GET(
     *     path="/api/room/member",
     *     tags={"Room"},
     *     summary="Get room member",
     *     security={{"bearerAuth":{}}},
     *
     *     @OA\Parameter(
     *         name="room_id",
     *         in="query",
     *         required=true,
     *
     *         @OA\Schema(type="string"), example="abc_123", description="room id"),
     *
     *     @OA\Parameter(
     *         name="per_page",
     *         in="query",
     *         required=false,
     *
     *         @OA\Schema(type="number"), example="25", description="per page"),
     *
     *     @OA\Response(response=200, description="success", @OA\JsonContent(ref="room-response.json#/room/success")),
     *     @OA\Response(response=401, description="Unauthorized", @OA\JsonContent(ref="user-response.json#/invalid")),
     *     @OA\Response(response=404, description="Room not found", @OA\JsonContent(ref="room-response.json#/room/fail")),
     * )
     */
    public function getMember(Request $request)
    {
        $perPage = $request->input('per_page');
        $page = $request->page ?? 1;

        $validator = $this->validate($request->all(), $this->rulesGet);

        if ($validator->fails()) {
            return $this->validateFail($validator);
        }

        $members = $this->service->getMember($request->room_id, $perPage, $page);

        return $this->response->withPaginator($members, $this->transformer);
    }
}
