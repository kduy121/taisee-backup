<?php

namespace App\Api\Controllers;

use App\Models\Area;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class AreaController extends ApiController
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth.token', ['except' => ['index']]);
    }

    /**
     * @OA\GET(
     *     path="/api/area",
     *     tags={"Other"},
     *     summary="Get common",
     *
     *     @OA\Parameter(
     *         name="lang",
     *         in="query",
     *         required=false,
     *
     *         @OA\Schema(type="string"), example="en", description="language"),
     *
     *     @OA\Response(response="200", description="success", @OA\JsonContent(ref="other-response.json#/common")),
     * )
     */
    public function index(Request $request)
    {
        $lang = $request->lang ?? 'en';
        $areas = Area::all();

        $transformedAreas = [];

        foreach ($areas as $area) {
            $name = $area->address['province'][$lang];
            $transformedAreas[$name] = $name;
        }

        return response()->json([
            'data' => [
                'lang' => $lang,
                'province' => $transformedAreas,
            ],
        ], Response::HTTP_OK);
    }
}
