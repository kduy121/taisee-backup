<?php

namespace App\Api\Controllers;

use App\Services\MessageService;
use App\Transformers\MessageTransformer;
use App\Transformers\RoomTransformer;
use App\Transformers\SearchMessageTransformer;
use App\Transformers\TopUserTransformer;
use Carbon\Carbon;
use EllipseSynergie\ApiResponse\Contracts\Response as ApiResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class MessageController extends ApiController
{
    public function __construct(
        ApiResponse $response,
        Request $request,
        private MessageService $service,
        private SearchMessageTransformer $searchMessageTransformer,
        private TopUserTransformer $topUserTransformer,
        private RoomTransformer $roomTransformer,

    ) {
        parent::__construct($response, $request);
    }

    protected $rulesSend = [
        'room_id' => 'required|string',
        'timestamp' => 'nullable|integer',
        'message' => 'required|string',
    ];

    protected $rulesGet = [
        'room_id' => 'required|string',
        'timestamp' => 'nullable|integer',
        'per_page' => 'nullable|integer',
    ];

    protected $rulesSearch = [
        'keyword' => 'required|string',
        'per_page' => 'nullable|integer',
        'page' => 'nullable|integer|min:1',
    ];

    protected $rulesSearchFriend = [
        'per_page' => 'nullable|integer',
        'page' => 'nullable|integer|min:1',
    ];

    protected $rulesDelete = [
        'room_id' => 'required|string',
    ];

    public function getRepo()
    {
    }

    public function getTransformer()
    {
        return new MessageTransformer();
    }

    /**
     * @OA\POST(
     *     path="/api/room/message",
     *     tags={"Room - Message"},
     *     summary="Send message",
     *     security={{"bearerAuth":{}}},
     *
     *     @OA\RequestBody(@OA\JsonContent(ref="room-request.json#/send-message")),
     *
     *     @OA\Response(response="200", description="success", @OA\JsonContent(ref="room-response.json#/room/success")),
     *     @OA\Response(response="401", description="Unauthorized", @OA\JsonContent(ref="user-response.json#/invalid")),
     *     @OA\Response(response="404", description="Room not found.", @OA\JsonContent(ref="room-response.json#/room/fail")),
     * )
     */
    public function sendMessage(Request $request)
    {
        $data['user_id'] = Auth::user()->id;
        $data['room_id'] = $request->room_id;
        $data['content'] = $request->message;
        // $data['created_at'] = $request->timestamp ? Carbon::createFromTimestamp($request->timestamp) : Carbon::now();
        $data['timestamp'] = Carbon::now()->timestamp;

        $validator = $this->validate($request->all(), $this->rulesSend);

        if ($validator->fails()) {
            return $this->validateFail($validator);
        }

        $message = $this->service->sendMessage($data);

        return $this->response->withItem($message, $this->transformer);
    }

    /**
     * @OA\GET(
     *     path="/api/room/message",
     *     tags={"Room - Message"},
     *     summary="Get messgae",
     *     security={{"bearerAuth":{}}},
     *
     *     @OA\Parameter(
     *         name="room_id",
     *         in="query",
     *         required=true,
     *
     *         @OA\Schema(type="string"), example="abc_123", description="room_id"),
     *
     *     @OA\Parameter(
     *         name="per_page",
     *         in="query",
     *         required=true,
     *
     *         @OA\Schema(type="number"), example="25", description="per_page"),
     *
     *     @OA\Response(response=200, description="success", @OA\JsonContent(ref="room-response.json#/room/success")),
     *     @OA\Response(response=401, description="Unauthorized", @OA\JsonContent(ref="user-response.json#/invalid")),
     *     @OA\Response(response=404, description="Not found", @OA\JsonContent(ref="room-response.json#/room/faile")),
     * )
     */
    public function getMessage(Request $request)
    {
        $perPage = $request->per_page;
        $page = $request->page ?? 1;
        $roomId = $request->room_id;

        $validator = $this->validate($request->all(), $this->rulesGet);

        if ($validator->fails()) {
            return $this->validateFail($validator);
        }

        $message = $this->service->getMessage($roomId, $perPage, $page);

        // Seen message
        if (count($message)) {
            $latestMessages = Carbon::now()->timestamp;
            $this->service->seenMessage($roomId, $latestMessages);
        }

        return $this->response->withPaginator($message, $this->transformer);
    }

    /**
     * @OA\GET(
     *     path="/api/room/top",
     *     tags={"Room - Search"},
     *     summary="Search inbox",
     *     security={{"bearerAuth":{}}},
     *
     *     @OA\Parameter(
     *         name="keyword",
     *         in="query",
     *         required=true,
     *
     *         @OA\Schema(type="string"), example="abc_123", description="keyword"),
     *
     *     @OA\Response(response=200, description="success", @OA\JsonContent(ref="room-response.json#/search-message")),
     *     @OA\Response(response=401, description="Unauthorized", @OA\JsonContent(ref="user-response.json#/invalid")),
     * )
     */
    public function searchInbox(Request $request)
    {
        $keyword = $request->keyword;
        $validator = $this->validate($request->all(), $this->rulesSearch);

        if ($validator->fails()) {
            return $this->validateFail($validator);
        }

        $result = $this->service->searchTop($keyword);

        return $this->response->withItem($result, $this->searchMessageTransformer);
    }

    /**
     * @OA\GET(
     *     path="/api/room/top-user",
     *     tags={"Room - Search"},
     *     summary="Top inbox user",
     *     security={{"bearerAuth":{}}},
     *
     *     @OA\Parameter(
     *         name="keyword",
     *         in="query",
     *         required=true,
     *
     *         @OA\Schema(type="string"), example="abc_123", description="keyword"),
     *
     *     @OA\Parameter(
     *         name="per_page",
     *         in="query",
     *         required=false,
     *
     *         @OA\Schema(type="number"), example="25", description="per_page"),
     *
     *     @OA\Response(response=200, description="success", @OA\JsonContent(ref="room-response.json#/top-user")),
     *     @OA\Response(response=401, description="Unauthorized", @OA\JsonContent(ref="user-response.json#/invalid")),
     * )
     */
    public function topUser(Request $request)
    {
        $keyword = $request->keyword;
        $validator = $this->validate($request->all(), $this->rulesSearch);

        if ($validator->fails()) {
            return $this->validateFail($validator);
        }

        $perPage = $request->has('per_page') ? $request->per_page : null;
        $page = $request->page ?? 1;
        $users = $this->service->topUser($keyword, $perPage, $page);

        return $this->response->withPaginator($users, $this->topUserTransformer);
    }

    /**
     * @OA\GET(
     *     path="/api/room/top-room",
     *     tags={"Room - Search"},
     *     summary="Search top room",
     *     security={{"bearerAuth":{}}},
     *
     *     @OA\Parameter(
     *         name="keyword",
     *         in="query",
     *         required=true,
     *
     *         @OA\Schema(type="string"), example="abc_123", description="keyword"),
     *
     *     @OA\Parameter(
     *         name="per_page",
     *         in="query",
     *         required=false,
     *
     *         @OA\Schema(type="number"), example="25", description="per_page"),
     *
     *     @OA\Response(response=200, description="success", @OA\JsonContent(ref="room-response.json#/room/success")),
     *     @OA\Response(response=401, description="Unauthorized", @OA\JsonContent(ref="user-response.json#/invalid")),
     * )
     */
    public function topRoom(Request $request)
    {
        $keyword = $request->keyword;
        $validator = $this->validate($request->all(), $this->rulesSearch);

        if ($validator->fails()) {
            return $this->validateFail($validator);
        }

        $perPage = $request->has('per_page') ? $request->per_page : null;
        $page = $request->page ?? 1;
        $rooms = $this->service->topRooms($keyword, $perPage, $page);

        return $this->response->withPaginator($rooms, $this->roomTransformer);
    }

    /**
     * @OA\GET(
     *     path="/api/room/search-friend",
     *     tags={"Room - Search"},
     *     summary="Search friend",
     *     security={{"bearerAuth":{}}},
     *
     *     @OA\Parameter(
     *         name="per_page",
     *         in="query",
     *         required=false,
     *
     *         @OA\Schema(type="number"), example="25", description="per_page"),
     *
     *     @OA\Response(response=200, description="success", @OA\JsonContent(ref="room-response.json#/top-user")),
     *     @OA\Response(response=401, description="Unauthorized", @OA\JsonContent(ref="user-response.json#/invalid")),
     * )
     */
    public function searchFriend(Request $request)
    {
        $validator = $this->validate($request->all(), $this->rulesSearchFriend);

        if ($validator->fails()) {
            return $this->validateFail($validator);
        }

        $perPage = isset($request->per_page) ? $request->per_page : 10;
        $page = $request->page ?? 1;
        $keyword = $request->keyword;
        $checkRoomId = $request->check_room_id;

        $result = $this->service->searchFriend($keyword, $checkRoomId, $perPage, $page);

        return $this->response->withPaginator($result, $this->topUserTransformer);
    }

    /**
     * @OA\DELETE(
     *     path="/api/room/message",
     *     tags={"Room - Message"},
     *     summary="Delete message",
     *     security={{"bearerAuth":{}}},
     *
     *     @OA\Parameter(
     *         name="room_id",
     *         in="query",
     *         required=false,
     *
     *         @OA\Schema(type="string"), example="abc_123", description="room id"),
     *
     *     @OA\Response(response=200, description="success", @OA\JsonContent(ref="room-response.json#/delete")),
     *     @OA\Response(response=401, description="Unauthorized", @OA\JsonContent(ref="user-response.json#/invalid")),
     *     @OA\Response(response=404, description="Not found", @OA\JsonContent(ref="room-response.json#/room/fail"))
     * )
     */
    public function delete(Request $request)
    {
        $validator = $this->validate($request->all(), $this->rulesDelete);

        if ($validator->fails()) {
            return $this->validateFail($validator);
        }

        $this->service->deleteMessage($request->room_id);

        return response()->json([
            'message' => "Message in Room: $request->room_id delete successful",
        ], Response::HTTP_OK);
    }

    /**
     * @OA\GET(
     *     path="/api/room/count/message",
     *     tags={"Room - Message"},
     *     summary="Count unread messgae",
     *     security={{"bearerAuth":{}}},
     *
     *     @OA\Response(response=200, description="success", @OA\JsonContent(ref="room-response.json#/count-messgae")),
     *     @OA\Response(response=401, description="Unauthorized", @OA\JsonContent(ref="user-response.json#/invalid")),
     * )
     */
    public function countUnreadMessage()
    {
        $totalUnredMess = $this->service->countUnreadMessage();

        return response()->json([
            'data' => [
                'total_unread_message' => $totalUnredMess, // total room unread message
            ],
        ], Response::HTTP_OK);
    }
}
