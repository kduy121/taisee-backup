<?php

namespace App\Api\Controllers;

use App\Services\CollectionVideoService;
use App\Transformers\CollectionVideoTransformer;
use EllipseSynergie\ApiResponse\Contracts\Response as ApiResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class CollectionVideoController extends ApiController
{
    public function __construct(ApiResponse $response, Request $request, private CollectionVideoService $service)
    {
        parent::__construct($response, $request);
    }

    protected $rulesBookMarkVideo = [
        'collection_id' => 'nullable|string',
        'video_id' => 'required|string',
    ];

    protected $rulesBookMarkVideos = [
        'collection_id' => 'nullable|string',
        'video_ids' => 'required|array',
    ];

    protected $rulesGetVideo = [
        'collection_id' => 'nullable|string',
        'per_page' => 'nullable|integer|min:1',
        'page' => 'nullable|integer|min:1',
    ];

    public function getRepo()
    {
    }

    public function getTransformer()
    {
        return new CollectionVideoTransformer();
    }

    /**
     * @OA\POST(
     *     path="/api/video/collection/change",
     *     tags={"Video - Bookmark"},
     *     summary="Bookmartk change video",
     *     security={{"bearerAuth":{}}},
     *
     *     @OA\RequestBody(@OA\JsonContent(ref="video-request.json#/change-bookmark")),
     *
     *     @OA\Response(response="200", description="success", @OA\JsonContent(ref="video-response.json#/collection-video")),
     *     @OA\Response(response="401", description="Unauthorized", @OA\JsonContent(ref="user-response.json#/invalid")),
     *     @OA\Response(response="404", description="Video not found", @OA\JsonContent(ref="video-response.json#/not-found"))
     * )
     */
    public function changeCollection(Request $request)
    {
        $userId = Auth::user()->id;
        $data = $request->all();
        $collectionId = isset($data['collection_id']) ? $data['collection_id'] : null;

        $validator = $this->validate($data, $this->rulesBookMarkVideos);

        if ($validator->fails()) {
            return $this->validateFail($validator);
        }

        $collection = $this->service->bookmarkVideos($userId, $data['video_ids'], $collectionId);

        return $this->response->withCollection($collection, $this->transformer);
    }

    /**
     * @OA\POST(
     *     path="/api/video/bookmark",
     *     tags={"Video - Bookmark"},
     *     summary="Save bookmark video",
     *     security={{"bearerAuth":{}}},
     *
     *     @OA\RequestBody(@OA\JsonContent(ref="video-request.json#/save-bookmark")),
     *
     *     @OA\Response(response="200", description="success", @OA\JsonContent(ref="video-response.json#/collection-video")),
     *     @OA\Response(response="401", description="Unauthorized", @OA\JsonContent(ref="user-response.json#/invalid")),
     *     @OA\Response(response="404", description="Video not found", @OA\JsonContent(ref="video-response.json#/not-found"))
     * )
     */
    public function bookmarkVideo(Request $request)
    {
        $userId = Auth::user()->id;
        $data = $request->all();
        $collectionId = isset($data['collection_id']) ? $data['collection_id'] : null;

        $validator = $this->validate($data, $this->rulesBookMarkVideo);

        if ($validator->fails()) {
            return $this->validateFail($validator);
        }

        $collection = $this->service->bookmarkVideo($userId, $data['video_id'], $collectionId);

        return $this->response->withItem($collection, $this->transformer);
    }

    /**
     * @OA\DELETE(
     *     path="/api/video/bookmark",
     *     tags={"Video - Bookmark"},
     *     summary="Unbookmark video",
     *     security={{"bearerAuth":{}}},
     *
     *     @OA\RequestBody(@OA\JsonContent(ref="video-request.json#/unbookmark")),
     *
     *     @OA\Response(response="200", description="success", @OA\JsonContent(ref="video-response.json#/un-bookmark")),
     *     @OA\Response(response=401, description="Unauthorized", @OA\JsonContent(ref="user-response.json#/invalid")),
     *     @OA\Response(response=404, description="Video not found."),
     * )
     */
    public function unbookmarkVideo(Request $request)
    {
        $userId = Auth::user()->id;
        $data = $request->all();

        $validator = $this->validate($data, $this->rulesBookMarkVideo);

        if ($validator->fails()) {
            return $this->validateFail($validator);
        }

        $this->service->unbookmarkVideo($userId, $data['video_id']);

        return response()->json([
            'data' => [
                'message' => 'Unbookmark successfully',
            ],
        ], Response::HTTP_OK);
    }

    /**
     * @OA\GET(
     *     path="/api/video/bookmark",
     *     tags={"Video - Bookmark"},
     *     summary="Get list video save",
     *     security={{"bearerAuth":{}}},
     *
     *     @OA\Parameter(
     *         name="collection_id",
     *         in="query",
     *         required=false,
     *
     *         @OA\Schema(type="string"), example="null", description="collection id"),
     *
     *     @OA\Parameter(
     *         name="per_page",
     *         in="query",
     *         required=false,
     *
     *         @OA\Schema(type="number"), example="25", description="perpage"),
     *
     *     @OA\Parameter(
     *         name="page",
     *         in="query",
     *         required=false,
     *
     *         @OA\Schema(type="number"), example="1", description="page"),
     *
     *     @OA\Response(response="200", description="success", @OA\JsonContent(ref="video-response.json#/collection-video")),
     *     @OA\Response(response=401, description="Unauthorized", @OA\JsonContent(ref="user-response.json#/invalid")),
     * )
     */
    public function getVideo(Request $request)
    {
        $userId = Auth::user()->id;
        $data = $request->all();

        $validator = $this->validate($request->all(), $this->rulesGetVideo);
        if ($validator->fails()) {
            return $this->validateFail($validator);
        }

        $perPage = isset($data['per_page']) ? $data['per_page'] : null;
        $page = $request->input('page', 1);
        $collectionId = isset($data['collection_id']) ? $data['collection_id'] : null;
        $collection = $this->service->getVideo($userId, $collectionId, $perPage, $page);

        return $this->response->withPaginator($collection, $this->transformer);
    }
}
