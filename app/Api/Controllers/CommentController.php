<?php

namespace App\Api\Controllers;

use App\Repositories\CommentRepository;
use App\Services\CommentService;
use App\Transformers\CommentTransformer;
use EllipseSynergie\ApiResponse\Contracts\Response as ApiResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CommentController extends ApiController
{
    public function __construct(ApiResponse $response, Request $request, private CommentService $commentService)
    {
        parent::__construct($response, $request);
    }

    public function getRepo()
    {
        return new CommentRepository();
    }

    public function getTransformer()
    {
        return new CommentTransformer();
    }

    /**
     * @OA\POST(
     *     path="/api/comment/create",
     *     tags={"Comment"},
     *     summary="Create comment",
     *     security={{"bearerAuth":{}}},
     *
     *     @OA\RequestBody(@OA\JsonContent(ref="user-request.json#/create-comment")),
     *
     *     @OA\Response(response="200", description="Login success", @OA\JsonContent(ref="user-response.json#/comment/success")),
     *     @OA\Response(response="401", description="Unauthorized", @OA\JsonContent(ref="user-response.json#/invalid")),
     *     @OA\Response(response="404", description="Video not found", @OA\JsonContent(ref="user-response.json#/not-found"))
     * )
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'video_id' => 'required|string',
            'comment' => 'required|string',
        ]);

        if ($validator->fails()) {
            return $this->validateFail($validator);
        }

        $comment = $this->commentService->create($request->all());

        return $this->response->withItem($comment, $this->transformer);
    }

    /**
     * @OA\DELETE(
     *     path="/api/comment/delete",
     *     tags={"Comment"},
     *     summary="Delete comment",
     *     security={{"bearerAuth":{}}},
     *
     *     @OA\RequestBody(@OA\JsonContent(ref="user-request.json#/delete-comment")),
     *
     *     @OA\Response(response="200", description="Login success", @OA\JsonContent(ref="user-response.json#/delete-comment")),
     *     @OA\Response(response="401", description="Unauthorized", @OA\JsonContent(ref="user-response.json#/invalid")),
     *     @OA\Response(response="404", description="Comment/Video not found", @OA\JsonContent(ref="user-response.json#/not-found"))
     * )
     */
    public function delete(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'comment_id' => 'required|string',
            'video_id' => 'required|string',
        ]);

        if ($validator->fails()) {
            return $this->validateFail($validator);
        }

        $this->commentService->delete($request->all());

        return response()->json([
            'message' => "Comment: $request->comment_id delete successful",
        ], 200);
    }

    /**
     * @OA\POST(
     *     path="/api/comment/update",
     *     tags={"Comment"},
     *     summary="Update comment",
     *     security={{"bearerAuth":{}}},
     *
     *     @OA\RequestBody(@OA\JsonContent(ref="user-request.json#/update-comment")),
     *
     *     @OA\Response(response="200", description="Login success", @OA\JsonContent(ref="user-response.json#/comment/success")),
     *     @OA\Response(response="401", description="Unauthorized", @OA\JsonContent(ref="user-response.json#/invalid")),
     *     @OA\Response(response="404", description="Comment/Video not found", @OA\JsonContent(ref="user-response.json#/not-found"))
     * )
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'comment_id' => 'required|string',
            'video_id' => 'required|string',
            'comment' => 'required|string',
        ]);

        if ($validator->fails()) {
            return $this->validateFail($validator);
        }

        $data = $request->all();
        $comment = $this->commentService->update($data);

        return $this->response->withItem($comment, $this->transformer);
    }

    /**
     * @OA\GET(
     *     path="/api/comment/list",
     *     tags={"Comment"},
     *     summary="Get list comment",
     *     security={{"bearerAuth":{}}},
     *
     *     @OA\Parameter(
     *         name="video_id",
     *         in="query",
     *         required=true,
     *
     *         @OA\Schema(type="string"), example="aabc-123", description="Uuid"),
     *
     *     @OA\Response(response=200, description="Login success", @OA\JsonContent(ref="user-response.json#/comment/success")),
     *     @OA\Response(response=401, description="Login success", @OA\JsonContent(ref="user-response.json#/invalid"))
     * )
     */
    public function getComment(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'video_id' => 'required|string',
            'per_page' => 'sometimes|numeric',
        ]);

        if ($validator->fails()) {
            return $this->validateFail($validator);
        }

        $perPage = $request->has('per_page') ? $request->per_page : null;
        $videoId = $request->video_id;
        $comment = $this->commentService->searchByVideoId($videoId, $perPage);

        return $this->response->withPaginator($comment, $this->transformer);
    }
}
