<?php

namespace App\Api\Controllers;

use App\Services\RoomService;
use App\Transformers\RoomTransformer;
use EllipseSynergie\ApiResponse\Contracts\Response as ApiResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class RoomController extends ApiController
{
    public function __construct(ApiResponse $response, Request $request, private RoomService $service)
    {
        parent::__construct($response, $request);
    }

    protected $rulesCreate = [
        'name' => 'nullable|string',
        'member' => 'required|array',
    ];

    protected $rulesPin = [
        'room_id' => 'required|string',
    ];

    protected $rulesDelete = [
        'room_id' => 'required|string',
    ];

    protected $rulesLeave = [
        'room_id' => 'required|string',
    ];

    protected $rulesAddMember = [
        'room_id' => 'required|string',
        'user_ids' => 'required|array',
    ];

    protected $rulesChange = [
        'room_id' => 'required|string',
        'name' => 'required|string',
    ];

    protected $rulesGet = [
        'room_id' => 'required|string',
        'per_page' => 'nullable|integer|min:1',
        'page' => 'nullable|integer|min:1',
    ];

    public function getRepo()
    {
    }

    public function getTransformer()
    {
        return new RoomTransformer();
    }

    /**
     * @OA\POST(
     *     path="/api/room",
     *     tags={"Room"},
     *     summary="Create room",
     *     security={{"bearerAuth":{}}},
     *
     *     @OA\RequestBody(@OA\JsonContent(ref="room-request.json#/create")),
     *
     *     @OA\Response(response="200", description="Create success", @OA\JsonContent(ref="room-response.json#/room/success")),
     *     @OA\Response(response="401", description="Unauthorized", @OA\JsonContent(ref="user-response.json#/invalid"))
     * )
     */
    public function createRoom(Request $request)
    {
        $validator = $this->validate($request->all(), $this->rulesCreate);

        if ($validator->fails()) {
            return $this->validateFail($validator);
        }

        $data['user_id'] = Auth::user()->id;
        $data['name'] = $request->name;
        $data['member'] = $request->member;
        $data['thumb_ids'] = $request->member;

        $room = $this->service->createRoom($data);

        return $this->response->withItem($room, $this->transformer);
    }

    /**
     * @OA\GET(
     *     path="/api/room",
     *     tags={"Room"},
     *     summary="Get list room",
     *     security={{"bearerAuth":{}}},
     *
     *     @OA\Parameter(
     *         name="per_page",
     *         in="query",
     *         required=true,
     *
     *         @OA\Schema(type="number"), example="25", description="per page"),
     *
     *     @OA\Response(response=200, description="success", @OA\JsonContent(ref="room-response.json#/room/success")),
     *     @OA\Response(response=401, description="Unauthorized", @OA\JsonContent(ref="user-response.json#/invalid")),
     * )
     */
    public function getRooms(Request $request)
    {
        $perPage = $request->input('per_page');
        $page = $request->page ?? 1;
        $room = $this->service->getRooms($perPage, $page);

        return $this->response->withPaginator($room, $this->transformer);
    }

    /**
     * @OA\DELETE(
     *     path="/api/room",
     *     tags={"Room"},
     *     summary="Delete room",
     *     security={{"bearerAuth":{}}},
     *
     *     @OA\Parameter(
     *         name="room_id",
     *         in="query",
     *         required=true,
     *
     *         @OA\Schema(type="number"), example="abc_123", description="room id"),
     *
     *     @OA\Response(response=200, description="success", @OA\JsonContent(ref="room-response.json#/delete")),
     *     @OA\Response(response=401, description="Unauthorized", @OA\JsonContent(ref="user-response.json#/invalid")),
     *     @OA\Response(response=403, description="Forbidden")
     * )
     */
    public function deleteRoom(Request $request)
    {
        $validator = $this->validate($request->all(), $this->rulesDelete);
        $roomId = $request->room_id;

        if ($validator->fails()) {
            return $this->validateFail($validator);
        }

        $this->service->deleteRoom($roomId);

        return response()->json([
            'message' => "Room: $roomId delete successful",
        ], Response::HTTP_OK);
    }

    /**
     * @OA\POST(
     *     path="/api/room/leave",
     *     tags={"Room"},
     *     summary="Leave room",
     *     security={{"bearerAuth":{}}},
     *
     *     @OA\RequestBody(@OA\JsonContent(ref="room-request.json#/room")),
     *
     *     @OA\Response(response="200", description="success", @OA\JsonContent(ref="room-response.json#/leave-room")),
     *     @OA\Response(response="401", description="Unauthorized", @OA\JsonContent(ref="user-response.json#/invalid")),
     *     @OA\Response(response="404", description="Room not found.", @OA\JsonContent(ref="room-response.json#/room/fail"))
     * )
     */
    public function leaveRoom(Request $request)
    {
        $validator = $this->validate($request->all(), $this->rulesLeave);
        $roomId = $request->room_id;
        $userId = Auth::user()->id;

        if ($validator->fails()) {
            return $this->validateFail($validator);
        }

        $this->service->leaveRoom($roomId);

        return response()->json([
            'data' => [
                'message' => "User: $userId leave Room: $roomId successful",
            ],
        ], Response::HTTP_OK);
    }

    /**
     * @OA\POST(
     *     path="/api/room/add-mem",
     *     tags={"Room"},
     *     summary="Add member",
     *     security={{"bearerAuth":{}}},
     *
     *     @OA\RequestBody(@OA\JsonContent(ref="room-request.json#/add-member")),
     *
     *     @OA\Response(response="200", description="success", @OA\JsonContent(ref="room-response.json#/room/success")),
     *     @OA\Response(response="401", description="Unauthorized", @OA\JsonContent(ref="user-response.json#/invalid")),
     *     @OA\Response(response="404", description="Room not found.", @OA\JsonContent(ref="room-response.json#/room/fail"))
     * )
     */
    public function addMember(Request $request)
    {
        $validator = $this->validate($request->all(), $this->rulesAddMember);
        $roomId = $request->room_id;
        $memberIds = $request->user_ids;

        if ($validator->fails()) {
            return $this->validateFail($validator);
        }

        $room = $this->service->addMember($roomId, $memberIds);

        return $this->response->withItem($room, $this->transformer);
    }

    /**
     * @OA\POST(
     *     path="/api/room/pin",
     *     tags={"Room"},
     *     summary="Pin room",
     *     security={{"bearerAuth":{}}},
     *
     *     @OA\RequestBody(@OA\JsonContent(ref="room-request.json#/room")),
     *
     *     @OA\Response(response="200", description="success", @OA\JsonContent(ref="room-response.json#/pin-room")),
     *     @OA\Response(response="401", description="Unauthorized", @OA\JsonContent(ref="user-response.json#/invalid")),
     *     @OA\Response(response="404", description="Room not found.", @OA\JsonContent(ref="room-response.json#/room/fail"))
     * )
     */
    public function pinRoom(Request $request)
    {
        $validator = $this->validate($request->all(), $this->rulesPin);

        if ($validator->fails()) {
            return $this->validateFail($validator);
        }

        $room = $this->service->pinRoom($request->room_id);

        return response()->json([
            'data' => [
                'room_id' => $room->id,
                'message' => "Room: $room->id pin successful",
            ],
        ], Response::HTTP_OK);
    }

    /**
     * @OA\POST(
     *     path="/api/room/un-pin",
     *     tags={"Room"},
     *     summary="Un-Pin room",
     *     security={{"bearerAuth":{}}},
     *
     *     @OA\RequestBody(@OA\JsonContent(ref="room-request.json#/room")),
     *
     *     @OA\Response(response="200", description="success", @OA\JsonContent(ref="room-response.json#/pin-room")),
     *     @OA\Response(response="401", description="Unauthorized", @OA\JsonContent(ref="user-response.json#/invalid")),
     *     @OA\Response(response="404", description="Room not found.")
     * )
     */
    public function unPinRoom(Request $request)
    {
        $validator = $this->validate($request->all(), $this->rulesPin);

        if ($validator->fails()) {
            return $this->validateFail($validator);
        }

        $room = $this->service->pinRoom($request->room_id, false);

        return response()->json([
            'data' => [
                'room_id' => $room->id,
                'message' => "Room: $room->id un-pin successful",
            ],
        ], Response::HTTP_OK);
    }

    /**
     * @OA\POST(
     *     path="/api/room/change",
     *     tags={"Room"},
     *     summary="Change room name",
     *     security={{"bearerAuth":{}}},
     *
     *     @OA\RequestBody(@OA\JsonContent(ref="room-request.json#/change-room")),
     *
     *     @OA\Response(response="200", description="success", @OA\JsonContent(ref="room-response.json#/room/success")),
     *     @OA\Response(response="401", description="Unauthorized", @OA\JsonContent(ref="user-response.json#/invalid")),
     *     @OA\Response(response="404", description="Room not found.", @OA\JsonContent(ref="room-response.json#/room/fail")),
     *     @OA\Response(response="403", description="does not group.")
     * )
     */
    public function changeRoomName(Request $request)
    {
        $roomId = $request->room_id;
        $name = $request->name;

        $validator = $this->validate($request->all(), $this->rulesChange);

        if ($validator->fails()) {
            return $this->validateFail($validator);
        }

        $room = $this->service->changeRoomName($roomId, $name);

        return $this->response->withItem($room, $this->transformer);
    }

    /**
     * @OA\GET(
     *     path="/api/room/detail",
     *     tags={"Room"},
     *     summary="Get detail room",
     *     security={{"bearerAuth":{}}},
     *
     *     @OA\Parameter(
     *         name="room_id",
     *         in="query",
     *         required=true,
     *
     *         @OA\Schema(type="string"), example="abc_123", description="room id"),
     *
     *     @OA\Response(response=200, description="success", @OA\JsonContent(ref="room-response.json#/room/success")),
     *     @OA\Response(response=401, description="Unauthorized", @OA\JsonContent(ref="user-response.json#/invalid")),
     *     @OA\Response(response=404, description="Room not found", @OA\JsonContent(ref="room-response.json#/room/fail")),
     * )
     */
    public function getRoomDetail(Request $request)
    {
        $validator = $this->validate($request->all(), $this->rulesGet);

        if ($validator->fails()) {
            return $this->validateFail($validator);
        }

        $userId = Auth::user()->id;

        $room = $this->service->getRoomDetail($request->room_id, $userId);

        return $this->response->withItem($room, $this->transformer);
    }
}
