<?php

namespace App\Api\Controllers;

use App\Services\ImageService;
use EllipseSynergie\ApiResponse\Contracts\Response as ApiResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class ImageController extends ApiController
{
    public function __construct(ApiResponse $response, Request $request, private ImageService $imageService)
    {
        parent::__construct($response, $request);
    }

    public function getTransformer()
    {

    }

    protected $rulesUploadImage = [
        'content' => 'required|string',
        'file_type' => 'required|string|in:jpeg,png,jpg,gif',
    ];

    /**
     * @OA\POST(
     *     path="/api/image/upload",
     *     tags={"Upload"},
     *     summary="Upload image",
     *     security={{"bearerAuth":{}}},
     *
     *     @OA\RequestBody(@OA\JsonContent(ref="other-request.json#/upload-image")),
     *
     *     @OA\Response(response="200", description="success", @OA\JsonContent(ref="other-response.json#/upload-image")),
     *     @OA\Response(response="401", description="Unauthorized", @OA\JsonContent(ref="user-response.json#/invalid")),
     *     @OA\Response(response="422", description="Unprocessable Content")
     * )
     */
    public function handleUpload(Request $request)
    {
        $data = $request->all();
        $validator = $this->validate($data, $this->rulesUploadImage);
        if ($validator->fails()) {
            return $this->validateFail($validator);
        }

        $userId = Auth::user()->id;
        $image_url = $this->imageService->handleUpload($userId, $request);

        return response()->json(['data' => ['image_url' => $image_url]], Response::HTTP_OK);
    }
}
