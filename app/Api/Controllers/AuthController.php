<?php

namespace App\Api\Controllers;

use App\Enums\GenderType;
use App\Enums\SocialiteType;
use App\Http\Requests\Auth\LoginRequest;
use App\Models\User;
use App\Repositories\InterestRepository;
use App\Services\AuthService;
use App\Services\SocialiteService;
use App\Services\UserService;
use App\Transformers\UserTransformer;
use Carbon\Carbon;
use EllipseSynergie\ApiResponse\Contracts\Response as ApiResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthController extends ApiController
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    private $apiResponse;

    private $userTransformer;

    protected $rulesRefreshToken = [
        'device_id' => 'required|string',
    ];

    public function __construct(
        ApiResponse $apiResponse,
        UserTransformer $userTransformer,
        private SocialiteService $socialiteService,
        private UserService $userService,
        private AuthService $authService
    ) {
        $this->middleware('auth.token', ['except' => ['login', 'register', 'guestLogin', 'ping', 'checkEmail', 'refresh', 'loginAdmin']]);
        $this->expireTokenTime = config('constants.token.expire_token_time');
        $this->apiResponse = $apiResponse;
        $this->userTransformer = $userTransformer;
    }

    /**
     * @OA\POST(
     *     path="/api/auth/login",
     *     tags={"Auth"},
     *
     *     @OA\RequestBody(@OA\JsonContent(ref="auth-request.json#/login")),
     *
     *     @OA\Response(response="200", description="Login success", @OA\JsonContent(ref="auth-response.json#/login/success")),
     *     @OA\Response(response="401", description="Unauthorized", @OA\JsonContent(ref="auth-response.json#/login/fail"))
     * )
     */
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
            'device_id' => 'sometimes|string',
        ]);

        $deviceId = $request->device_id;

        if ($validator->fails()) {
            return response()->json([
                'error' => $validator->errors(),
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $credentials = request(['email', 'password']);

        try {
            if (! $token = auth('api')->attempt($credentials)) {
                return $this->respondUnauthorized();
            }

            $user = auth('api')->user();

            $userWithRelationships = User::with('interests', 'areas')->find($user->id);

            // if (!$user->hasVerifiedEmail() || !$user->status) {
            //     throw new AccessDeniedHttpException();
            // }
        } catch (JWTException $th) {
            throw new HttpException(500);
        }

        $refreshToken = '';
        if ($deviceId) {
            $refreshToken = $this->authService->createRefreshToken($user, $deviceId);
        }

        return $this->respondWithToken($token, $userWithRelationships, 'Login success', $refreshToken);
    }

    /**
     * @OA\POST(
     *     path="/api/auth/guest-login",
     *     tags={"Auth"},
     *
     *     @OA\RequestBody(@OA\JsonContent(ref="auth-request.json#/guest-login")),
     *
     *     @OA\Response(response="200", description="Login success", @OA\JsonContent(ref="auth-response.json#/guest-login"))
     * )
     */
    public function guestLogin(Request $request)
    {
        $deviceId = $request->device_id;

        if ($request->date_of_birth) {
            $dateOfBirth = Carbon::createFromFormat('Y-m-d H:i:s', $request->date_of_birth, 'UTC');
        }

        $dataUser = [
            'name' => 'Guest User',
            // TODO: guest +
            'device_info' => $request->device_info ?? '',
            'gender' => $request->gender ?? GenderType::other->name,
            // TODO: enum type
            'date_of_birth' => $dateOfBirth ?? null,
            'interest' => $request->user['interest'] ?? [],
            'socialite_type' => SocialiteType::GUEST->name,
        ];

        if (isset($request->device_info['DEVICE_ID'])) {
            $guestUser = User::updateOrCreate(
                [
                    'device_info->DEVICE_ID' => $request->device_info['DEVICE_ID'],
                    'socialite_type' => SocialiteType::GUEST->name,
                ],
                $dataUser
            );
        } else {
            $guestUser = User::create(
                $dataUser
            );
        }

        if (Auth::loginUsingId($guestUser->id)) {
            $token = JWTAuth::fromUser(Auth::user());

            // Insert Interest
            if ($request->has('user.interest')) {
                $this->insertInterest($guestUser, $request->user['interest']);
            }

            $refreshToken = '';
            if ($deviceId) {
                $refreshToken = $this->authService->createRefreshToken($guestUser, $deviceId);
            }

            return $this->respondWithToken($token, $guestUser, 'Login success', $refreshToken);
        }

        return $this->respondUnauthorized();
    }

    public function insertInterest(User $user, array $interest)
    {
        $interestRepo = new InterestRepository;
        $interestRepo->attachData($user, $interest);
    }

    /**
     * @OA\POST(
     *     path="/api/auth/register",
     *     tags={"Auth"},
     *
     *     @OA\RequestBody(@OA\JsonContent(ref="auth-request.json#/register")),
     *
     *     @OA\Response(response="200", description="Login success", @OA\JsonContent(ref="auth-response.json#/login/success"))
     * )
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            // 'name' => 'required|string|between:2,100',
            'email' => 'required|string|email|max:100|unique:users',
            'password' => 'required|string|min:6',
            // TODO: validate cac thong tin con lai
        ]);

        if ($request->has('user.date_of_birth')) {
            $dateOfBirth = Carbon::createFromFormat('Y-m-d H:i:s', $request->user['date_of_birth'], 'UTC');
        }

        if ($validator->fails()) {
            $errorFields = $validator->errors()->keys();
            $msg = implode($validator->errors()->get($errorFields[0]));

            return response()->json([
                'data' => [
                    'message' => $msg,
                ],
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        // Check user name
        $userName = $this->socialiteService->generateUserName($request->get('email'));

        $user = User::create([
            'name' => $request->get('name') ?? $userName,
            'email' => $request->get('email'),
            'password' => Hash::make($request->get('password')),
            'socialite_type' => $request->socialite_id ? SocialiteType::from($request->socialite_type)->name : SocialiteType::from('EMAIL')->name,
            'socialite_id' => $request->socialite_id ?? null,
            'device_info' => $request->device_info ?? '',
            'gender' => $request->user['gender'] ?? GenderType::other->name,
            // TODO: enum type
            'date_of_birth' => $dateOfBirth ?? null,
            'user_name' => $userName,
        ]);

        // Insert Interest
        if ($request->has('user.interest')) {
            $this->insertInterest($user, $request->user['interest']);
        }

        $token = JWTAuth::fromUser($user);

        return $this->respondWithToken($token, $user, 'User successfully registered');
    }

    /**
     * @OA\GET(
     *     path="/api/auth/me",
     *     tags={"Auth"},
     *     security={{"bearerAuth":{}}},
     *     summary="Get the authenticated user",
     *
     *     @OA\Response(response="200", description="Login success", @OA\JsonContent(ref="auth-response.json#/user-transformer")),
     * )
     */
    public function me(Request $request)
    {
        $data = $request->all();
        $extraInfo = [];
        $extraInfo['count_draft'] = ! empty($data['count_draft']) ? true : false;
        $extraInfo['count_msg'] = ! empty($data['count_msg']) ? true : false;

        $user = auth()->user();
        $user = $this->userService->appendUserInfo($user, $extraInfo);

        return $this->apiResponse->withItem($user, $this->userTransformer);
    }

    /**
     * @OA\POST(
     *     path="/api/auth/logout",
     *     tags={"Auth"},
     *
     *     security={{"bearerAuth":{}}},
     *
     *     @OA\Response(response="200", description="Login success", @OA\JsonContent(ref="auth-response.json#/logout"))
     * )
     */
    public function logout()
    {
        auth()->logout();

        return response()->json([
            'data' => [
                'message' => 'Successfully logged out',
            ],
        ]);
    }

    /**
     * @OA\POST(
     *     path="/api/auth/refresh",
     *     tags={"Auth"},
     *
     *     security={{"bearerAuth":{}}},
     *
     *     @OA\RequestBody(@OA\JsonContent(ref="auth-request.json#/refresh")
     *     ),
     *
     *     @OA\Response(response="200", description="Login success", @OA\JsonContent(ref="auth-response.json#/refresh/success")),
     *     @OA\Response(response="401", description="Unauthorized", @OA\JsonContent(ref="auth-response.json#/refresh/invalid"))
     * )
     */
    public function refresh(Request $request)
    {
        $refreshToken = $request->bearerToken();
        $deviceId = $request->device_id;

        $validator = $this->validate($request->all(), $this->rulesRefreshToken);

        if ($validator->fails()) {
            return $this->validateFail($validator);
        }

        try {
            $res = $this->authService->refreshToken($refreshToken, $deviceId);
            $message = 'Refresh Token Successfully';
            if ($res == false) {
                return $this->respondUnauthorized();
            }

            return $this->respondWithToken($res['newToken'], $res['user'], $message, $res['newRefreshToken']);
        } catch (JWTException $th) {
            return response()->json([
                'data' => [
                    'error' => $th->getMessage(),
                ],
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Get the token array structure.
     *
     * @param  string  $token
     * @param  User  $user
     * @param  string  $message
     * @return \Illuminate\Http\JsonResponse
     */
    // protected function respondWithToken($token, User $user = null, $message = null)
    // {
    //     return response()->json([
    //         'data' => [
    //             'message' => $message,
    //             'user' => $user,
    //             'token' => $token,
    //             'expires_in' => auth('api')->factory()->getTTL() * 60,
    //         ]
    //     ], Response::HTTP_OK);
    // }

    // protected function respondUnauthorized()
    // {
    //     return response()->json([
    //         'data' => [
    //             'status' => 'fail',
    //             'message' => 'Unauthorized'
    //         ]
    //     ], Response::HTTP_UNAUTHORIZED);
    // }

    public function delete($email)
    {
        // TODO: kiểm tra user có quyền xóa không
        $user = User::where('email', $email)->first();

        if ($user) {
            $user->delete();

            return response()->json([
                'data' => [
                    'message' => 'Delete user success',
                ],
            ], Response::HTTP_OK);
        }

        return response()->json([
            'data' => [
                'message' => 'User not found',
            ],
        ], Response::HTTP_NOT_FOUND);
    }

    /**
     * @OA\GET(
     *     path="/api/ping",
     *     tags={"Other"},
     *     summary="Ping to server",
     *
     *     @OA\Response(response="200", description="success", @OA\JsonContent(ref="other-response.json#/ping")),
     * )
     */
    public function ping()
    {
        return response()->json([
            'message' => 'Already logged in',
        ], Response::HTTP_OK);
    }

    /**
     * @OA\POST(
     *     path="/api/auth/login-admin",
     *     tags={"Auth"},
     *
     *     @OA\RequestBody(@OA\JsonContent(ref="auth-request.json#/login")),
     *
     *     @OA\Response(response="200", description="Login success", @OA\JsonContent(ref="auth-response.json#/login/success")),
     *     @OA\Response(response="401", description="Unauthorized", @OA\JsonContent(ref="auth-response.json#/login/fail"))
     * )
     */
    public function loginAdmin(LoginRequest $request)
    {
        $credentials = request(['email', 'password']);

        try {
            if (! $token = auth('admin')->attempt($credentials)) {
                return $this->respondUnauthorized();
            }

            $user = auth('admin')->user();
        } catch (JWTException $th) {
            throw new HttpException(500);
        }

        return $this->respondWithToken($token, $user, 'Login success');
    }
}
