<?php

namespace App\Api\Controllers;

use App\Services\ByteplusService;
use EllipseSynergie\ApiResponse\Contracts\Response as ApiResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ByteplusController extends ApiController
{
    public function __construct(ApiResponse $response, Request $request, private ByteplusService $byteplusService)
    {
        parent::__construct($response, $request);
    }

    public function getTransformer()
    {

    }

    /**
     * @OA\GET(
     *     path="/api/bp/upload/config",
     *     tags={"Byteplus"},
     *     summary="Get upload config",
     *     security={{"bearerAuth":{}}},
     *
     *     @OA\Response(response="200", description="success", @OA\JsonContent(ref="other-response.json#/config")),
     *     @OA\Response(response="401", description="Unauthorized", @OA\JsonContent(ref="user-response.json#/invalid"))
     * )
     */
    public function getUploadConfig()
    {
        $configs = $this->byteplusService->getUploadConfig();

        return response()->json(['data' => $configs], Response::HTTP_OK);
    }

    /**
     * @OA\GET(
     *     path="/api/bp/upload/handle",
     *     tags={"Byteplus"},
     *     summary="Get upload config",
     *     security={{"bearerAuth":{}}},
     *
     *     @OA\Response(response="200", description="success"),
     *     @OA\Response(response="401", description="Unauthorized", @OA\JsonContent(ref="user-response.json#/invalid"))
     * )
     */
    public function handleUpload(Request $request)
    {
        $result = $this->byteplusService->handlerUpload($request);

        return response()->json(['data' => ['status' => 'success']], Response::HTTP_OK);
    }
}
