<?php

namespace App\Api\Clients\Socialite;

use GuzzleHttp\RequestOptions;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Http;

class VerifyGoogle
{
    private $accessToken;

    private $type;

    public function __construct(string $accessToken, string $type)
    {
        $this->accessToken = $accessToken;
        $this->type = $type;
    }

    public function sendContext()
    {
        $headers = $this->getOptions()['headers'];
        $body = $this->getOptions()['json'];

        if ($this->type == 'ios') {
            $response = Http::withHeaders($headers)->get(config('clients.socialite.google.base_url').'oauth2/v3/tokeninfo', [
                'access_token' => $body['token'],
            ]);
        } else {
            $response = Http::withHeaders($headers)->post(config('clients.socialite.google.base_url').'oauth2/v3/tokeninfo', [
                'id_token' => $body['token'],
            ]);
        }

        $statusCode = $response->getStatusCode();

        if ($statusCode == Response::HTTP_OK) {
            return json_decode((string) $response->getBody(), true);
        }

        return false;
    }

    public function getOptions(): array
    {
        return [
            RequestOptions::JSON => [
                'token' => $this->accessToken,
            ],
            RequestOptions::HEADERS => [

            ],
        ];
    }
}
