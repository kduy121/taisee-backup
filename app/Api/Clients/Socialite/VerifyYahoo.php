<?php

namespace App\Api\Clients\Socialite;

use GuzzleHttp\RequestOptions;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Http;

class VerifyYahoo
{
    private $accessToken;

    public function __construct(string $accessToken)
    {
        $this->accessToken = $accessToken;
    }

    public function sendContext()
    {
        $headers = $this->getOptions()['headers'];
        $body = $this->getOptions()['json'];
        $response = Http::withHeaders($headers)->get(config('clients.socialite.yahoo.base_url'), $body);
        $statusCode = $response->getStatusCode();

        if ($statusCode == Response::HTTP_OK) {
            return json_decode((string) $response->getBody(), true);
        }

        return false;
    }

    public function getOptions(): array
    {
        return [
            RequestOptions::JSON => [
            ],
            RequestOptions::HEADERS => [
                'Authorization' => 'Bearer '.$this->accessToken,
            ],
        ];
    }
}
