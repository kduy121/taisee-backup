<?php

namespace App\Api\Clients\Socialite;

use GuzzleHttp\RequestOptions;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Http;

class VerifyTwitter
{
    private $accessToken;

    private $platform = null;

    public function __construct(string $accessToken, string $platform = null)
    {
        $this->accessToken = $accessToken;
        $this->platform = $platform;
    }

    public function sendContext()
    {
        $headers = $this->getOptions()['headers'];
        $body = $this->getOptions()['json'];
        $response = Http::withHeaders($headers)->get(config('clients.socialite.twitter.base_url').'2/users/me', $body);
        $statusCode = $response->getStatusCode();

        if ($statusCode == Response::HTTP_OK) {
            return json_decode((string) $response->getBody(), true);
        }

        return false;
    }

    public function getOptions(): array
    {
        $token = 'Bearer '.$this->accessToken;

        if ($this->platform) {
            $token = $this->accessToken;
        }

        return [
            RequestOptions::JSON => [
            ],
            RequestOptions::HEADERS => [
                'Authorization' => $token,
            ],
        ];
    }
}
