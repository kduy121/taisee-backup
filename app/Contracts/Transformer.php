<?php

namespace App\Contracts;

interface Transformer
{
    public static function transform($model);
}
