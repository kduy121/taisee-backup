<?php

namespace App\Http\Requests\User;

use App\Exceptions\CommonException;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;

class UpdateAdmin extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        $rules = [
            'avatar_file' => 'nullable|file|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'name' => 'nullable|string',
            'password' => 'nullable|string',
            'confirm_password' => 'nullable|string',
        ];
    
        if ($this->filled('password')) {
            $rules['password'] .= '|min:6';
        }
    
        return $rules;
    }

    protected function failedValidation(Validator $validator)
    {
        throw new CommonException(CommonException::DEFAULT_ERR_CODE);
    }
}
