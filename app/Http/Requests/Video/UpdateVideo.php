<?php

namespace App\Http\Requests\Video;

use App\Enums\VideoStatus;
use App\Exceptions\CommonException;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;

class UpdateVideo extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        $status = implode(',', [VideoStatus::Public->value, VideoStatus::Private->value]);
        $rulesStatus = 'required|numeric'.'|in:'.$status;

        return [
            'status' => $rulesStatus,
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new CommonException(CommonException::DEFAULT_ERR_CODE);
    }
}
