<?php

namespace App\Http\Requests\Role;

use App\Exceptions\CommonException;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;

class CreateRole extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'name' => 'required|unique:roles,name',
            'permission' => 'sometimes|array',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new CommonException(CommonException::DEFAULT_ERR_CODE);
    }
}
