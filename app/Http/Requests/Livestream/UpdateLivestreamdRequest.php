<?php

namespace App\Http\Requests\Livestream;

use App\Enums\LivestreamStatus;
use App\Exceptions\CommonException;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;

class UpdateLivestreamdRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        $status = implode(',', [LivestreamStatus::Public->value, LivestreamStatus::Practice->value]);
        $rulesStatus = 'required|numeric'.'|in:'.$status;

        return [
            'livestream_id' => 'required|string',
            'cover' => 'required|string',
            'title' => 'required|string',
            // 'streaming_path' => 'required|string',
            'introduction' => 'sometimes|string',
            'status' => $rulesStatus,
            // 'pause_time' => 'sometimes|string',
            'livestream_duration' => 'sometimes|numeric',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new CommonException(CommonException::DEFAULT_ERR_CODE);
    }
}
