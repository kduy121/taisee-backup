<?php

namespace App\Http\Middleware;

use App\Exceptions\UserException;
use Closure;
use Illuminate\Http\Response;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Auth;

class VerifyToken
{
    public function handle($request, Closure $next)
    {
        $authorizationHeader = $request->header('Authorization');

        if (empty($authorizationHeader)) {
            return response()->json([
                'data' => [
                    'message' => 'Error validating access token',
                ],
            ], Response::HTTP_UNAUTHORIZED);
        }

        $token = JWTAuth::parseToken();

        try {
            $user = $token->authenticate();
            $adminUser = Auth::guard('admin')->user();

            if (!$user && !$adminUser) {
                throw new UserException(UserException::USER_NOT_FOUND);
            }

            return $next($request);
        } catch (\Throwable $th) {
            return response()->json([
                'data' => [
                    // 'message' => 'Oops! Access token verification failed. Please double-check the token and give it another try',
                    'message' => 'Error validating access token',
                ],
            ], Response::HTTP_UNAUTHORIZED);
        }
    }
}
