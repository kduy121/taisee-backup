import { createApp } from 'vue';
import App from './App.vue';
import '@layouts/styles/index.scss';
import '@/@iconify/icons-bundle'
import vuetify from '@/plugins/vuetify'
import { loadFonts } from '@/plugins/webfontloader'
import router from '@/router'
import '@core-scss/template/index.scss'
import '@styles/styles.scss'
import { createPinia } from 'pinia'

loadFonts()

const app = createApp(App);

// Use plugins
app.use(vuetify)
app.use(createPinia())
app.use(router)

app.mount('#app');