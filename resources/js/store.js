import { defineStore } from "pinia";

export const useGlobalStore = defineStore("globalStore", {
    state: () => ({
        auth: {
            user: null,
            token: null
        }
    }),
    actions: {
        addAuth(user, token) {
            this.auth.user = user;
            this.auth.token = token;
        },
        removeAuth() {
            this.auth.user = null;
            this.auth.token = null;
        },
    },
});