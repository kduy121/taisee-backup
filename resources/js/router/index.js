import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    { path: '/', redirect: 'dashboard' },
    // { path: '/', redirect: '' },
    {
      path: '/',
      component: () => import('../layouts/default.vue'),
      children: [
        {
          path: 'dashboard',
          component: () => import('../pages/dashboard.vue'),
        },
        {
          path: 'account-settings',
          component: () => import('../pages/account-settings.vue'),
        },
        {
          path: 'typography',
          component: () => import('../pages/typography.vue'),
        },
        {
          path: 'icons',
          component: () => import('../pages/icons.vue'),
        },
        {
          path: 'cards',
          component: () => import('../pages/cards.vue'),
        },
        {
          path: 'tables',
          component: () => import('../pages/tables.vue'),
        },
        {
          path: 'form-layouts',
          component: () => import('../pages/form-layouts.vue'),
        },
        {
          path: 'user-roles',
          component: () => import('../pages/user-roles.vue'),
        },
        {
          path: 'user-roles/create',
          component: () => import('../pages/user-role-create.vue'),
        },
        {
          path: 'user-roles/update/:id',
          component: () => import('../pages/user-role-update.vue'),
          props: true,
        },
        {
          path: 'user-permissions',
          component: () => import('../pages/user-permissions.vue'),
        },
        {
          path: 'user-permission/create',
          component: () => import('../pages/user-permission-create.vue'),
        },
        {
          path: 'user-permission/update/:id',
          component: () => import('../pages/user-permission-update.vue'),
          props: true,
        },
        {
          path: 'users',
          component: () => import('../pages/users.vue'),
        },
        {
          path: 'users-admin',
          component: () => import('../pages/users-admin.vue'),
        },
        {
          path: 'user-admin/update/:id',
          component: () => import('../pages/user-admin-update.vue'),
          props: true,
        },
        {
          path: 'videos',
          component: () => import('../pages/videos.vue'),
        },
        {
          path: 'hashtags',
          component: () => import('../pages/hashtags.vue'),
        },
        {
          path: 'hashtag/create',
          component: () => import('../pages/hashtag-create.vue'),
        },
        {
          path: 'hashtag/update/:id',
          component: () => import('../pages/hashtag-update.vue'),
          props: true,
        },
        {
          path: 'report',
          component: () => import('../pages/report.vue'),
        },
      ],
    },
    {
      path: '/',
      component: () => import('../layouts/blank.vue'),
      children: [
        {
          path: 'login',
          component: () => import('../pages/login.vue'),
        },
        {
          path: 'register',
          component: () => import('../pages/register.vue'),
        },
        {
          path: '/:pathMatch(.*)*',
          component: () => import('../pages/[...all].vue'),
        },
      ],
    },
  ],
})

export default router
