<?php

namespace Tests\Feature;

use App\Exceptions\UserException;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UserApiTest extends TestCase
{
    use RefreshDatabase;

    protected $token;

    public function setUp(): void
    {
        parent::setUp();

        $response = $this->json('POST', '/api/auth/register', [
            'name' => 'John Doe',
            'email' => 'john@example.com',
            'password' => 'password',
        ]);

        $this->token = $response->json('data.token');
    }

    public function test_user_not_found()
    {
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$this->token,
        ])->json('POST', '/api/follow-user', [
            'following_id' => 'cc',
        ]);

        $response->assertStatus(400)
            ->assertJsonFragment([
                'data' => [
                    'status' => 'fail',
                    'message' => UserException::$statusTexts[UserException::USER_NOT_FOUND],
                    'code' => UserException::USER_NOT_FOUND,
                ],
            ]);
    }

    public function test_create_room_user_not_found()
    {
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$this->token,
        ])->json('POST', '/api/room', [
            'name' => 'room',
            'member' => ['abc'],
        ]);

        $response->assertStatus(400)
            ->assertJsonFragment([
                'data' => [
                    'status' => 'fail',
                    'message' => UserException::$statusTexts[UserException::USER_NOT_FOUND],
                    'code' => UserException::USER_NOT_FOUND,
                ],
            ]);
    }

    public function test_send_message_user_not_found()
    {
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$this->token,
        ])->json('POST', '/api/room', [
            'name' => 'room',
            'member' => ['abc'],
        ]);

        $response->assertStatus(400)
            ->assertJsonFragment([
                'data' => [
                    'status' => 'fail',
                    'message' => UserException::$statusTexts[UserException::USER_NOT_FOUND],
                    'code' => UserException::USER_NOT_FOUND,
                ],
            ]);
    }
}
