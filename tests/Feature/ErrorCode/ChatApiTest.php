<?php

namespace Tests\Feature;

use App\Exceptions\ChatException;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ChatApiTest extends TestCase
{
    use RefreshDatabase;

    protected $user;

    protected $guestUser;

    protected $guestUserB;

    protected $guestUserC;

    protected $memberNotRoom;

    protected $room;

    public function setUp(): void
    {
        parent::setUp();

        $createUser = $this->json('POST', '/api/auth/register', [
            'name' => 'John Doe',
            'email' => 'john@example.com',
            'password' => 'password',
        ]);

        $createGuestUser = $this->json('POST', '/api/auth/guest-login', [
            'type' => 'guest',
            'device_id' => 'abc_123',
        ]);

        $createGuestUserB = $this->json('POST', '/api/auth/guest-login', [
            'type' => 'guest',
            'device_id' => 'abc_1234',
        ]);

        $createGuestUserC = $this->json('POST', '/api/auth/guest-login', [
            'type' => 'guest',
            'device_id' => 'abc_12345',
        ]);

        $createGuestUserNotMember = $this->json('POST', '/api/auth/guest-login', [
            'type' => 'guest',
            'device_id' => 'abc_1234',
        ]);

        $this->user = $createUser->json('data');
        $this->guestUser = $createGuestUser->json('data');
        $this->guestUserB = $createGuestUserB->json('data');
        $this->guestUserC = $createGuestUserC->json('data');
        $this->memberNotRoom = $createGuestUserNotMember->json('data');

        $room = $this->withHeaders([
            'Authorization' => 'Bearer '.$this->user['token'],
        ])->json('POST', '/api/room', [
            'name' => 'abc',
            'member' => [
                $this->guestUser['user']['id'],
            ],
        ]);

        $this->room = $room->json('data');
    }

    public function test_member_already_in_room()
    {
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$this->user['token'],
        ])->json('POST', '/api/room/add-mem', [
            'room_id' => $this->room['id'],
            'user_ids' => [
                $this->guestUser['user']['id'],
            ],
        ]);

        $response->assertStatus(400)
            ->assertJsonFragment([
                'data' => [
                    'status' => 'fail',
                    'message' => ChatException::$statusTexts[ChatException::MEMBER_ALREADY],
                    'code' => ChatException::MEMBER_ALREADY,
                ],
            ]);
    }

    public function test_room_not_found()
    {
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$this->user['token'],
        ])->json('POST', '/api/room/leave', [
            'room_id' => 'abc',
        ]);

        $response->assertStatus(400)
            ->assertJsonFragment([
                'data' => [
                    'status' => 'fail',
                    'message' => ChatException::$statusTexts[ChatException::ROOM_NOT_FOUND],
                    'code' => ChatException::ROOM_NOT_FOUND,
                ],
            ]);
    }

    public function test_room_not_group()
    {
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$this->user['token'],
        ])->json('POST', '/api/room/change', [
            'room_id' => $this->room['id'],
            'name' => 'abc',
        ]);

        $roomId = $this->room['id'];
        $response->assertStatus(400)
            ->assertJsonFragment([
                'data' => [
                    'status' => 'fail',
                    'message' => 'Only support to change room name if it is public group and not private type.',
                    'code' => ChatException::ROOM_NOT_GROUP,
                ],
            ]);
    }

    public function test_room_already_pinned()
    {
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$this->user['token'],
        ])->json('POST', '/api/room/pin', [
            'room_id' => $this->room['id'],
        ]);

        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$this->user['token'],
        ])->json('POST', '/api/room/pin', [
            'room_id' => $this->room['id'],
        ]);

        $response->assertStatus(400)
            ->assertJsonFragment([
                'data' => [
                    'status' => 'fail',
                    'message' => ChatException::$statusTexts[ChatException::ROOM_ALREADY_PINNED],
                    'code' => ChatException::ROOM_ALREADY_PINNED,
                ],
            ]);
    }

    public function test_room_already_unpinned()
    {
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$this->user['token'],
        ])->json('POST', '/api/room/un-pin', [
            'room_id' => $this->room['id'],
        ]);

        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$this->user['token'],
        ])->json('POST', '/api/room/un-pin', [
            'room_id' => $this->room['id'],
        ]);

        $response->assertStatus(400)
            ->assertJsonFragment([
                'data' => [
                    'status' => 'fail',
                    'message' => ChatException::$statusTexts[ChatException::ROOM_ALREADY_UNPINNED],
                    'code' => ChatException::ROOM_ALREADY_UNPINNED,
                ],
            ]);
    }

    public function test_room_unauthor()
    {
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$this->guestUser['token'],
        ])->json('DELETE', '/api/room', [
            'room_id' => $this->room['id'],
        ]);

        $response->assertStatus(400)
            ->assertJsonFragment([
                'data' => [
                    'status' => 'fail',
                    'message' => ChatException::$statusTexts[ChatException::FORBIDDEN],
                    'code' => ChatException::FORBIDDEN,
                ],
            ]);
    }

    public function test_send_message()
    {
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$this->guestUser['token'],
        ])->json('POST', '/api/room/message', [
            'room_id' => 'abc',
            'message' => $this->room['id'],
        ]);

        $response->assertStatus(400)
            ->assertJsonFragment([
                'data' => [
                    'status' => 'fail',
                    'message' => ChatException::$statusTexts[ChatException::ROOM_NOT_FOUND],
                    'code' => ChatException::ROOM_NOT_FOUND,
                ],
            ]);
    }

    public function test_leave_room()
    {
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$this->guestUser['token'],
        ])->json('POST', '/api/room/leave', [
            'room_id' => 'abc',
        ]);

        $response->assertStatus(400)
            ->assertJsonFragment([
                'data' => [
                    'status' => 'fail',
                    'message' => ChatException::$statusTexts[ChatException::ROOM_NOT_FOUND],
                    'code' => ChatException::ROOM_NOT_FOUND,
                ],
            ]);
    }

    public function test_get_room_detail()
    {
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$this->guestUser['token'],
        ])->json('GET', '/api/room/detail', [
            'room_id' => 'abc',
        ]);

        $response->assertStatus(400)
            ->assertJsonFragment([
                'data' => [
                    'status' => 'fail',
                    'message' => ChatException::$statusTexts[ChatException::ROOM_NOT_FOUND],
                    'code' => ChatException::ROOM_NOT_FOUND,
                ],
            ]);
    }

    public function test_get_message()
    {
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$this->memberNotRoom['token'],
        ])->json('GET', '/api/room/message', [
            'room_id' => $this->room['id'],
        ]);

        $response->assertStatus(400)
            ->assertJsonFragment([
                'data' => [
                    'status' => 'fail',
                    'message' => ChatException::$statusTexts[ChatException::USER_NOT_MEMBER],
                    'code' => ChatException::USER_NOT_MEMBER,
                ],
            ]);
    }

    public function test_user_send_message()
    {
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$this->memberNotRoom['token'],
        ])->json('POST', '/api/room/message', [
            'room_id' => $this->room['id'],
            'message' => 'abc',
        ]);

        $response->assertStatus(400)
            ->assertJsonFragment([
                'data' => [
                    'status' => 'fail',
                    'message' => ChatException::$statusTexts[ChatException::USER_NOT_MEMBER],
                    'code' => ChatException::USER_NOT_MEMBER,
                ],
            ]);
    }

    public function test_not_user_leave_room()
    {
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$this->memberNotRoom['token'],
        ])->json('POST', '/api/room/leave', [
            'room_id' => $this->room['id'],
        ]);

        $response->assertStatus(400)
            ->assertJsonFragment([
                'data' => [
                    'status' => 'fail',
                    'message' => ChatException::$statusTexts[ChatException::USER_NOT_MEMBER],
                    'code' => ChatException::USER_NOT_MEMBER,
                ],
            ]);
    }

    public function test_not_user_pinned_room()
    {
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$this->memberNotRoom['token'],
        ])->json('POST', '/api/room/pin', [
            'room_id' => $this->room['id'],
        ]);

        $response->assertStatus(400)
            ->assertJsonFragment([
                'data' => [
                    'status' => 'fail',
                    'message' => ChatException::$statusTexts[ChatException::USER_NOT_MEMBER],
                    'code' => ChatException::USER_NOT_MEMBER,
                ],
            ]);
    }

    public function test_member_not_in_room()
    {
        $room = $this->withHeaders([
            'Authorization' => 'Bearer '.$this->user['token'],
        ])->json('POST', '/api/room', [
            'name' => 'room',
            'member' => [
                $this->guestUser['user']['id'],
                $this->guestUserB['user']['id'],
            ],
        ]);
        $room = $room->json('data');

        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$this->guestUserC['token'],
        ])->json('POST', '/api/room/change', [
            'name' => 'room',
            'room_id' => $room['id'],
        ]);

        $userId = $this->guestUserC['user']['id'];
        $message = 'Only support to change room name if user must belong to this group.';
        $response->assertStatus(400)
            ->assertJsonFragment([
                'data' => [
                    'status' => 'fail',
                    'message' => $message,
                    'code' => ChatException::MEMBER_NOT_IN_ROOM,
                ],
            ]);
    }
}
