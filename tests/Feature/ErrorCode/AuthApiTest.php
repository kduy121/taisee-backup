<?php

namespace Tests\Feature;

use App\Exceptions\AuthException;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class AuthApiTest extends TestCase
{
    use RefreshDatabase;

    protected $data;

    public function setUp(): void
    {
        parent::setUp();

        $response = $this->json('POST', '/api/auth/register', [
            'name' => 'John Doe',
            'email' => 'john@example.com',
            'password' => 'password',
        ]);

        $dataLogin = $this->json('POST', '/api/auth/login', [
            'email' => 'john@example.com',
            'password' => 'password',
            'device_id' => 'abc_123',
        ]);

        $this->data = $dataLogin->json('data');
    }

    public function test_refresh_token()
    {
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$this->data['refresh_token'],
        ])->json('POST', '/api/auth/refresh', [
            'device_id' => 'abc_123',
        ]);

        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$this->data['refresh_token'],
        ])->json('POST', '/api/auth/refresh', [
            'device_id' => 'abc_123',
        ]);

        $response->assertStatus(400)
            ->assertJsonFragment([
                'data' => [
                    'status' => 'fail',
                    'message' => AuthException::$statusTexts[AuthException::REFRESH_TOKEN_NOT_FOUND],
                    'code' => AuthException::REFRESH_TOKEN_NOT_FOUND,
                ],
            ]);
    }

    public function test_token_invalid()
    {
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$this->data['refresh_token'],
        ])->json('POST', '/api/auth/refresh', [
            'device_id' => 'abc',
        ]);

        $response->assertStatus(400)
            ->assertJsonFragment([
                'data' => [
                    'status' => 'fail',
                    'message' => AuthException::$statusTexts[AuthException::REFRESH_TOKEN_INVALID],
                    'code' => AuthException::REFRESH_TOKEN_INVALID,
                ],
            ]);
    }
}
