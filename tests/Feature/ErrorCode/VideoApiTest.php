<?php

namespace Tests\Feature;

use App\Exceptions\VideoException;
use App\Models\Video;
use App\Models\VideoDraft;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class VideoApiTest extends TestCase
{
    use RefreshDatabase;

    protected $video;

    protected $token;

    protected $videoDraft;

    public function setUp(): void
    {
        parent::setUp();

        $response = $this->json('POST', '/api/auth/register', [
            'name' => 'John Doe',
            'email' => 'john@example.com',
            'password' => 'password',
        ]);

        $responseB = $this->json('POST', '/api/auth/register', [
            'name' => 'John Doe',
            'email' => 'johnB@example.com',
            'password' => 'password',
        ]);

        $userId = $responseB->json('data.user.id');
        $this->token = $response->json('data.token');
        $this->video = Video::factory()->create(['user_id' => $userId]);
        $this->videoDraft = VideoDraft::factory()->create(['user_id' => $userId]);
    }

    public function test_video_not_found()
    {
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$this->token,
        ])->json('POST', '/api/video/collection/change', [
            'collection_id' => 'cc',
            'video_ids' => [
                'b4b8d06d-f581-4d26-9b28-66087e5a1394',
            ],
        ]);

        $response->assertStatus(400)
            ->assertJsonFragment([
                'data' => [
                    'status' => 'fail',
                    'message' => VideoException::$statusTexts[VideoException::VIDEO_NOT_FOUND],
                    'code' => VideoException::VIDEO_NOT_FOUND,
                ],
            ]);
    }

    public function test_video_not_author()
    {
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$this->token,
        ])->json('POST', '/api/video/change', [
            'status' => 1,
            'video_id' => $this->video->id,
        ]);

        $response->assertStatus(400)
            ->assertJsonFragment([
                'data' => [
                    'status' => 'fail',
                    'message' => VideoException::$statusTexts[VideoException::NOT_AUTHOR],
                    'code' => VideoException::NOT_AUTHOR,
                ],
            ]);
    }

    public function test_video_draft_not_found()
    {
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$this->token,
        ])->json('GET', '/api/video/draft', [
            'video_id' => 'abc',
        ]);

        $response->assertStatus(400)
            ->assertJsonFragment([
                'data' => [
                    'status' => 'fail',
                    'message' => VideoException::$statusTexts[VideoException::VIDEO_DRAFT_NOT_FOUND],
                    'code' => VideoException::VIDEO_DRAFT_NOT_FOUND,
                ],
            ]);
    }

    public function test_cant_publish_video()
    {
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$this->token,
        ])->json('POST', '/api/video/draft/publish', [
            'video_id' => $this->videoDraft->id,
            'status' => 1,
            'title' => 'title',
            'description' => 'description',
        ]);

        $response->assertStatus(400)
            ->assertJsonFragment([
                'data' => [
                    'status' => 'fail',
                    'message' => VideoException::$statusTexts[VideoException::FAILED_PUBLISH_VIDEO],
                    'code' => VideoException::FAILED_PUBLISH_VIDEO,
                ],
            ]);
    }
}
