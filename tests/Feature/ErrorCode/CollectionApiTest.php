<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CollectionApiTest extends TestCase
{
    use RefreshDatabase;

    protected $token;

    public function setUp(): void
    {
        parent::setUp();

        $response = $this->json('POST', '/api/auth/register', [
            'name' => 'John Doe',
            'email' => 'john@example.com',
            'password' => 'password',
        ]);

        $this->token = $response->json('data.token');
    }

    /**
     * Test create collection.
     */
    public function test_can_create_collection()
    {
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$this->token,
        ])->json('POST', '/api/video/collection', [
            'name' => 'cc',
            'public' => false,
        ]);

        $response->assertStatus(200);
    }

    /**
     * Test rename collection.
     */
    public function test_can_rename_collection()
    {
        $responseCreate = $this->withHeaders([
            'Authorization' => 'Bearer '.$this->token,
        ])->json('POST', '/api/video/collection', [
            'name' => 'cc',
            'public' => false,
        ]);

        $responseCreate = $responseCreate->json('data');

        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$this->token,
        ])->json('PUT', '/api/video/collection', [
            'name' => 'ccc',
            'collection_id' => $responseCreate['id'],
        ]);

        $response->assertStatus(200);
    }
}
